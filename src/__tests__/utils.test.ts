import { mocked } from 'ts-jest/utils';
import * as utils from '../utils';
import { ItemType } from '../models/todo/itemtype.model';
import { PassportTodo, MeetingTodo } from '../models/app/todo.model';

import axios from 'axios';

jest.mock('axios', () => ({
  request: jest.fn()
}));

describe('utils', () => {
  describe('fetchWrapper', () => {
    afterEach(() => {
      jest.clearAllMocks();
    });

    it('should update api urls', () => {
      mocked(axios.request).mockImplementation(() =>
        Promise.resolve({ data: {} })
      );
      const url = '/test/url';
      utils.fetchWrapper(url);
      expect(mocked(axios.request).mock.calls).toHaveLength(1);
      expect(mocked(axios.request).mock.calls[0][0].url).toBeDefined();
      expect(mocked(axios.request).mock.calls[0][0].url).not.toEqual(url);
    });

    it('should update graphql urls', () => {
      mocked(axios.request).mockImplementation(() =>
        Promise.resolve({ data: {} })
      );
      const url = '/test/graphql';
      utils.fetchWrapper(url);
      expect(mocked(axios.request).mock.calls).toHaveLength(1);
      expect(mocked(axios.request).mock.calls[0][0].url).toBeDefined();
      expect(mocked(axios.request).mock.calls[0][0].url).not.toEqual(url);
    });

    it('should return data', async () => {
      const testData = 'this is test data';
      mocked(axios.request).mockImplementation(() =>
        Promise.resolve({ data: testData })
      );
      const url = '/test';
      const resp = await utils.fetchWrapper(url);
      expect(resp).toEqual(testData);
    });

    it('should throw errors', () => {
      const error = 'this is an error';
      mocked(axios.request).mockImplementation(() => Promise.reject(error));
      const url = '/test';
      expect(utils.fetchWrapper(url)).rejects.toEqual(new Error(error));
    });
  });

  //formatDate

  describe('formatDate', () => {
    it('should format date of type string properly', () => {
      const result = utils.formatDate('2019-11-01T08:00:00.463Z');
      expect(result).toBe('11/1');
    });
    it('should format date  properly', () => {
      const result = utils.formatDate(new Date('2019-11-01T00:04:42.463Z'));
      let currentDate = new Date();
      let month = currentDate.getMonth() + 1;
      let day = currentDate.getDate();
      let res = month + '/' + day;
      expect(result).toBe(res);
    });
    it('should return formatted date', () => {
      const result = utils.formatDate(2019 / 11 / 1);
      let currentDate = new Date();
      let month = currentDate.getMonth() + 1;
      let day = currentDate.getDate();
      let res = month + '/' + day;
      expect(result).toBe(res);
    });
    it('should return current date if an invalid string is passed', () => {
      const result = utils.formatDate('2019-13-09');
      let currentDate = new Date();
      let month = currentDate.getMonth() + 1;
      let day = currentDate.getDate();
      let res = month + '/' + day;
      expect(result).toBe(res);
    });
    it('should return current date if an empty string is passed', () => {
      const result = utils.formatDate('');
      let currentDate = new Date();
      let month = currentDate.getMonth() + 1;
      let day = currentDate.getDate();
      let res = month + '/' + day;
      expect(result).toBe(res);
    });
    it('should return formated date', () => {
      const result = utils.formatDate(2019 / 11 / 1, true);
      var days = [
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday'
      ];
      var months = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
      ];

      var day = days[new Date().getDay()];
      var month = months[new Date().getMonth()];
      const res =
        day +
        ',' +
        ' ' +
        month +
        ' ' +
        ('0' + new Date().getDate()).slice(-2) +
        ',' +
        ' ' +
        new Date().getFullYear();
      expect(result).toBe(res);
    });
    it('should return formatted date', () => {
      const result = utils.formatDate(2019 / 11 / 1, false);
      let currentDate = new Date();
      let month = currentDate.getMonth() + 1;
      let day = currentDate.getDate();
      let res = month + '/' + day;
      expect(result).toBe(res);
    });
    it('should return formatted date', () => {
      const result = utils.formatDate(2019 / 11 / 1, false);
      let currentDate = new Date();
      let month = currentDate.getMonth() + 1;
      let day = currentDate.getDate();
      let res = month + '/' + day;
      expect(result).toBe(res);
    });
  });

  //handleDateOffset

  describe('handleOffset', () => {
    it('should return null if a date is passed', () => {
      const result = utils.handleDateOffset(
        new Date('2019-11-01T08:00:00.463Z')
      );
      expect(result).toBeUndefined();
    });

    it('should return null if an empty string is passed', () => {
      const result = utils.handleDateOffset('');
      expect(result).toEqual(null);
    });

    it('should return the date based upon the timezone if a string type date is passed', () => {
      const result = utils.handleDateOffset('2019-11-01T00:04:42.463Z');
      expect(result).toEqual(new Date('2019-11-01T00:04:42.463Z'));
    });
  });

  //parseTime

  describe('parseTime', () => {
    it('should return formatted time ', () => {
      const result = utils.parseTime(new Date());
      let ndate = new Date();
      expect(result).toBe(
        ('0' + ndate.getHours()).slice(-2) +
          ':' +
          ('0' + ndate.getMinutes()).slice(-2)
      );
    });
    it('should return current time  when an empty string is passed', () => {
      const result = utils.parseTime('');
      let ndate = new Date();
      expect(result).toBe(
        ('0' + ndate.getHours()).slice(-2) +
          ':' +
          ('0' + ndate.getMinutes()).slice(-2)
      );
    });
    it('should return current time  when an invalid string is passed', () => {
      const result = utils.parseTime('2019-13-02');
      let ndate = new Date();
      expect(result).toBe(
        ('0' + ndate.getHours()).slice(-2) +
          ':' +
          ('0' + ndate.getMinutes()).slice(-2)
      );
    });
    it('should return current time', () => {
      const result = utils.parseTime(2019 / 11 / 1);
      let ndate = new Date();
      expect(result).toBe(
        ('0' + ndate.getHours()).slice(-2) +
          ':' +
          ('0' + ndate.getMinutes()).slice(-2)
      );
    });
  });
});

//todoTransform

describe('todoTransform', () => {
  ///Task Todo

  it('should return a task todo', () => {
    const type: ItemType = ItemType['Task'];
    let dDate = new Date();
    dDate.setHours(dDate.getHours() - 7);
    let sDate = new Date();
    sDate.setHours(sDate.getHours() - 8);
    const taskTodos = [
      {
        assignedTo: 'Brian Carroll',
        attendees: [
          { name: 'brian carrol' },
          { name: 'kevin burroughs' },
          { name: 'davisson, mark' },
          { name: 'barnum, michael' },
          { name: 'peterson, elizabeth' }
        ],
        body: 'The agenda has been emailed to attendees.',
        dueDate: dDate,
        id: 'email',
        itemType: type,
        startDate: sDate,
        status: 'Unknown (Status)',
        subjectLine: 'Send email to Randy',
        webLink: 'https://example.com/webLink'
      }
    ];
    const results = utils.todoTransform(taskTodos);
    expect(results.length).toEqual(1);
    expect(results[0]).toBeDefined();
    expect(results[0].tasks[0]).toBeDefined();
    expect(results[0].tasks[0].hasOwnProperty('newcapurl')).toEqual(false);
    expect(results[0].tasks[0].hasOwnProperty('meetingInfo')).toEqual(false);
  });

  ////Passport Todo

  it('should return a passport todo', () => {
    const type: ItemType = ItemType['Passport'];
    let dDate = new Date();
    dDate.setHours(dDate.getHours() - 7);
    let sDate = new Date();
    sDate.setHours(sDate.getHours() - 8);
    const passportTodos = [
      {
        assignedTo: 'Brian Carroll',
        attendees: [
          { name: 'brian carrol' },
          { name: 'kevin burroughs' },
          { name: 'davisson, mark' },
          { name: 'barnum, michael' },
          { name: 'peterson, elizabeth' }
        ],
        body: 'The agenda has been emailed to attendees.',
        dueDate: dDate,
        id: 'email',
        itemType: type,
        newcapurl: 'https://example.com/newcapurl',
        startDate: sDate,
        status: 'Unknown (Status)',
        subjectLine: 'Send email to Randy',
        webLink: 'https://example.com/webLink'
      }
    ];
    const results = utils.todoTransform(passportTodos);

    expect(results.length).toEqual(1);
    expect(results[0]).toBeDefined();
    expect(results[0].tasks[0]).toBeDefined();
    expect((results[0].tasks[0] as PassportTodo).newcapurl).toEqual(
      'https://example.com/newcapurl'
    );
  });

  ///Meeting Todo

  it('should return a meeting todo', () => {
    const type: ItemType = ItemType['Meeting'];
    let dDate = new Date();
    dDate.setDate(dDate.getDay() + 1);
    dDate.setHours(dDate.getHours() - 7);
    let sDate = new Date();
    sDate.setDate(sDate.getDay() + 1);
    sDate.setHours(sDate.getHours() - 8);

    const meetingTodos = [
      {
        assignedTo: 'Brian Carroll',
        attendees: [
          { name: 'brian carrol' },
          { name: 'kevin burroughs' },
          { name: 'davisson, mark' },
          { name: 'barnum, michael' },
          { name: 'peterson, elizabeth' }
        ],
        body: 'The agenda has been emailed to attendees.',
        dueDate: dDate,
        id: 'weekly-team-meeting',
        itemType: type,
        onlineMeetingUrl: 'https://example.com/onlineMeetingUrl',
        startDate: sDate,
        status: 'Unknown (Status)',
        subjectLine: 'Send email to Randy',
        webLink: 'https://example.com/webLink'
      }
    ];

    const results = utils.todoTransform(meetingTodos);

    expect(results.length).toEqual(1);
    expect(results[0]).toBeDefined();
    expect(results[0].tasks[0]).toBeDefined();
    expect((results[0].tasks[0] as MeetingTodo).meetingInfo).toBeDefined();
    expect(
      (results[0].tasks[0] as MeetingTodo).meetingInfo.onlineMeetingUrl
    ).toEqual('https://example.com/onlineMeetingUrl');
  });

  ///Todo with no due date

  it('Should return an empty list if there is no due date', () => {
    const type: ItemType = ItemType['Meeting'];
    const meetingTodos = [
      {
        assignedTo: 'Brian Carroll',
        attendees: [
          { name: 'brian carrol' },
          { name: 'kevin burroughs' },
          { name: 'davisson, mark' },
          { name: 'barnum, michael' },
          { name: 'peterson, elizabeth' }
        ],
        body: 'The agenda has been emailed to attendees.',
        id: 'weekly-team-meeting',
        itemType: type,
        onlineMeetingUrl: 'https://example.com/onlineMeetingUrl',
        startDate: new Date(),
        status: 'Unknown (Status)',
        subjectLine: 'Send email to Randy',
        webLink: 'https://example.com/webLink'
      }
    ];

    const results = utils.todoTransform(meetingTodos);
    expect(results.length).toEqual(0);
  });

  ///Todo with no start date

  it('Should return an empty list if there is no start date', () => {
    const type: ItemType = ItemType['Meeting'];
    const meetingTodos = [
      {
        assignedTo: 'Brian Carroll',
        attendees: [
          { name: 'brian carrol' },
          { name: 'kevin burroughs' },
          { name: 'davisson, mark' },
          { name: 'barnum, michael' },
          { name: 'peterson, elizabeth' }
        ],
        body: 'The agenda has been emailed to attendees.',
        id: 'weekly-team-meeting',
        itemType: type,
        onlineMeetingUrl: 'https://example.com/onlineMeetingUrl',
        dueDate: new Date(),
        status: 'Unknown (Status)',
        subjectLine: 'Send email to Randy',
        webLink: 'https://example.com/webLink'
      }
    ];

    const results = utils.todoTransform(meetingTodos);
    expect(results.length).toEqual(0);
  });

  ///uomConverter

  describe('uomConverter', () => {
    it('should return % when percent is passed', () => {
      const result = utils.uomConverter('Percent');
      expect(result).toBe('%');
    });
    it('should return the same string passed if it is not percent', () => {
      const result = utils.uomConverter('LB/HR');
      expect(result).toBe('LB/HR');
    });
  });
});

///shortenString

describe('shortenString', () => {
  it('should return formated string', () => {
    const result = utils.shortenString('Testing', 6);
    expect(result).toEqual('Testin...');
  });
  it('should return formated string', () => {
    const result = utils.shortenString('Testing', 0);
    expect(result).toEqual('...');
  });
  it('should return formated string', () => {
    const result = utils.shortenString('Testing', -1);
    expect(result).toEqual('Testin...');
  });
});

//transformConfigParams

describe('transformConfigParams', () => {
  it('should return transformed config', () => {
    let params = [{ key: 'widgetSize', value: 'small' }];
    const result = utils.transformConfigParams(params);
    expect(result).toEqual({ widgetSize: 'small' });
  });
  it('should return empty object on passing empty array', () => {
    let params = [{}];
    const result = utils.transformConfigParams(params);
    expect(result).toEqual({});
  });
});

//getWidgetIdArray

describe('getWidgetTypeArray', () => {
  let widgetJson = [
    {
      id: ['PowerBI', 'powerbi', 'PowerBIDefault', 'powerbidefault'],
      component: 'IFrameWidget',
      name: 'PowerBI'
    },
    {
      id: ['Appian', 'appian', 'appiandefault', 'AppianDefault'],
      component: 'AppianWidget',
      name: 'Appian'
    }
  ];
  it('should return widgets ID array', () => {
    const result = utils.getWidgetTypeArray(widgetJson, 'powerbi');
    expect(result).toEqual([
      'PowerBI',
      'powerbi',
      'PowerBIDefault',
      'powerbidefault'
    ]);
  });
  it('should return widgets empty array', () => {
    const result = utils.getWidgetTypeArray(widgetJson, 'test');
    expect(result).toEqual([]);
  });
});

//getWidgetNames

describe('getWidgetNames', () => {
  let widgetJson = [
    {
      id: ['PowerBI', 'powerbi', 'PowerBIDefault', 'powerbidefault'],
      component: 'IFrameWidget',
      name: 'PowerBI'
    },
    {
      id: ['Appian', 'appian', 'appiandefault', 'AppianDefault'],
      component: 'AppianWidget',
      name: 'Appian'
    }
  ];
  it('should return widgets names', () => {
    const result = utils.getWidgetNames(widgetJson, true);
    expect(result).toEqual([
      { value: 'all', label: 'All' },
      { value: 'PowerBI', label: 'PowerBI' },
      { value: 'Appian', label: 'Appian' }
    ]);
  });
  it('should return widgets empty array', () => {
    const result = utils.getWidgetNames(widgetJson, false);
    expect(result).toEqual([
      { value: 'PowerBI', label: 'PowerBI' },
      { value: 'Appian', label: 'Appian' }
    ]);
  });
});
