import { mocked } from 'ts-jest/utils';
import * as storeUtils from '../storeUtils';

import store from '../store';

jest.mock('../store', () => ({
  getState: jest.fn()
}));

describe('storeUtils', () => {
  it('should return user_id', () => {
    const mockOidcState = {
      user_id: 'mock user_id'
    };
    mocked(store.getState).mockImplementation(
      () => ({ oidc: mockOidcState } as any)
    );
    const user_id = storeUtils.user_id();
    expect(user_id).toEqual(mockOidcState.user_id);
  });

  it('should return empty string', () => {
    const mockOidcState = {
      user_id: false
    };
    mocked(store.getState).mockImplementation(
      () => ({ oidc: mockOidcState } as any)
    );
    const user_id = storeUtils.user_id();
    expect(user_id).toEqual('');
  });
});
