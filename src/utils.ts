import axios, { AxiosRequestConfig } from 'axios';

import { API, ONE_HOUR, OFFSET } from './vars';

import { Item } from './models/todo/item.model';
import { Todo } from './models/app/todo.model';
import { WidgetConfigParam } from './models/marketplace/widgetconfigparam.model';
import { Attendee } from './models/todo/attendee.model';
import { WidgetComponentMap } from './models/marketplace/widgetcomponentmap.model';
import { Option } from './components/shared/Option';

// IE polyfill for lack of forEach on NodeList
if (
  typeof NodeList !== 'undefined' &&
  NodeList.prototype &&
  !NodeList.prototype.forEach
) {
  // @ts-ignore
  NodeList.prototype.forEach = Array.prototype.forEach;
}

// IE polyfill for Array.find()
if (!('find' in Array.prototype)) {
  // @ts-ignore
  // eslint-disable-next-line
  Array.prototype.find = function<T>(fn) {
    let filter = (this as Array<T>).filter(fn);

    return filter[0];
  };
}

// IE polyfill for String.startsWith
if (!('startsWith' in String.prototype)) {
  // @ts-ignore
  // eslint-disable-next-line
  String.prototype.startsWith = function(chars, pos = 0) {
    let str = (this as String).slice(pos, chars.length);

    return str === chars;
  };
}

if (!('finally' in Promise.prototype)) {
  require('promise.prototype.finally');
}

function fetchWrapper<T>(
  apiUri: string,
  opts: AxiosRequestConfig = {}
): Promise<T> {
  opts.url = !/graphql/.test(apiUri)
    ? `${API.url}/${API.version}/${apiUri}`
    : process.env.REACT_APP_GRAPHQL_URL ||
      `${API.url}/${API.version}/${apiUri}`;

  return axios
    .request<T>(opts)
    .then(response => response.data)
    .catch(error => {
      console.error(`Error fetching ${apiUri}: ${error}`);
      throw Error(error);
    });
}

function handleDateOffset(d: string | number | Date) {
  let dateFromString = new Date(d);

  if (
    typeof dateFromString !== 'object' ||
    dateFromString.toString() === 'Invalid Date'
  ) {
    return null;
  }

  if (typeof d === 'string') {
    if (/^\d{2,4}\D\d{2,4}\D\d{2,4}$/.test(d)) {
      return new Date(`${d}T00:00`);
    } else if (/^\d+$/.test(d) || /Z$/.test(d)) {
      return new Date(d);
    } else {
      return new Date(dateFromString.getTime() - OFFSET);
    }
  }
}

function formatDate(
  d: string | number | Date,
  long = false,
  opts: Intl.DateTimeFormatOptions = {}
) {
  let dateFromString = handleDateOffset(d) || new Date();

  let defaultOpts: Intl.DateTimeFormatOptions = {
    month: 'numeric',
    day: 'numeric'
  };

  if (long) {
    defaultOpts = {
      weekday: 'long',
      month: 'long',
      day: '2-digit',
      year: 'numeric'
    };
  }

  return new Intl.DateTimeFormat('en-US', {
    ...defaultOpts,
    ...opts
  }).format(dateFromString);
}

function parseTime(
  d: string | number | Date,
  opts: Intl.DateTimeFormatOptions = {}
) {
  let dateFromString = handleDateOffset(d) || new Date();

  let defaultOpts = {
    hour: 'numeric',
    minute: 'numeric',
    hour12: false
  };

  return new Intl.DateTimeFormat('en-US', {
    ...defaultOpts,
    ...opts
  }).format(dateFromString);
}

function getCurrentTime() {
  return new Intl.DateTimeFormat('en-US', {
    hour: 'numeric',
    minute: 'numeric',
    hour12: false
  }).format(new Date());
}

function todoTransform(todos: Item[], currentUserDisplayName?: string | null) {
  const currentDateTime = new Date();
  const currentDateInMS = new Date(
    Date.UTC(
      currentDateTime.getFullYear(),
      currentDateTime.getMonth(),
      currentDateTime.getDate(),
      0,
      0,
      0
    )
  ).getTime();
  const tomorrow = currentDateInMS + 24 * ONE_HOUR;
  let past: { category: string; tasks: Todo[] } = {
    category: 'Overdue',
    tasks: []
  };
  let present: { category: string; tasks: Todo[] } = {
    category: 'Due Today',
    tasks: []
  };
  let future: { category: string; tasks: Todo[] } = {
    category: '',
    tasks: []
  };

  todos &&
    todos.length &&
    todos.forEach(todo => {
      if (!todo.dueDate || !todo.startDate) return;

      let dateInMS = Date.parse(
        formatDate(todo.dueDate, true, {
          hour: 'numeric',
          minute: 'numeric',
          hour12: false
        })
      );
      let startDateInMS = Date.parse(
        formatDate(todo.startDate, true, {
          hour: 'numeric',
          minute: 'numeric',
          hour12: false
        })
      );

      let nowInMS = Date.parse(
        formatDate(new Date(), true, {
          hour: 'numeric',
          minute: 'numeric',
          hour12: false
        })
      );

      // Short-circuit for meetings that have already ended
      if (todo.itemType === 'M' && dateInMS < nowInMS) {
        return;
      }

      let td: Todo | undefined = undefined;

      if (todo.itemType === 'M') {
        const attendees =
          (todo.attendees &&
            todo.attendees.length &&
            Object.keys(todo.attendees)
              .map(i => todo.attendees && todo.attendees[+i])
              .filter((o): o is Attendee => !!o)) ||
          [];

        let status = 'None';

        if (todo.owner === currentUserDisplayName) {
          status = 'Accepted';
        }

        let attendee = attendees.filter(
          person => person.name === currentUserDisplayName
        );
        const attendee1 = attendee.length ? attendee[0] : null;
        status = (attendee1 && attendee1.responseStatus) || 'None';

        const length =
          todo.startDate && todo.dueDate
            ? (new Date(todo.dueDate).getTime() -
                new Date(todo.startDate).getTime()) /
              (1000 * 60)
            : 0;
        td = {
          type: todo.itemType,
          body: todo.subjectLine,
          id: todo.id,
          meetingInfo: {
            subject: todo.subjectLine,
            date: formatDate(dateInMS, true),
            startTime: parseTime(startDateInMS),
            endTime: parseTime(dateInMS),
            length,
            allDay: length >= 1440, // One or more days
            organizer: todo.owner,
            attendees,
            status,
            body: todo.body,
            webLink: todo.webLink,
            onlineMeetingUrl: todo.onlineMeetingUrl
          }
        };
      } else if (todo.itemType === 'P') {
        td = {
          type: todo.itemType,
          body: todo.subjectLine,
          id: todo.id,
          newcapurl: todo.newcapurl
        };
      } else if (todo.itemType === 'T') {
        td = {
          type: todo.itemType,
          body: todo.subjectLine,
          id: todo.id
        };
      }

      if (td) {
        if (dateInMS && dateInMS >= currentDateInMS && dateInMS < tomorrow) {
          present.tasks.push(td);
        } else if (!dateInMS || dateInMS >= tomorrow) {
          future.tasks.push(td);
        } else {
          past.tasks.push(td);
        }
      }
    });

  let retArray = [];

  if (past.tasks.length) {
    retArray.push(past);
  }
  if (present.tasks.length) {
    retArray.push(present);
  }
  if (future.tasks.length) {
    retArray.push(future);
  }

  return retArray;
}

function getParentNodeByClass(
  $el: HTMLElement,
  matchClass: string,
  cancelNode = 'body'
): HTMLElement | null {
  if ($el && $el.classList && $el.classList.contains(matchClass)) {
    return $el;
  } else if (cancelNode === $el.nodeName || !$el.parentElement) {
    return null;
  }

  return getParentNodeByClass($el.parentElement, matchClass, cancelNode);
}

function uomConverter(uom: string) {
  switch (uom) {
    case 'Percent':
      return '%';
    default:
      return uom;
  }
}

function shortenString(str: string, cutAt: number) {
  return str.length < cutAt ? str : str.slice(0, cutAt) + '...';
}

function transformConfigParams(params: WidgetConfigParam[]) {
  let transform: { [key: string]: string | null | undefined } = {};

  params.forEach(p => {
    if (p.key) {
      transform[p.key] = p.value;
    }
  });

  return transform;
}

function getWidgetTypeArray(widgetJson: WidgetComponentMap[], value: string) {
  const widget = widgetJson.find(t => t.id && t.id.find(id => value === id));
  let widgetTypeArr = widget && widget.id;
  return widgetTypeArr ? widgetTypeArr : [];
}

function getWidgetNames(widgetJson: WidgetComponentMap[], all: boolean) {
  let widgetNames = widgetJson.map(item => item.name);
  let options: Option[] = [];
  widgetNames.forEach(item => {
    item && options.push({ value: item, label: item });
  });
  if (all) {
    options.unshift({ value: 'all', label: 'All' });
  }
  return options;
}

/// https://stackoverflow.com/a/8809472/82199
function generateUUID() {
  // Public Domain/MIT
  var d = new Date().getTime(); //Timestamp
  var d2 = (performance && performance.now && performance.now() * 1000) || 0; //Time in microseconds since page-load or 0 if unsupported
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16; //random number between 0 and 16
    if (d > 0) {
      //Use timestamp until depleted
      r = (d + r) % 16 | 0;
      d = Math.floor(d / 16);
    } else {
      //Use microseconds since page-load if supported
      r = (d2 + r) % 16 | 0;
      d2 = Math.floor(d2 / 16);
    }
    return (c === 'x' ? r : (r & 0x3) | 0x8).toString(16);
  });
}

export {
  fetchWrapper,
  todoTransform,
  getParentNodeByClass,
  uomConverter,
  formatDate,
  handleDateOffset,
  parseTime,
  getCurrentTime,
  shortenString,
  transformConfigParams,
  getWidgetTypeArray,
  getWidgetNames,
  generateUUID
};
