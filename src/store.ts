import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware, RouterState } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { loadUser as loadOidcUser } from 'redux-oidc';

import createRootReducer from './reducers';
import thunk from 'redux-thunk';
import userManager from './userManager';
import { initialAppState, AppState } from './reducers/appReducer';
import { OidcState, initialOidcState } from './reducers/customOidcReducer';
import {
  DashboardState,
  initialDashboardState
} from './reducers/dashboardReducer';

export const history = createBrowserHistory();

export interface StoreState {
  router?: RouterState;
  oidc: OidcState;
  app: AppState;
  dashboard: DashboardState;
}

export const initialState: StoreState = {
  oidc: initialOidcState,
  app: initialAppState,
  dashboard: initialDashboardState
};

var middleware = [routerMiddleware(history), thunk];

if (process.env.NODE_ENV === 'development') {
  const { logger } = require('redux-logger');

  middleware.push(logger);
}

export const store = createStore(
  createRootReducer(history),
  initialState,
  compose(applyMiddleware(...middleware))
);

export const loadUser = loadOidcUser(store, userManager);

export default store;
