import axios from 'axios';
import store from '../store';
import { setComponentlist } from '../actions/marketplace/actions';
import { WidgetComponentMap } from '../models/marketplace/widgetcomponentmap.model';

export function getComponents() {
  axios
    .get<WidgetComponentMap[]>('../json/widgetConfig.json')
    .then(json => store.dispatch(setComponentlist(json.data)));
}
