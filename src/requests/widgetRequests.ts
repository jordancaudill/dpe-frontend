import { fetchWrapper } from '../utils';
import { user_id } from '../storeUtils';
import { URLs } from '../vars';
import { Widget } from '../models/marketplace/widget.model';

function createWidget(body: Widget) {
  return fetchWrapper<Widget>(`${URLs.createWidget}?userId=${user_id()}`, {
    method: 'POST',
    data: body
  });
}

function updateWidget(modelId: string, body: Widget) {
  return fetchWrapper<Widget>(
    `${URLs.updateWidget}${modelId}?userId=${user_id()}`,
    {
      method: 'PUT',
      data: body
    }
  );
}

export { createWidget, updateWidget };
