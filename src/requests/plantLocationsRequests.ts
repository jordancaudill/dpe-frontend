import store from '../store';
import { setPlantLocations } from '../actions/marketplace/actions';
import { fetchWrapper } from '../utils';
import { GraphResponse } from '../models/hl_data/graphresponse.model';
import { Plant } from '../models/hl_data/plant.model';

const query = `
  query {
    plants {
      id
      name
    }
  }
`;

const getPlantLocations = () => {
  return fetchWrapper<GraphResponse<{ plants: Plant[] }>>('hld/graphql/', {
    method: 'POST',
    data: { query }
  }).then(json => {
    return store.dispatch(setPlantLocations(json.data.plants));
  });
};

export default getPlantLocations;
