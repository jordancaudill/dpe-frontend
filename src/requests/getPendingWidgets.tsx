import store from '../store';
import { updateWidgetRequests } from '../actions/marketplace/actions';
import { fetchWrapper } from '../utils';
import { user_id } from '../storeUtils';
import { Widget } from '../models/marketplace/widget.model';

const limit = 99;

function getPendingWidgets() {
  //URL to retrieve widgets for specified user ID with a status of U i.e. requests to review
  const URL = `marketplace/widgets?userId=${user_id()}&status=U&limit=${limit}&since=1`;

  return fetchWrapper<Widget[]>(URL).then(json => {
    store.dispatch(updateWidgetRequests(json));

    return json;
  });
}

export { getPendingWidgets };
