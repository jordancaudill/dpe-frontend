import axios from 'axios';
import store from '../store';
import { setQuickLinks } from '../actions/marketplace/actions';
import { Links } from '../models/app/link.model';

export function getQuickLinks() {
  axios
    .get<Links>('/json/links.json')
    .then(json => store.dispatch(setQuickLinks(json.data)));
}
