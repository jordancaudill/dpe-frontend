import { fetchWrapper } from '../utils';
import { user_id } from '../storeUtils';
import { URLs } from '../vars';
import { store, loadUser } from '../store';
import { userDetailsLoading, enrichUserFromApi } from '../actions/user/actions';
import { User } from '../models/todo/user.model';
import { UserDashboard } from '../models/marketplace/userdashboard.model';

const getUser = async function(userId?: string) {
  if (!userId) {
    await loadUser; // loadUser sets the default store.oidc.user_id if it is not provided
    userId = user_id();
  }
  store.dispatch(userDetailsLoading(true));

  try {
    const json = await fetchWrapper<User>(`${URLs.getUser}/${userId}`);
    store.dispatch(enrichUserFromApi(json));
    store.dispatch(userDetailsLoading(false));
  } catch (err) {
    console.error(err);

    store.dispatch(userDetailsLoading(false));
  }
};

const updateDashboard = function(dashboard: UserDashboard) {
  return fetchWrapper<UserDashboard>(
    `${URLs.updateDashboard}/${dashboard.dashboardId}`,
    {
      method: 'PUT',
      data: dashboard
    }
  );
};

export { getUser, updateDashboard };
