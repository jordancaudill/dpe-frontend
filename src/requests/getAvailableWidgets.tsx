import store from '../store';
import { updateMarketplaceWidgets } from '../actions/marketplace/actions';
import { fetchWrapper } from '../utils';
import { user_id } from '../storeUtils';
import { Widget } from '../models/marketplace/widget.model';

const limit = 99;

function getAvailableWidgets() {
  //URL to retrieve widgets for specified user ID with a status of A i.e. all available widgets
  const URL = `marketplace/widgets?userId=${user_id()}&status=A&limit=${limit}&since=1`;

  return fetchWrapper<Widget[]>(URL).then(json => {
    store.dispatch(updateMarketplaceWidgets(json));
    return json;
  });
}

export { getAvailableWidgets };
