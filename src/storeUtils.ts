import store from './store';

// Return a string containing the user id (C112233@exelonds.com) or empty
export function user_id() {
  const oidc = store.getState().oidc;
  return oidc.user_id || '';
}
