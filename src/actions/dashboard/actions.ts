import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'redux';
import { Layouts } from 'react-grid-layout';

import * as Types from './types';

import { fetchWrapper } from '../../utils';
import { StoreState } from '../../store';
import { user_id } from '../../storeUtils';
import { receiveError } from '../user/actions';

import { UserDashboard } from '../../models/marketplace/userdashboard.model';
import { DashboardWidgetOrder } from '../../models/marketplace/dashboardwidgetorder.model';

export function setWidgetList(
  widgetList: UserDashboard
): Types.DashboardActionTypes {
  return {
    type: Types.SET_WIDGET_LIST,
    payload: widgetList
  };
}

export function addDashboardWidget(
  payload: DashboardWidgetOrder
): Types.DashboardActionTypes {
  return {
    type: Types.ADD_WIDGET,
    payload
  };
}

export function removeDashboardWidget(
  widgetId: string
): Types.DashboardActionTypes {
  return {
    type: Types.REMOVE_WIDGET,
    payload: widgetId
  };
}

export function widgetsLoading(bool: boolean): Types.DashboardActionTypes {
  return {
    type: Types.WIDGETS_LOADING,
    payload: bool
  };
}

export function setGridBreakpoint(
  payload: Types.Breakpoint
): Types.DashboardActionTypes {
  return {
    type: Types.SET_GRID_BREAKPOINT,
    payload
  };
}

export function setGridCols(payload: number): Types.DashboardActionTypes {
  return {
    type: Types.SET_GRID_COLS,
    payload
  };
}

export function setGridLayout(payload: Layouts): Types.DashboardActionTypes {
  return {
    type: Types.SET_GRID_LAYOUT,
    payload
  };
}

export function fetchWidgets() {
  return (dispatch: ThunkDispatch<StoreState, undefined, Action>) => {
    dispatch(widgetsLoading(true));

    fetchWrapper<UserDashboard>(`marketplace/dashboard/${user_id()}`)
      .then(async items => {
        if (items.dashboardOrder) {
          dispatch(setWidgetList(items));
          dispatch(widgetsLoading(false));
        }
      })
      .catch((err: Error) => {
        dispatch(receiveError(err));
        dispatch(widgetsLoading(false));
      });
  };
}
