import { Layouts } from 'react-grid-layout';

import { UserDashboard } from '../../models/marketplace/userdashboard.model';
import { DashboardWidgetOrder } from '../../models/marketplace/dashboardwidgetorder.model';

export type Breakpoint = 'lg' | 'md' | 'sm' | 'xs' | 'xxs';

export const SET_WIDGET_LIST = 'SET_WIDGET_LIST';
export const ADD_WIDGET = 'ADD_WIDGET';
export const REMOVE_WIDGET = 'REMOVE_WIDGET';
export const WIDGETS_LOADING = 'WIDGETS_LOADING';
export const SET_GRID_BREAKPOINT = 'SET_GRID_BREAKPOINT';
export const SET_GRID_COLS = 'SET_GRID_COLS';
export const SET_GRID_LAYOUT = 'SET_GRID_LAYOUT';

interface SetWidgetListAction {
  type: typeof SET_WIDGET_LIST;
  payload: UserDashboard;
}

interface AddWidgetAction {
  type: typeof ADD_WIDGET;
  payload: DashboardWidgetOrder;
}

interface RemoveWidgetAction {
  type: typeof REMOVE_WIDGET;
  payload: string;
}

interface WidgetsLoadingAction {
  type: typeof WIDGETS_LOADING;
  payload: boolean;
}

interface SetGridBreakpointAction {
  type: typeof SET_GRID_BREAKPOINT;
  payload: Breakpoint;
}
interface SetGridColsAction {
  type: typeof SET_GRID_COLS;
  payload: number;
}
interface SetGridLayoutAction {
  type: typeof SET_GRID_LAYOUT;
  payload: Layouts;
}

export type DashboardActionTypes =
  | SetWidgetListAction
  | AddWidgetAction
  | RemoveWidgetAction
  | WidgetsLoadingAction
  | SetGridBreakpointAction
  | SetGridColsAction
  | SetGridLayoutAction;
