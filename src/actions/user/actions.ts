import { User as TodoUser } from '../../models/todo/user.model';

import {
  UserActionTypes,
  ENRICH_USER_FROM_API,
  USER_DETAILS_LOADING,
  RECEIVE_USER_ERROR
} from './types';

export function enrichUserFromApi(apiUserInfo: TodoUser): UserActionTypes {
  return {
    type: ENRICH_USER_FROM_API,
    payload: { ...apiUserInfo }
  };
}

export function userDetailsLoading(bool: boolean): UserActionTypes {
  return {
    type: USER_DETAILS_LOADING,
    payload: bool
  };
}

export function receiveError(error: Error): UserActionTypes {
  return {
    type: RECEIVE_USER_ERROR,
    payload: error
  };
}
