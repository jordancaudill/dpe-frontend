import { User as OidcUser } from 'oidc-client';
import { User as TodoUser } from '../../models/todo/user.model';

export const SET_USER_FROM_OIDC = 'redux-oidc/USER_FOUND';
export const ENRICH_USER_FROM_API = 'ENRICH_USER_FROM_API';
export const USER_DETAILS_LOADING = 'USER_DETAILS_LOADING';
export const RECEIVE_USER_ERROR = 'RECEIVE_USER_ERROR';

interface SetUserFromOidcAction {
  type: typeof SET_USER_FROM_OIDC;
  payload: OidcUser;
}

interface EnrichUserFromApiAction {
  type: typeof ENRICH_USER_FROM_API;
  payload: TodoUser;
}

interface UserDetailsLoadingAction {
  type: typeof USER_DETAILS_LOADING;
  payload: boolean;
}

interface ReceiveUserErrorAction {
  type: typeof RECEIVE_USER_ERROR;
  payload: Error;
}

export type UserActionTypes =
  | SetUserFromOidcAction
  | EnrichUserFromApiAction
  | UserDetailsLoadingAction
  | ReceiveUserErrorAction;
