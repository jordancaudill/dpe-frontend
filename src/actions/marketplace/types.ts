import { Widget } from '../../models/marketplace/widget.model';
import { Plant } from '../../models/hl_data/plant.model';
import { Links } from '../../models/app/link.model';
import { WidgetComponentMap } from '../../models/marketplace/widgetcomponentmap.model';

export const SET_QUICK_LINKS = 'SET_QUICK_LINKS';
export const UPDATE_MARKETPLACE_WIDGETS = 'UPDATE_MARKETPLACE_WIDGETS';
export const SET_PLANT_LOCATIONS = 'SET_PLANT_LOCATIONS';
export const UPDATE_WIDGET_REQUESTS = 'UPDATE_WIDGET_REQUESTS';
export const WIDGET_REQUEST_COUNT = 'WIDGET_REQUEST_COUNT';
export const SET_COMPONENT_LIST = 'SET_COMPONENT_LIST';

interface SetQuickLinksAction {
  type: typeof SET_QUICK_LINKS;
  payload: Links;
}
interface SetComponentListAction {
  type: typeof SET_COMPONENT_LIST;
  payload: WidgetComponentMap[];
}
interface UpdateMarketplaceWidgetsAction {
  type: typeof UPDATE_MARKETPLACE_WIDGETS;
  payload: Widget[];
}

interface SetPlantLocationsAction {
  type: typeof SET_PLANT_LOCATIONS;
  payload: Plant[];
}

interface UpdateWidgetRequestsAction {
  type: typeof UPDATE_WIDGET_REQUESTS;
  payload: Widget[];
}

interface WidgetRequestCountAction {
  type: typeof WIDGET_REQUEST_COUNT;
  payload: number;
}

export type MarketplaceActionTypes =
  | SetQuickLinksAction
  | UpdateMarketplaceWidgetsAction
  | SetPlantLocationsAction
  | UpdateWidgetRequestsAction
  | WidgetRequestCountAction
  | SetComponentListAction;
