import store from '../../store';
import { Widget } from '../../models/marketplace/widget.model';
import { Plant } from '../../models/hl_data/plant.model';
import { Links } from '../../models/app/link.model';
import { WidgetComponentMap } from '../../models/marketplace/widgetcomponentmap.model';
import {
  MarketplaceActionTypes,
  SET_QUICK_LINKS,
  UPDATE_MARKETPLACE_WIDGETS,
  SET_PLANT_LOCATIONS,
  UPDATE_WIDGET_REQUESTS,
  WIDGET_REQUEST_COUNT,
  SET_COMPONENT_LIST
} from './types';

export function setQuickLinks(payload: Links): MarketplaceActionTypes {
  return {
    type: SET_QUICK_LINKS,
    payload
  };
}

export function setComponentlist(
  payload: WidgetComponentMap[]
): MarketplaceActionTypes {
  return {
    type: SET_COMPONENT_LIST,
    payload
  };
}
export function updateMarketplaceWidgets(
  payload: Widget[]
): MarketplaceActionTypes {
  return {
    type: UPDATE_MARKETPLACE_WIDGETS,
    payload
  };
}

export function setPlantLocations(
  plantLocations: Plant[]
): MarketplaceActionTypes {
  return {
    type: SET_PLANT_LOCATIONS,
    payload: plantLocations
  };
}

export function reviewWidgetRequestCount(
  payload: number
): MarketplaceActionTypes {
  return {
    type: WIDGET_REQUEST_COUNT,
    payload
  };
}

export function updateWidgetRequests(
  payload: Widget[]
): MarketplaceActionTypes {
  store.dispatch(reviewWidgetRequestCount(payload.length));

  return {
    type: UPDATE_WIDGET_REQUESTS,
    payload
  };
}
