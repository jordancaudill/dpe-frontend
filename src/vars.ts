const crypto = require('crypto');

const API = {
  url: process.env.REACT_APP_API_URL || 'localhost:3000',
  version: process.env.REACT_APP_API_VERSION || 'v1'
};

function guid() {
  //@ts-ignore
  const cryptObj = window.crypto || window.msCrypto || crypto; // For IE11
  const buf = new Uint16Array(8);
  cryptObj.getRandomValues(buf);
  function s4(num: number) {
    var ret = num.toString(16);
    while (ret.length < 4) {
      ret = '0' + ret;
    }
    return ret;
  }
  return (
    s4(buf[0]) +
    s4(buf[1]) +
    '-' +
    s4(buf[2]) +
    '-' +
    s4(buf[3]) +
    '-' +
    s4(buf[4]) +
    '-' +
    s4(buf[5]) +
    s4(buf[6]) +
    s4(buf[7])
  );
}

const URLs = {
  getUser: 'todoLists/userInfo',
  updateDashboard: 'marketplace/dashboard',
  createWidget: 'marketplace/widget',
  updateWidget: 'marketplace/widget/'
};

const ONE_HOUR = 60 * 60 * 1000;
const OFFSET = new Date().getTimezoneOffset() * 60 * 1000;

export { API, guid, URLs, ONE_HOUR, OFFSET };
