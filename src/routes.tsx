import * as React from 'react';
import { Switch as Router, Route } from 'react-router';

import HomePage from './components/HomePage';
import CallbackPage from './components/Callback';
import ErrorPage from './components/ErrorPage';

export default function Routes() {
  return (
    <Router>
      <Route exact path="/" component={HomePage} />

      <Route exact path="/error" component={ErrorPage} />

      <Route exact path="/callback" component={CallbackPage} />
    </Router>
  );
}
