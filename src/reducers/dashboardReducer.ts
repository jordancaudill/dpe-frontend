import { Layouts, Layout } from 'react-grid-layout';

import * as Types from '../actions/dashboard/types';
import {
  UPDATE_MARKETPLACE_WIDGETS,
  MarketplaceActionTypes
} from '../actions/marketplace/types';
import { transformConfigParams } from '../utils';
import { updateDashboard } from '../requests/userRequests';

import { DashboardWidgetOrder } from '../models/marketplace/dashboardwidgetorder.model';
import { Widget } from '../models/marketplace/widget.model';
import { WidgetConfigParam } from '../models/marketplace/widgetconfigparam.model';

export interface DashboardState {
  dashboardLoading: boolean;
  userId?: string | null;
  dashboardId?: string | null;
  widgetDetail: { [modelId: string]: Widget };
  dashboardWidgets: DashboardWidgetOrder[];
  breakpoint: Types.Breakpoint;
  cols: { [breakpoint in Types.Breakpoint]: number };
  layouts: Layouts;
}

export const initialDashboardState: DashboardState = {
  dashboardLoading: false,
  widgetDetail: {},
  dashboardWidgets: [],
  breakpoint: 'lg' as Types.Breakpoint,
  cols: { lg: 12, md: 10, sm: 6, xs: 4, xxs: 2 },
  layouts: { lg: [] }
};

export default function dashboardReducer(
  state = initialDashboardState,
  action: Types.DashboardActionTypes | MarketplaceActionTypes
): DashboardState {
  switch (action.type) {
    case Types.WIDGETS_LOADING:
      return {
        ...state,
        dashboardLoading: action.payload === true
      };
    case Types.SET_WIDGET_LIST: {
      const dashboardWidgets = getDashboardWidgets(
        action.payload.dashboardOrder || [],
        state.widgetDetail
      );
      return {
        ...state,
        userId: action.payload.userId,
        dashboardId: action.payload.dashboardId,
        dashboardWidgets,
        layouts: generateLayout(dashboardWidgets)
      };
    }
    case Types.ADD_WIDGET: {
      const dashboardWidgets = getDashboardWidgets(
        [...state.dashboardWidgets, action.payload],
        state.widgetDetail
      );
      return {
        ...state,
        dashboardWidgets,
        layouts: generateLayout(dashboardWidgets)
      };
    }
    case Types.REMOVE_WIDGET: {
      const dashboardWidgets = getDashboardWidgets(
        state.dashboardWidgets.filter(w => w.instanceId !== action.payload),
        state.widgetDetail
      );
      return {
        ...state,
        dashboardWidgets,
        layouts: generateLayout(dashboardWidgets)
      };
    }
    case UPDATE_MARKETPLACE_WIDGETS: {
      const widgetDetail = action.payload.reduce(
        (obj, item) => {
          if (item.modelId) obj[item.modelId] = item;
          return obj;
        },
        {} as { [modelId: string]: Widget }
      );
      const dashboardWidgets = getDashboardWidgets(
        state.dashboardWidgets,
        widgetDetail
      );
      return {
        ...state,
        dashboardWidgets,
        widgetDetail,
        layouts: generateLayout(dashboardWidgets)
      };
    }
    case Types.SET_GRID_BREAKPOINT:
      return {
        ...state,
        breakpoint: action.payload
      };
    case Types.SET_GRID_LAYOUT:
      updateDashboardLayout(state.dashboardWidgets, action.payload);
      return {
        ...state,
        layouts: action.payload
      };
  }
  return state;

  function updateDashboardLayout(
    dashboardWidgets?: DashboardWidgetOrder[],
    layouts?: Layouts
  ) {
    if (!dashboardWidgets || !layouts) return;

    const dashboardOrder = dashboardWidgets.map((widget, i) => {
      let widgetInstanceConfigparms: WidgetConfigParam[] = [];
      const layout = layouts[state.breakpoint].find(
        l => l.i === widget.instanceId
      );
      if (layout) {
        updateInstanceParam(
          widgetInstanceConfigparms,
          'x',
          layout.x.toString()
        );
        updateInstanceParam(
          widgetInstanceConfigparms,
          'y',
          layout.y.toString()
        );
        updateInstanceParam(
          widgetInstanceConfigparms,
          'w',
          layout.w.toString()
        );
        updateInstanceParam(
          widgetInstanceConfigparms,
          'h',
          layout.h.toString()
        );
      }
      return {
        instanceId: widget.instanceId,
        modelId: widget.modelId,
        order: i,
        widgetInstanceConfigparms
      };
    });

    updateDashboard({
      userId: state.userId,
      dashboardId: state.dashboardId,
      dashboardOrder
    });
  }

  function updateInstanceParam(
    params: WidgetConfigParam[],
    key: string,
    value: string
  ) {
    let param = params.find(p => p.key === key);
    if (param) {
      param.value = value;
    } else {
      params.push(new WidgetConfigParam(key, value));
    }
  }

  function getDashboardWidgets(
    widgetList: DashboardWidgetOrder[],
    widgetDetail: { [modelId: string]: Widget } = {}
  ) {
    return widgetList
      .filter(
        widget =>
          widget &&
          widget.modelId &&
          (!Object.keys(widgetDetail).length ||
            widgetDetail[widget.modelId as string])
      )
      .map(
        widget =>
          new DashboardWidgetOrder({
            ...widgetDetail[widget.modelId as string],
            modelId: widget.modelId,
            instanceId: widget.instanceId,
            order: widget.order,
            widgetInstanceConfigparms: widget.widgetInstanceConfigparms
          })
      );
  }

  function generateLayout(dashboardWidgets: DashboardWidgetOrder[]) {
    let layouts: Layout[] = [];
    dashboardWidgets.forEach((widget, i) => {
      const size = getWidgetSize(widget);
      const cur =
        state.layouts &&
        state.layouts[state.breakpoint].find(
          layout => layout.i === widget.instanceId
        );
      const pos = getOpenLayout(layouts, size.w, size.h);
      const layout = cur || {
        i: widget.instanceId,
        w: size.w,
        h: size.h,
        x: size.x || pos.x,
        y: size.y || pos.y
      };
      layouts.push(layout);
    });
    return {
      [state.breakpoint]: layouts
    };
  }

  function getOpenLayout(layouts: Layout[], w: number, h: number) {
    const xs = [0]
      .concat(layouts.map(l => l.x), layouts.map(l => l.x + l.w))
      .filter((x, i, self) => self.indexOf(x) === i)
      .filter(x => x + w <= state.cols[state.breakpoint])
      .sort((a, b) => a - b);
    const ys = [0]
      .concat(layouts.map(l => l.y), layouts.map(l => l.y + l.h))
      .filter((y, i, self) => self.indexOf(y) === i)
      .sort((a, b) => a - b);

    for (let y of ys) {
      for (let x of xs) {
        const r1 = {
          left: x,
          right: x + w,
          top: y,
          bottom: y + h
        };

        const conflict = layouts.find(l =>
          intersect(r1, {
            left: l.x,
            right: l.x + l.w,
            top: l.y,
            bottom: l.y + l.h
          })
        );

        if (!conflict) {
          return { x, y };
        }
      }
    }
    return {
      x: Infinity,
      y: Infinity
    };
  }

  type Rect = {
    top: number;
    left: number;
    bottom: number;
    right: number;
  };

  function intersect(a: Rect, b: Rect) {
    return (
      a.left < b.right &&
      b.left < a.right &&
      a.top < b.bottom &&
      b.top < a.bottom
    );
  }

  function getWidgetSize(widget: DashboardWidgetOrder) {
    let config = {
      ...transformConfigParams(widget.widgetConfigparms || []),
      ...transformConfigParams(widget.widgetInstanceConfigparms || [])
    };

    return {
      x: config['x'] && +config['x'],
      y: config['y'] && +config['y'],
      w: getWidgetWidth(config),
      h: getWidgetHeight(config)
    };
  }

  function getWidgetWidth(config: {
    [x: string]: string | null | undefined;
  }): number {
    if (config.hasOwnProperty('w') && !isNaN(Number(config['w'])))
      return Number(config['w']);
    switch (config.widgetSize) {
      case 'small':
        return 3;
      case 'medium':
        return 6;
      case 'large':
        return 9;
      case 'extra-large':
      default:
        return 12;
    }
  }

  function getWidgetHeight(config: {
    [x: string]: string | null | undefined;
  }): number {
    if (config.hasOwnProperty('h') && !isNaN(Number(config['h'])))
      return Number(config['h']);
    return 6;
  }
}
