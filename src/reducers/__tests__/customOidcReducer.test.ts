import { mocked } from 'ts-jest/utils';
import * as customOidcReducer from '../customOidcReducer';
import { SET_USER_FROM_OIDC } from '../../actions/user/types';
import { API } from '../../vars';

import axios from 'axios';

jest.mock('axios', () => ({
  interceptors: {
    request: {
      eject: jest.fn(),
      use: jest.fn()
    }
  }
}));

describe('customOidcReducer', () => {
  const action = {
    type: SET_USER_FROM_OIDC,
    payload: {
      profile: {
        preferred_username: 'test user'
      }
    }
  };

  beforeEach(() => {
    // clear the interceptor
    mocked(axios.interceptors.request.use).mockImplementationOnce(() => 0);
    customOidcReducer.default(undefined, action as any);
    jest.clearAllMocks();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should return intiial state', () => {
    const nextState = customOidcReducer.default(undefined, { type: 'test' });
    expect(nextState).toEqual(customOidcReducer.initialOidcState);
  });

  it('should return same state', () => {
    const state = {
      isLoadingUser: false,
      isAuthenticated: true
    };
    const nextState = customOidcReducer.default(state, { type: 'test' });
    expect(nextState).toEqual(state);
  });

  it('should set isAuthenticated', () => {
    const state = {
      isLoadingUser: true,
      isAuthenticated: false
    };
    const nextState = customOidcReducer.default(state, action as any);
    expect(nextState.isAuthenticated).toBe(true);
  });

  it('should not set isAuthenticated when missing profile', () => {
    const action = {
      type: SET_USER_FROM_OIDC,
      payload: {}
    };
    const nextState = customOidcReducer.default(undefined, action as any);
    expect(nextState.isAuthenticated).toBe(false);
  });

  it('should not set isAuthenticated when missing name', () => {
    const action = {
      type: SET_USER_FROM_OIDC,
      payload: {
        profile: {}
      }
    };
    const nextState = customOidcReducer.default(undefined, action as any);
    expect(nextState.isAuthenticated).toBe(false);
  });

  it('should not set isAuthenticated when empty name', () => {
    const action = {
      type: SET_USER_FROM_OIDC,
      payload: {
        profile: {
          preferred_username: ''
        }
      }
    };
    const nextState = customOidcReducer.default(undefined, action as any);
    expect(nextState.isAuthenticated).toBe(false);
  });

  it('should not set isAuthenticated when name null', () => {
    const action = {
      type: SET_USER_FROM_OIDC,
      payload: {
        profile: {
          preferred_username: null
        }
      }
    };
    const nextState = customOidcReducer.default(undefined, action as any);
    expect(nextState.isAuthenticated).toBe(false);
  });

  it('should set user_id', () => {
    const nextState = customOidcReducer.default(undefined, action as any);
    expect(nextState.user_id).toBe(action.payload.profile.preferred_username);
  });

  it('should not set user_id when name null', () => {
    const action = {
      type: SET_USER_FROM_OIDC,
      payload: {
        profile: {
          preferred_username: null
        }
      }
    };
    const nextState = customOidcReducer.default(undefined, action as any);
    expect(nextState.user_id).toBe('');
  });

  it('should not set user_id when name empty', () => {
    const action = {
      type: SET_USER_FROM_OIDC,
      payload: {
        profile: {
          preferred_username: ''
        }
      }
    };
    const nextState = customOidcReducer.default(undefined, action as any);
    expect(nextState.user_id).toBe('');
  });

  it('should not set user_id when name missing', () => {
    const action = {
      type: SET_USER_FROM_OIDC,
      payload: {
        profile: {}
      }
    };
    const nextState = customOidcReducer.default(undefined, action as any);
    expect(nextState.user_id).toBe('');
  });

  it('should not set user_id when profile missing', () => {
    const action = {
      type: SET_USER_FROM_OIDC,
      payload: {}
    };
    const nextState = customOidcReducer.default(undefined, action as any);
    expect(nextState.user_id).toBe('');
  });

  it('should set request interceptor', () => {
    mocked(axios.interceptors.request.use).mockImplementation(() =>
      new Date().getTime()
    );
    customOidcReducer.default(undefined, action as any);
    expect(axios.interceptors.request.eject).toBeCalledTimes(0);
    expect(axios.interceptors.request.use).toBeCalledTimes(1);
  });

  it('should reset request interceptor', () => {
    mocked(axios.interceptors.request.use).mockImplementation(() =>
      new Date().getTime()
    );
    const state = customOidcReducer.default(undefined, action as any);
    customOidcReducer.default(state, action as any);
    expect(axios.interceptors.request.eject).toBeCalledTimes(1);
    expect(axios.interceptors.request.use).toBeCalledTimes(2);
  });

  describe('setUserHeaders', () => {
    it('should throw exception when not authenticated', () => {
      const state = {
        isLoadingUser: false,
        isAuthenticated: false
      };
      const config = {};
      expect(() => customOidcReducer.setUserHeaders(state)(config)).toThrow();
    });

    it('should add auth headers', () => {
      const state = {
        isLoadingUser: false,
        isAuthenticated: true,
        user: {
          id_token: 'id_token',
          access_token: 'access_token'
        }
      };
      const config = {
        url: `${API.url}/test/url`
      };
      const res = customOidcReducer.setUserHeaders(state as any)(config);
      expect(res.headers).toBeDefined();
      expect(res.headers['Content-Type']).toBe('application/json');
      expect(res.headers.Authorization).toBe(`Bearer ${state.user.id_token}`);
      expect(res.headers.token).toBe(state.user.access_token);
    });

    it('should not add auth headers', () => {
      const state = {
        isLoadingUser: false,
        isAuthenticated: true,
        user: {
          id_token: 'id_token',
          access_token: 'access_token'
        }
      };
      const config = {
        url: 'https://example.com/test/url'
      };
      const res = customOidcReducer.setUserHeaders(state as any)(config);
      expect(res.headers).not.toBeDefined();
    });
  });
});
