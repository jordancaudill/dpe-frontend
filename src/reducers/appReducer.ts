import userManager from '../userManager';

import {
  MarketplaceActionTypes,
  SET_QUICK_LINKS,
  UPDATE_MARKETPLACE_WIDGETS,
  SET_PLANT_LOCATIONS,
  UPDATE_WIDGET_REQUESTS,
  WIDGET_REQUEST_COUNT,
  SET_COMPONENT_LIST
} from '../actions/marketplace/types';
import {
  UserActionTypes,
  SET_USER_FROM_OIDC,
  ENRICH_USER_FROM_API
} from '../actions/user/types';

import { Widget } from '../models/marketplace/widget.model';
import { Plant } from '../models/hl_data/plant.model';
import { User } from '../models/app/user.model';
import { Links } from '../models/app/link.model';
import { WidgetComponentMap } from '../models/marketplace/widgetcomponentmap.model';

export interface AppState {
  quickLinks: Links;
  user: User | null;
  plantLocations: Plant[];
  marketplaceWidgets: Widget[];
  reviewWidgetRequestCount: number;
  reviewWidgetRequests: Widget[];
  componentLists: WidgetComponentMap[];
}

export const initialAppState: AppState = {
  quickLinks: {},
  user: null,
  plantLocations: [],
  marketplaceWidgets: [],
  reviewWidgetRequestCount: 0,
  reviewWidgetRequests: [],
  componentLists: []
};

export default function appReducer(
  state = initialAppState,
  action: MarketplaceActionTypes | UserActionTypes
): AppState {
  switch (action.type) {
    /* USER */
    // Set as much data as possible from the initial OIDC call
    // so we have a faster experience
    case SET_USER_FROM_OIDC:
      if (action.payload.expired) {
        userManager.signinRedirect();
      }

      const pr = action.payload.profile;
      const firstName = pr.given_name;
      const lastName = pr.family_name;
      return {
        ...state,
        user: {
          id: pr.preferred_username || 'not_found',
          title: '',
          displayName: `${firstName} ${lastName}`,
          firstName: firstName,
          lastName: lastName,
          email: pr.preferred_username,
          plantLocation: 'Unknown',
          lang: 'en',
          phone: { business: [''], mobile: '' },
          userImage: null,
          role: pr.role,
          isAdmin: pr.role === 'admin'
        }
      };
    // This will not replace the fields, but try to complete empty fields
    // from the new payload
    case ENRICH_USER_FROM_API:
      const json = action.payload;
      const mergedUser = {
        ...state.user,
        id: json.userPrincipalName || 'not_found',
        apiId: json.id,
        title: json.jobTitle,
        displayName: json.displayName,
        firstName: json.givenName,
        lastName: json.surname,
        email: json.mail,
        timeZone: json.timezone,
        plantLocation: json.officeLocation,
        lang: json.preferredLanguage,
        phone: { business: json.businessPhones, mobile: json.mobilePhone },
        userImage: json.userPhoto,
        role: json.role || (state.user && state.user.role),
        isAdmin:
          json.role === 'admin' || (state.user && state.user.role) === 'admin'
      };
      return {
        ...state,
        user: mergedUser
      };
    /* \USER */

    /* HEADER */
    case SET_COMPONENT_LIST:
      return {
        ...state,
        componentLists: action.payload
      };
    /* \HEADER */

    /* HEADER */
    case SET_QUICK_LINKS:
      return {
        ...state,
        quickLinks: action.payload
      };
    /* \HEADER */

    /* MARKETPLACE */
    case UPDATE_MARKETPLACE_WIDGETS:
      return {
        ...state,
        marketplaceWidgets: action.payload
      };
    /* \MARKETPLACE */

    /* PLANT STATUS */
    case SET_PLANT_LOCATIONS:
      return {
        ...state,
        plantLocations: action.payload
      };
    /* \PLANT STATUS */

    /* WIDGET REQUESTS */
    case UPDATE_WIDGET_REQUESTS:
      return {
        ...state,
        reviewWidgetRequests: action.payload
      };

    case WIDGET_REQUEST_COUNT:
      return {
        ...state,
        reviewWidgetRequestCount: action.payload
      };
    /* \WIDGET REQUESTS */

    default:
      return state;
  }
}
