import { connectRouter } from 'connected-react-router';
import { History } from 'history';
import { combineReducers } from 'redux';
import appReducer from './appReducer';
import customOidcReducer from './customOidcReducer';
import dashboardReducer from './dashboardReducer';

const reducer = (history: History) =>
  combineReducers({
    router: connectRouter(history),
    oidc: customOidcReducer,
    app: appReducer,
    dashboard: dashboardReducer
  });

export default reducer;
