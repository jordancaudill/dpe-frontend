import { reducer, UserState, Action } from 'redux-oidc';
import axios, { AxiosRequestConfig } from 'axios';
import { User } from 'oidc-client';

import { API } from '../vars';
import { SET_USER_FROM_OIDC } from '../actions/user/types';

export interface OidcState extends UserState {
  isAuthenticated: boolean;
  user_id?: string;
}

export const initialOidcState: OidcState = {
  isLoadingUser: true,
  isAuthenticated: false
};

let interceptor: number;

export default function customOidcReducer(
  state = initialOidcState,
  action: Action<User | Error>
): OidcState {
  switch (action.type) {
    case SET_USER_FROM_OIDC:
      const oidc = reducer(state, action);
      const nextState = {
        ...oidc,
        isAuthenticated:
          (oidc &&
            !oidc.isLoadingUser &&
            oidc.user &&
            oidc.user.profile &&
            notEmpty(oidc.user.profile.preferred_username)) ||
          false,
        user_id:
          (oidc &&
            oidc.user &&
            oidc.user.profile &&
            oidc.user.profile.preferred_username) ||
          ''
      };

      interceptor && axios.interceptors.request.eject(interceptor);
      interceptor = axios.interceptors.request.use(setUserHeaders(nextState));

      return nextState;
    default:
      return state;
  }
}

function notEmpty(what?: string) {
  return what !== '' && what !== undefined && what !== null;
}

export const setUserHeaders = (oidc: OidcState) => (
  config: AxiosRequestConfig
) => {
  if (!oidc.isAuthenticated) {
    console.error('Called authenticated API before getting credentials');

    throw Error('Called authenticated API before getting credentials');
  }

  if (config.url && config.url.startsWith(API.url)) {
    config.headers = config.headers || {};
    config.headers['Content-Type'] = 'application/json';
    config.headers.Authorization = `Bearer ${(oidc.user &&
      oidc.user.id_token) ||
      ''}`;
    config.headers.token = (oidc.user && oidc.user.access_token) || '';
  }
  return config;
};
