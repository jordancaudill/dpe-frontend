import * as React from 'react';
import { Action } from 'redux';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';

import { StoreState } from '../store';
import LoadingNotifier from './LoadingNotifier';
import WidgetGrid from './WidgetGrid';
import { fetchWidgets } from '../actions/dashboard/actions';

type DashboardProps = ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatchToProps>;

export class Dashboard extends React.Component<DashboardProps> {
  componentDidMount() {
    this.props.fetchWidgets();
  }

  render() {
    return (
      <div style={{ margin: '0 10px' }}>
        {!this.props.loading ? (
          <WidgetGrid />
        ) : (
          <LoadingNotifier classes="widget-list-loading" />
        )}
      </div>
    );
  }
}

function mapStateToProps(store: StoreState) {
  return {
    loading: store.dashboard.dashboardLoading
  };
}

function mapDispatchToProps(
  dispatch: ThunkDispatch<StoreState, undefined, Action>
) {
  return {
    fetchWidgets: () => dispatch(fetchWidgets())
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard);
