import * as React from 'react';
import { OidcProvider } from 'redux-oidc';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { MuiThemeProvider } from '@material-ui/core';

import { theme } from '../theme';
import Root from '../components/root';
import Routes from '../routes';
import { store, history } from '../store';
import userManager from '../userManager';

import '../fonts/ProximaNova.ttf';
import '../fonts/ProximaNova.woff';
import '../fonts/ProximaNova.otf';
import '../fonts/ProximaNova-Semibold.ttf';
import '../fonts/ProximaNova-Semibold.woff';
import '../fonts/ProximaNova-Semibold.otf';

export default function App() {
  return (
    <MuiThemeProvider theme={theme}>
      <Provider store={store}>
        <OidcProvider store={store} userManager={userManager}>
          <ConnectedRouter history={history}>
            <Root>
              <Routes />
            </Root>
          </ConnectedRouter>
        </OidcProvider>
      </Provider>
    </MuiThemeProvider>
  );
}
