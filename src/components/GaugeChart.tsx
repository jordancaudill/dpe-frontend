import * as React from 'react';
import { ResponsiveContainer, PieChart, Pie, Cell } from 'recharts';
import { GaugeChartGroup } from '../models/hl_data/guagechart.model';

type GaugeChartProps = GaugeChartGroup & {
  startColor: string;
  stopColor: string;
};

class GaugeChart extends React.Component<GaugeChartProps> {
  defaults = {
    cx: '50%',
    cy: '50%',
    startAngle: 180,
    endAngle: 0,
    guideInnerRadius: '80%',
    guideOuterRadius: '99%',
    innerRadius: '80%',
    outerRadius: '99%',
    labelY: '40%'
  };

  render() {
    return <BaseChart {...{ ...this.defaults, ...this.props }} />;
  }
}
class DonutChart extends React.Component<GaugeChartProps> {
  defaults = {
    cx: '50%',
    cy: '50%',
    startAngle: 90,
    endAngle: 450,
    guideInnerRadius: '88%',
    guideOuterRadius: '96%',
    innerRadius: '85%',
    outerRadius: '99%',
    labelY: '50%'
  };

  render() {
    return <BaseChart {...{ ...this.defaults, ...this.props }} />;
  }
}

type BaseChartProps = GaugeChartProps & {
  cx: string;
  cy: string;
  startAngle: number;
  endAngle: number;
  guideInnerRadius: string;
  guideOuterRadius: string;
  innerRadius: string;
  outerRadius: string;
  labelY: string;
};

class BaseChart extends React.Component<BaseChartProps> {
  getNormalizedValue(max: number, min: number) {
    if (this.props.value >= max) {
      return Math.floor(this.props.value);
    } else if (this.props.value <= min) {
      return Math.ceil(this.props.value);
    } else {
      return Math.round(this.props.value);
    }
  }

  renderedLabel = () => {
    const value = Math.round(
      this.props.uom === '%' && this.props.value > 100 ? 100 : this.props.value
    );

    return (
      <>
        <text
          x="0"
          y={this.props.labelY}
          textAnchor="middle"
          dominantBaseline="central"
        >
          <tspan
            x="50%"
            dy="0"
            fill={`#${this.props.stopColor}`}
            style={{ fontSize: (value + '').length < 4 ? '2rem' : '1.75rem' }}
          >
            {value}

            <tspan style={{ fontSize: '0.8rem', paddingLeft: '0.25rem' }}>
              {this.props.uom}
            </tspan>
          </tspan>

          <tspan x="50%" dy="1.5em">
            {this.props.displayName}
          </tspan>
        </text>
      </>
    );
  };

  shouldComponentUpdate(nextProps: BaseChartProps) {
    return (
      this.props.value !== nextProps.value ||
      this.props.uom !== nextProps.uom ||
      this.props.min !== nextProps.min ||
      this.props.max !== nextProps.max ||
      this.props.kind !== nextProps.kind ||
      this.props.displayName !== nextProps.displayName
    );
  }

  render() {
    const MAX = +(this.props.max || 0) * 1;
    const MIN = +(this.props.min || 0) * 1;

    return (this.props.value || this.props.value === 0) &&
      this.props.value >= 0 &&
      !isNaN(MAX) &&
      MIN < MAX ? (
      <ResponsiveContainer>
        <PieChart>
          <defs>
            <linearGradient
              id={`donutGradient${this.props.startColor}`}
              x1="0"
              y1="0"
              x2="1"
              y2="1"
            >
              <stop
                offset="0%"
                stopColor={`#${this.props.startColor}`}
                stopOpacity={1}
              />

              <stop
                offset="80%"
                stopColor={`#${this.props.stopColor}`}
                stopOpacity={1}
              />
            </linearGradient>
          </defs>

          <Pie
            dataKey="value"
            data={[{ name: 'max', value: MAX }]}
            cx={this.props.cx}
            cy={this.props.cy}
            startAngle={this.props.startAngle}
            endAngle={this.props.endAngle}
            innerRadius={this.props.guideInnerRadius}
            outerRadius={this.props.guideOuterRadius}
            paddingAngle={0}
            isAnimationActive={false}
          >
            <Cell fill="#E0E2E7" />
          </Pie>

          <Pie
            dataKey="value"
            data={[
              {
                name: this.props.displayName,
                value: this.getNormalizedValue(MAX, MIN)
              }
            ]}
            cx="50%"
            cy="50%"
            startAngle={this.props.startAngle}
            endAngle={
              this.props.startAngle +
              (this.props.value / MAX) *
                (this.props.endAngle - this.props.startAngle)
            }
            innerRadius={this.props.innerRadius}
            outerRadius={this.props.outerRadius}
            paddingAngle={0}
            label={this.renderedLabel}
            labelLine={false}
          >
            <Cell
              fillOpacity={1}
              fill={`url(#donutGradient${this.props.startColor})`}
            />
          </Pie>
        </PieChart>
      </ResponsiveContainer>
    ) : this.props.value < 0 ? (
      <div className="no-data-view">Outage</div>
    ) : (
      <div className="no-data-view">No Data</div>
    );
  }
}

export { GaugeChart, DonutChart };
