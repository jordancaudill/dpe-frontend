import * as React from 'react';
import CloudIcon from '@material-ui/icons/Cloud';

import '../css/Error.css';

type ErrorProps = {
  onClick?: React.MouseEventHandler;
  display: boolean;
  graphicalElement?: boolean;
  title?: string;
  icon?: JSX.Element;
  message?: string;
  linkMessage?: string;
};

export default class Error extends React.Component<ErrorProps> {
  handleClick(ev: React.MouseEvent<HTMLDivElement, MouseEvent>) {
    if ('function' === typeof this.props.onClick) {
      return this.props.onClick(ev);
    }

    window.location.reload();
  }

  render() {
    return (
      <div
        className={`error-container ${this.props.display || 'display-none'}`}
      >
        <svg style={{ position: 'absolute', top: '-100%', left: '-100%' }}>
          <defs>
            <filter id="errorIconDropshadow">
              <feGaussianBlur in="SourceGraphic" stdDeviation={4} />

              <feOffset dx={0} dy={2} result="offsetblur" />

              <feFlood floodColor="rgba(0,0,0,0.5)" floodOpacity="0.9" />

              <feComposite in2="offsetblur" operator="in" />

              <feMerge>
                <feMergeNode />

                <feMergeNode in="SourceGraphic" />
              </feMerge>
            </filter>
          </defs>
        </svg>

        <div className="error-icon">
          {this.props.graphicalElement || (
            <div
              style={{ color: '#D8D8D8', filter: 'url(#errorIconDropshadow)' }}
            >
              {this.props.icon || <CloudIcon />}
            </div>
          )}
        </div>

        <h1 className="error-title">{this.props.title || 'Oh no!'}</h1>

        <div className="error-message">
          {this.props.message ||
            'Something went wrong. We are having trouble displaying your dashboard.'}
        </div>

        <div className="error-link" onClick={ev => this.handleClick(ev)}>
          {/* eslint-disable-next-line */}
          <a href="#">{this.props.linkMessage || 'TRY AGAIN'}</a>
        </div>
      </div>
    );
  }
}
