import * as React from 'react';
import { transformConfigParams } from '../utils';

import { DashboardWidgetOrder } from '../models/marketplace/dashboardwidgetorder.model';
import { StoreState } from '../store';
import { useSelector } from 'react-redux';
import { components } from '../widgets/Components';

export type DashboardWidgetProps = DashboardWidgetOrder & {
  hideChrome?: boolean;
};

export default function DashboardWidgetComponent(props: DashboardWidgetProps) {
  const componentLists = useSelector(
    (state: StoreState) => state.app.componentLists
  );

  if (
    props &&
    props.modelId &&
    props.instanceId &&
    props.type &&
    props.status !== 'D' &&
    componentLists !== undefined &&
    componentLists.length > 0
  ) {
    let config = {
      ...transformConfigParams(props.widgetConfigparms || []),
      ...transformConfigParams(props.widgetInstanceConfigparms || [])
    };
    const widgetProps = {
      key: `widget${props.instanceId}`,
      widgetId: props.instanceId,
      widgetSize: !config.widgetSize ? 'extra-large' : '',
      config,
      title: props.title,
      subtitle: props.subtitle,
      hideChrome: props.hideChrome
    };

    const component = componentLists.find(
      o => o.id && o.id.find(id => props.type === id)
    );
    const componentName =
      component && (component.component as keyof typeof components);

    let Component = componentName && components[componentName];
    return Component ? <Component {...widgetProps} /> : null;
  }
  return null;
}
