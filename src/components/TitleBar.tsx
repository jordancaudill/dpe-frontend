import * as React from 'react';
import { connect } from 'react-redux';
import LogoIcon from './LogoIcon';
import BareSelect from './shared/BareSelect';
import { Option } from './shared/Option';
import getPlantLocations from '../requests/plantLocationsRequests';
import { StoreState } from '../store';
import { theme } from '../theme';

type TitleBarProps = ReturnType<typeof mapStateToProps>;

type TitleBarState = {
  currentLocation: string | undefined;
  plantLocationOptions: Option[];
};

export class TitleBar extends React.Component<TitleBarProps, TitleBarState> {
  state: TitleBarState = {
    currentLocation: undefined,
    plantLocationOptions: this.transformPlantLocations()
  };

  componentDidMount() {
    getPlantLocations();
  }

  transformPlantLocations(): Option[] {
    return this.props.plantLocations.map(location => ({
      value: location.id,
      label: location.name
    }));
  }

  handleChange(event: React.ChangeEvent<{}>): void {
    this.setState({
      currentLocation: (event.target as HTMLFormElement).value
    });
  }

  render() {
    return (
      <div
        style={{
          backgroundColor: theme.palette.primary.main,
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center'
        }}
      >
        <div style={{ flexShrink: 0, paddingLeft: '4px' }}>
          <LogoIcon />
        </div>
        <BareSelect
          selectId="plant-locations-dropdown"
          options={this.transformPlantLocations()}
          color="#ffffff"
          padding="0 30px 0 0"
          currentSelection={this.state.currentLocation}
          onChange={event => this.handleChange(event)}
        />
      </div>
    );
  }
}

function mapStateToProps(state: StoreState) {
  return {
    plantLocations: state.app.plantLocations
  };
}

export default connect(mapStateToProps)(TitleBar);
