import * as React from 'react';
import Drawer from '@material-ui/core/Drawer';
import AddWidgetsContainer from './addwidgets/AddWidgetsContainer';
import GenericButton from './shared/GenericButton';
import { theme } from '../theme';

type AddWidgetsState = {
  drawerState: boolean;
};

export class AddWidgets extends React.Component<{}, AddWidgetsState> {
  state: AddWidgetsState = {
    drawerState: false
  };

  toggleDrawer(
    event: { type?: string } | React.MouseEvent,
    state: boolean
  ): void {
    if (
      event.type === 'keydown' &&
      ((event as React.KeyboardEvent).key === 'Tab' ||
        (event as React.KeyboardEvent).key === 'Shift')
    ) {
      return;
    }

    this.setState({ drawerState: state });
  }

  render() {
    return (
      <div>
        <GenericButton
          textColor={theme.palette.primary.main}
          bdColor={theme.palette.primary.main}
          handleClick={event => this.toggleDrawer(event, true)}
        >
          + Add Widgets
        </GenericButton>
        <Drawer
          anchor="right"
          open={this.state.drawerState}
          onClose={event => this.toggleDrawer(event, false)}
        >
          <AddWidgetsContainer
            drawerHandle={event => this.toggleDrawer(event, false)}
          />
        </Drawer>
      </div>
    );
  }
}

export default AddWidgets;
