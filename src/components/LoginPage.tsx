import * as React from 'react';
import { connect } from 'react-redux';

import { StoreState } from '../store';
import userManager from '../userManager';
import { getUser } from '../requests/userRequests';
import LoadingNotifier from './LoadingNotifier';

type LoginPageProps = ReturnType<typeof mapStateToProps>;

class LoginPage extends React.Component<LoginPageProps> {
  componentDidMount() {
    if (!this.props.isAuthenticated) {
      userManager.getUser().then(res => {
        if (!res || res.expired) {
          userManager.signinRedirect();
        } else {
          getUser();
        }
      });
    } else {
      getUser();
    }
  }

  render() {
    return <LoadingNotifier />;
  }
}

function mapStateToProps(state: StoreState) {
  return {
    isAuthenticated: state.oidc.isAuthenticated
  };
}

export default connect(mapStateToProps)(LoginPage);
