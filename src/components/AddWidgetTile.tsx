import * as React from 'react';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'redux';
import { Card, CardContent, Typography, Grid } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';

import { StoreState } from '../store';
import { setWidgetList } from '../actions/dashboard/actions';
import { updateDashboard } from '../requests/userRequests';
import { updateWidget } from '../requests/widgetRequests';
import { getAvailableWidgets } from '../requests/getAvailableWidgets';
import { formatDate } from '../utils';
import Snackbar from './Snackbar';
import Modal from './Modal';
import LoadingNotifier from './LoadingNotifier';
import GenericButton from './shared/GenericButton';
import { Widget } from '../models/marketplace/widget.model';
import { UserDashboard } from '../models/marketplace/userdashboard.model';

import '../css/MarketplaceTile.css';
import { theme } from '../theme';

export type AddWidgetTileProps = Widget & {
  onClick?: (ev: React.MouseEvent<HTMLElement, MouseEvent>) => void;
};

class AddWidgetTile extends React.Component<
  ReturnType<typeof mapStateToProps> &
    ReturnType<typeof mapDispatchToProps> &
    AddWidgetTileProps
> {
  defaultSnackbarMessage = `Added ${this.props.title} to Dashboard`;
  state = {
    openSnackbar: false,
    snackbarMessage: this.defaultSnackbarMessage,
    deleteConfirm: false,
    deleting: false,
    added: false,
    editing: false
  };

  async addWidget() {
    this.setState({
      openSnackbar: true,
      added: true
    });

    if (!this.props.modelId) return;

    const dashboard = await updateDashboard({
      userId: this.props.dashboard.userId,
      dashboardId: this.props.dashboard.dashboardId,
      dashboardOrder: [
        ...this.props.dashboard.dashboardWidgets,
        {
          ...this.props.widgetDetail[this.props.modelId],
          order: this.props.dashboard.dashboardWidgets.length
        }
      ]
    });

    dashboard && this.props.setWidgetList(dashboard);
  }

  closeWidgetModal() {
    this.setState({
      deleteConfirm: null,
      deleting: false
    });
  }

  deleteWidget(id: string) {
    this.setState({
      deleting: true
    });

    this.props
      .updateWidget(id, {
        status: 'D'
      })
      .then(res => {
        this.props.getAvailableWidgets();

        this.closeWidgetModal();

        this.setState({
          snackbarMessage: `${this.props.title} successfully deleted`,
          openSnackbar: true
        });

        this.setState({
          deleting: false
        });
      })
      .catch(err => {
        console.error(err);

        this.setState({
          deleting: false
        });
      });
  }

  openConfirmModal() {
    this.setState({
      deleteConfirm: true
    });
  }

  displayAddOrRemoveButton() {
    return (
      <>
        {!this.props.isAdmin || !this.state.editing ? (
          !this.state.added ? (
            <GenericButton
              textColor={theme.palette.secondary.main}
              bdColor={theme.palette.secondary.main}
              handleClick={() => this.addWidget()}
            >
              Add to Dash
            </GenericButton>
          ) : (
            <GenericButton
              textColor="#ffffff"
              bdColor={theme.palette.secondary.main}
              backgroundColor={theme.palette.secondary.main}
            >
              On Dash
            </GenericButton>
          )
        ) : (
          <>
            <GenericButton
              textColor={theme.palette.error.main}
              bdColor={theme.palette.error.main}
              handleClick={() => this.openConfirmModal()}
            >
              Remove Widget
            </GenericButton>

            <Modal
              onClick={() => this.closeWidgetModal()}
              title="REMOVE WIDGET"
              content={
                !this.state.deleteConfirm ? null : (
                  <div className="delete-widget-confirm">
                    <p>
                      Are you sure you want to delete this widget? Deletion is
                      permanent and can no longer be viewed by users.
                    </p>

                    <div className="delete-widget-button-group">
                      <button
                        className="secondary"
                        onClick={() => this.closeWidgetModal()}
                      >
                        Cancel
                      </button>

                      <button
                        className="primary warning"
                        onClick={() =>
                          this.props.modelId &&
                          this.deleteWidget(this.props.modelId)
                        }
                      >
                        DELETE WIDGET
                      </button>
                    </div>

                    <LoadingNotifier
                      classes={`content-loading ${this.state.deleting} ${
                        this.state.deleting ? '' : 'display-none'
                      }`}
                    />
                  </div>
                )
              }
            />
          </>
        )}
      </>
    );
  }

  handleSnackbarClose() {
    this.setState({
      openSnackbar: false,
      snackbarMessage: this.defaultSnackbarMessage
    });
  }

  render() {
    return (
      <>
        <Card style={{ width: '100%' }}>
          <CardContent>
            <Grid container>
              <Grid item zeroMinWidth md xs={12}>
                <Typography variant="h2">
                  {this.props.title}

                  {this.props.isAdmin ? (
                    <EditIcon
                      onClick={() =>
                        this.setState({
                          editing: !this.state.editing
                        })
                      }
                    ></EditIcon>
                  ) : null}
                </Typography>
                {this.props.subtitle ? (
                  <>
                    <br />
                    <Typography
                      variant="subtitle1"
                      color="textSecondary"
                      style={{ paddingBottom: '4px' }}
                    >
                      DESCRIPTION
                    </Typography>
                    <Typography variant="body2">
                      {this.props.subtitle}
                    </Typography>
                  </>
                ) : null}
                {this.props.widgetConfigparms || this.props.isAdmin ? (
                  <>
                    <br />
                    <Typography
                      variant="subtitle1"
                      color="textSecondary"
                      style={{ paddingBottom: '8px' }}
                    >
                      SPECS
                    </Typography>

                    {this.props.widgetConfigparms &&
                      this.props.widgetConfigparms.map(p =>
                        p.key ? (
                          <div
                            key={`AddWidgetTile${p.key}`}
                            style={{ paddingBottom: '8px' }}
                          >
                            <Typography variant="h4">
                              {p.key
                                .replace(/([A-Z])/g, ' $1')
                                .replace(/^./, function(str) {
                                  return str.toUpperCase();
                                })}
                            </Typography>{' '}
                            <Typography
                              variant="h3"
                              style={{
                                textOverflow: 'ellipsis',
                                overflow: 'hidden',
                                whiteSpace: 'nowrap',
                                height: '1em',
                                maxWidth: '180px'
                              }}
                            >
                              {p.value}
                            </Typography>
                          </div>
                        ) : null
                      )}
                    {!this.props.isAdmin ? null : (
                      <>
                        <div style={{ paddingBottom: '8px' }}>
                          <Typography variant="h4">Created By</Typography>{' '}
                          <Typography variant="h3">
                            {this.props.createdBy}
                          </Typography>
                        </div>

                        {this.props.createdOn ? (
                          <div style={{ paddingBottom: '8px' }}>
                            <Typography variant="h4">Created On</Typography>{' '}
                            <Typography variant="h3">
                              {formatDate(this.props.createdOn)}
                            </Typography>
                          </div>
                        ) : null}
                      </>
                    )}
                  </>
                ) : null}
              </Grid>
              <Grid item md xs={12} className="marketplace-tile-graph">
                <img
                  src={this.props.image || undefined}
                  alt="Widget Display Graphic"
                  onClick={ev => this.props.onClick && this.props.onClick(ev)}
                />
                <div
                  className="marketplace-tile-graph-overlay"
                  onClick={ev => this.props.onClick && this.props.onClick(ev)}
                >
                  <button>View</button>
                </div>
              </Grid>
              <Grid
                item
                container
                md
                xs={12}
                justify="flex-end"
                alignItems="center"
              >
                {this.displayAddOrRemoveButton()}
              </Grid>
            </Grid>
          </CardContent>
        </Card>

        <Snackbar
          message={this.state.snackbarMessage}
          display={this.state.openSnackbar}
          onClose={() => this.handleSnackbarClose()}
        />
      </>
    );
  }
}

function mapStateToProps(state: StoreState) {
  return {
    isAdmin: (state.app.user && state.app.user.isAdmin) || false,
    widgetDetail: state.dashboard.widgetDetail,
    dashboard: state.dashboard
  };
}

function mapDispatchToProps(
  dispatch: ThunkDispatch<StoreState, undefined, Action>
) {
  return {
    setWidgetList: (widgetList: UserDashboard) =>
      dispatch(setWidgetList(widgetList)),
    updateWidget,
    getAvailableWidgets
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddWidgetTile);
