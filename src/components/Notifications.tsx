import * as React from 'react';
import Badge from '@material-ui/core/Badge';

//@ts-ignore
import notifications_icon from '../assets/notifications_icon.svg';

const styles = {
  containerStyle: {
    display: 'flex',
    justifyContent: 'center' as 'center',
    alignItems: 'center',
    position: 'relative' as 'relative',
    letterSpacing: '0.79px',
    textAlign: 'center' as 'center'
  }
};

type NotificationsState = {
  qualsNumber: number;
};

export class Notifications extends React.Component {
  state: NotificationsState = {
    qualsNumber: Math.floor(Math.random() * 11)
  };

  render() {
    return (
      <div style={styles.containerStyle}>
        <Badge badgeContent={this.state.qualsNumber} color="error" max={9}>
          <img src={notifications_icon} alt="notifications" />
        </Badge>
      </div>
    );
  }
}

export default Notifications;
