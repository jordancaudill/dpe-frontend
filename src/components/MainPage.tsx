import * as React from 'react';
import { connect } from 'react-redux';
import { Grid, Divider } from '@material-ui/core';

import { StoreState } from '../store';
import TitleBar from './TitleBar';
import MainMenu from './mainmenu/MainMenu';
import Dashboard from '../components/Dashboard';

import '../css/app.css';

type MainPageProps = ReturnType<typeof mapStateToProps>;

class MainPage extends React.Component<MainPageProps> {
  render() {
    return this.props.isAuthenticated ? (
      <div>
        <Grid item xs={12}>
          <TitleBar />
        </Grid>
        <Grid container spacing={0} style={styles.fullHeight}>
          <Divider style={styles.divider} />
          <MainMenu />

          <Grid item xs style={{ marginLeft: '61px' }}>
            <Dashboard />
          </Grid>
        </Grid>
      </div>
    ) : null;
  }
}

const styles = {
  fullHeight: {
    height: '100vh'
  },
  divider: {
    position: 'absolute' as 'absolute',
    top: '120px',
    width: '100%'
  }
};

function mapStateToProps(state: StoreState) {
  return {
    isAuthenticated: state.oidc.isAuthenticated
  };
}

export default connect(mapStateToProps)(MainPage);
