import * as React from 'react';
import Grid from '@material-ui/core/Grid';
import SearchBar from './shared/SearchBar';
import Notifications from './Notifications';
import ReviewWidgets from './ReviewWidgets';
import AddWidgets from './AddWidgets';
import Settings from './Settings';

import { Typography } from '@material-ui/core';

export class DashboardHeader extends React.Component {
  render() {
    return (
      <Grid
        container
        direction="row"
        alignItems="center"
        justify="space-between"
        style={{ height: '100%' }}
      >
        <Grid item style={{ display: 'flex', paddingBottom: '10px' }}>
          <div style={{ padding: '5px 40px 0 0' }}>
            <Typography variant="h1">Dashboard</Typography>
          </div>
          <div>
            <SearchBar />
          </div>
        </Grid>
        <Grid item style={{ display: 'flex' }}>
          <div style={{ padding: '0 25px 0 0' }}>
            <Notifications />
          </div>
          <div style={{ padding: '0 8px 0 0' }}>
            <ReviewWidgets />
          </div>
          <div style={{ padding: '0 20px 0 0' }}>
            <AddWidgets />
          </div>
          <div style={{ padding: '6px 0 0 0' }}>
            <Settings />
          </div>
        </Grid>
      </Grid>
    );
  }
}

export default DashboardHeader;
