import * as React from 'react';
import GenericButton from '../shared/GenericButton';
import { theme } from '../../theme';

type FormSubmissionMessageProps = {
  handleClick: () => void;
};

export class FormSubmissionMessage extends React.Component<
  FormSubmissionMessageProps
> {
  render() {
    return (
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'space-between'
        }}
      >
        <p
          style={{
            marginTop: '20px',
            color: '#9b9b9b'
          }}
        >
          <em>Thank you for your submission.</em>
        </p>
        <p
          style={{
            marginBottom: '10px',
            color: '#9b9b9b'
          }}
        >
          <em>We will review it and get back to you shortly.</em>
        </p>
        <GenericButton
          textColor={theme.palette.primary.main}
          bdColor={theme.palette.primary.main}
          handleClick={this.props.handleClick}
        >
          SUBMIT ANOTHER WIDGET
        </GenericButton>
      </div>
    );
  }
}

export default FormSubmissionMessage;
