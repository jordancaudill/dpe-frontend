import * as React from 'react';
import { Container, Grid, Typography, Button } from '@material-ui/core';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import BareSelect from '../shared/BareSelect';
import SearchBar from '../shared/SearchBar';
import GenericButton from '../shared/GenericButton';
import TitleModal from '../shared/TitleModal';
import SubmitNewModal from './SubmitNewModal';
import { Option } from '../shared/Option';
import { theme } from '../../theme';

type AddWidgetsHeaderProps = {
  currentFilter: string;
  filterOptions: Option[];
  handleFilterChange: (event: React.ChangeEvent<{}>) => void;
  drawerHandle: (event: React.MouseEvent<{}>) => void;
  userName: string | null;
};

type AddWidgetsHeaderState = {
  modalOpen: boolean;
};

export class AddWidgetsHeader extends React.Component<
  AddWidgetsHeaderProps,
  AddWidgetsHeaderState
> {
  state = {
    modalOpen: false
  };

  handleModalClick = () => {
    this.setState({ modalOpen: true });
  };

  handleModalClose = () => {
    this.setState({ modalOpen: false });
  };

  render() {
    return (
      <Container
        style={{
          padding: '10px 10px 0 10px',
          width: '730px'
        }}
      >
        <Grid
          container
          alignItems="center"
          style={{
            paddingBottom: '32px',
            paddingTop: '12px'
          }}
        >
          <Grid item style={{ marginLeft: '-17px' }}>
            <Button
              onClick={this.props.drawerHandle}
              style={{ backgroundColor: 'transparent' }}
            >
              <ArrowForwardIosIcon color="action" />
            </Button>
          </Grid>
          <Grid item>
            <div style={{ marginLeft: '-4px' }}>
              <Typography variant="h1">Widget Marketplace</Typography>
            </div>
          </Grid>
          <Grid item style={{ marginLeft: 'auto' }}>
            <GenericButton
              textColor={theme.palette.primary.main}
              bdColor={theme.palette.primary.main}
              handleClick={this.handleModalClick}
            >
              SUBMIT NEW
            </GenericButton>
            <TitleModal
              open={this.state.modalOpen}
              onClose={this.handleModalClose}
              maxWidth="md"
              fullWidth
              title="Submit a Widget"
            >
              <SubmitNewModal userName={this.props.userName} />
            </TitleModal>
          </Grid>
        </Grid>
        <Grid
          container
          justify="space-between"
          align-items="center"
          style={{
            paddingTop: '7px',
            marginTop: '-8px',
            marginBottom: '-10px'
          }}
        >
          <Grid item style={{ paddingLeft: '8px', marginTop: '7px' }}>
            <BareSelect
              inputLabel="FILTER"
              selectId="marketplace-filter"
              options={this.props.filterOptions}
              onChange={this.props.handleFilterChange}
              currentSelection={this.props.currentFilter}
            />
          </Grid>
          <Grid item>
            <SearchBar />
          </Grid>
        </Grid>
      </Container>
    );
  }
}
export default AddWidgetsHeader;
