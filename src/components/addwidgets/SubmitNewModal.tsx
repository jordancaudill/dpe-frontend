import * as React from 'react';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import NewWidgetForm from './NewWidgetForm';
import FormSubmissionMessage from './FormSubmissionMessage';
import { Typography } from '@material-ui/core';

const widgetGuide = require('../../assets/widget_guide.png');

type SubmitNewModalProps = {
  userName: string | null;
};

type SubmitNewModalState = {
  isFormOpen: boolean;
};

export class SubmitNewModal extends React.Component<
  SubmitNewModalProps,
  SubmitNewModalState
> {
  state: SubmitNewModalState = {
    isFormOpen: true
  };

  renderMessage(): void {
    this.setState({ isFormOpen: false });
  }

  renderForm = () => {
    this.setState({ isFormOpen: true });
  };

  render() {
    return (
      <Grid container spacing={4}>
        <Grid item style={{ width: '435px' }}>
          <Typography variant="h3">SUBMISSION FORM</Typography>
          {this.state.isFormOpen ? (
            <NewWidgetForm
              handleClick={() => this.renderMessage()}
              userName={this.props.userName}
            />
          ) : (
            <FormSubmissionMessage handleClick={this.renderForm} />
          )}
        </Grid>
        <Grid item>
          <Divider orientation="vertical" />
        </Grid>
        <Grid item>
          <img src={widgetGuide} alt="Guide to widget layout" />
        </Grid>
      </Grid>
    );
  }
}

export default SubmitNewModal;
