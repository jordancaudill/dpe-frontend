import * as React from 'react';
import Container from '@material-ui/core/Container';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import AddWidgetTile from '../AddWidgetTile';
import { Widget } from '../../models/marketplace/widget.model';

type AddWidgetListProps = {
  tiles: Widget[];
};

export class AddWidgetList extends React.Component<AddWidgetListProps> {
  render() {
    return (
      <Container style={{ padding: '10px' }}>
        <List aria-label="widgets to add">
          {this.props.tiles.map((widget, i) => (
            <ListItem key={`addWidgetList${i}`}>
              <AddWidgetTile {...widget} />
            </ListItem>
          ))}
        </List>
      </Container>
    );
  }
}

export default AddWidgetList;
