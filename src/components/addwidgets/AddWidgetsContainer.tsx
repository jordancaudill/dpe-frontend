import * as React from 'react';
import { connect } from 'react-redux';
import AddWidgetsHeader from './AddWidgetsHeader';
import AddWidgetList from './AddWidgetList';
import { Option } from '../shared/Option';
import { Widget } from '../../models/marketplace/widget.model';

import { StoreState } from '../../store';
import { getWidgetNames, getWidgetTypeArray } from '../../utils';

type AddWidgetContainerProps = ReturnType<typeof mapStateToProps> & {
  drawerHandle: (event: React.MouseEvent<{}>) => void;
};

type AddWidgetContainerState = {
  tiles: Widget[];
  currentFilter: string;
  filterOptions: Option[];
};

export class AddWidgetsContainer extends React.Component<
  AddWidgetContainerProps,
  AddWidgetContainerState
> {
  state: AddWidgetContainerState = {
    tiles: this.props.availableWidgets,
    filterOptions: getWidgetNames(this.props.componentsList, true),
    currentFilter: 'all'
  };

  generateUserName = () => {
    if (!this.props.user || !this.props.userId) {
      return null;
    } else {
      return `${this.props.user.displayName} - (${this.props.userId})`;
    }
  };

  handleFilterChange(event: React.ChangeEvent<{}>): void {
    const newValue = (event.target as HTMLFormElement).value;
    this.setState({
      currentFilter: newValue
    });

    if (newValue === 'all') {
      return this.setState({
        tiles: this.props.availableWidgets
      });
    } else {
      const widgetTypeArr = getWidgetTypeArray(
        this.props.componentsList,
        newValue
      );
      return this.setState({
        tiles: this.props.availableWidgets.filter(tile =>
          widgetTypeArr.includes(tile.type || '')
        )
      });
    }
  }

  render() {
    return (
      <div style={{ width: '750px' }}>
        <AddWidgetsHeader
          currentFilter={this.state.currentFilter}
          filterOptions={this.state.filterOptions}
          handleFilterChange={event => this.handleFilterChange(event)}
          drawerHandle={this.props.drawerHandle}
          userName={this.generateUserName()}
        />
        <AddWidgetList tiles={this.state.tiles} />
      </div>
    );
  }
}

function mapStateToProps(state: StoreState) {
  return {
    availableWidgets: state.app.marketplaceWidgets,
    componentsList: state.app.componentLists,
    user: state.app.user,
    userId: state.oidc.user_id
  };
}

export default connect(mapStateToProps)(AddWidgetsContainer);
