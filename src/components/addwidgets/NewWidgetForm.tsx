import * as React from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import OutlinedSelect from '../shared/OutlinedSelect';
import GenericButton from '../shared/GenericButton';
import { Option } from '../shared/Option';
import LoadingNotifier from '../LoadingNotifier';
import { Widget } from '../../models/marketplace/widget.model';
import { createWidget } from '../../requests/widgetRequests';
import { theme } from '../../theme';
import { getWidgetNames } from '../../utils';
import { StoreState } from '../../store';
import { connect } from 'react-redux';

type NewWidgetFormProps = ReturnType<typeof mapStateToProps> & {
  handleClick: () => void;
  userName: string | null;
};

type NewWidgetFormState = Widget & {
  widgetTypeOptions: Option[];
  size: string;
  sizeOptions: Option[];
  error: boolean;
  loading: boolean;
  errorMessage: string;
};

export class NewWidgetForm extends React.Component<
  NewWidgetFormProps,
  NewWidgetFormState
> {
  state: NewWidgetFormState = {
    title: '',
    subtitle: '',
    widgetUrl: '',
    tags: '',
    type: '',
    widgetTypeOptions: getWidgetNames(this.props.componenList, false),
    size: '',
    sizeOptions: [
      { value: 'small', label: 'Small' },
      { value: 'medium', label: 'Medium' },
      { value: 'large', label: 'Large' },
      { value: 'extra-large', label: 'Extra Large' }
    ],
    image: '',
    configurable: 'Y', // this is required to submit the custom config params (i.e., key-value pairs)
    userConfigurable: 'N', // this is required to allow the user to configure a widget - will default to no until needed
    createdBy: this.props.userName,
    error: false,
    errorMessage: '',
    loading: false
  };

  handleFieldChange(event: React.ChangeEvent<{}>, which: string): void {
    //@ts-ignore
    this.setState({
      [which]: (event.target as HTMLFormElement).value
    });
  }

  submitWidget(): void {
    if (
      this.state.title &&
      this.state.subtitle &&
      this.state.widgetUrl &&
      this.state.type &&
      this.state.size
    ) {
      const body = this.transformData(this.state);
      this.setState({
        error: false,
        loading: true
      });
      createWidget(body)
        .then(() => this.props.handleClick())
        .catch(() => {
          this.setState({
            error: true,
            errorMessage: `Something went wrong, please try again or contact an admin`,
            loading: false
          });
        });
    } else {
      this.setState({ error: true });
    }
  }

  transformData(state: NewWidgetFormState): NewWidgetFormState {
    const body = state;
    body.widgetConfigparms = [
      {
        key: 'widgetSize',
        value: state.size
      }
    ];

    body.image = `../../assets/${body.type}.png`;

    delete body.size;
    delete body.error;
    delete body.errorMessage;
    return body;
  }

  render() {
    return (
      <Grid container direction="column" alignItems="flex-start">
        <Grid item>
          {!this.state.error ? null : (
            <div
              style={{
                color: '#f44336',
                paddingTop: '10px'
              }}
            >
              Please fill in all required fields.
            </div>
          )}
        </Grid>
        <Grid item>
          <form
            noValidate
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'flex-start'
            }}
          >
            <TextField
              name="title"
              label="Widget Title"
              value={this.state.title || undefined}
              onChange={event => this.handleFieldChange(event, 'title')}
              required
              variant="outlined"
              placeholder="Enter the title of the widget"
              margin="normal"
              InputLabelProps={{ shrink: true }}
              error={this.state.error && !this.state.title}
              style={{ width: '400px' }}
            />

            <TextField
              name="subtitle"
              label="Subtitle"
              value={this.state.subtitle || undefined}
              onChange={event => this.handleFieldChange(event, 'subtitle')}
              required
              variant="outlined"
              placeholder="Enter a static or dynamic subtitle for the widget"
              margin="normal"
              InputLabelProps={{ shrink: true }}
              error={this.state.error && !this.state.subtitle}
              style={{ width: '400px' }}
            />

            <TextField
              name="widgetUrl"
              label="Widget URL"
              value={this.state.widgetUrl || undefined}
              onChange={event => this.handleFieldChange(event, 'widgetUrl')}
              variant="outlined"
              placeholder="Enter the URL for the widget"
              required
              margin="normal"
              InputLabelProps={{ shrink: true }}
              error={this.state.error && !this.state.widgetUrl}
              style={{ width: '400px' }}
            />

            <TextField
              name="tags"
              label="Tags"
              value={this.state.tags || undefined}
              onChange={event => this.handleFieldChange(event, 'tags')}
              variant="outlined"
              placeholder="Add tags for your widget"
              margin="normal"
              InputLabelProps={{ shrink: true }}
              style={{ width: '400px' }}
            />

            <OutlinedSelect
              inputLabel="Type"
              selectId="type"
              value={this.state.type || undefined}
              options={this.state.widgetTypeOptions}
              placeholder="Select the type of your widget"
              required
              onChange={event => this.handleFieldChange(event, 'type')}
              error={this.state.error && !this.state.type}
              width="400px"
            />

            <OutlinedSelect
              inputLabel="Sizes"
              selectId="size"
              value={this.state.size || undefined}
              options={this.state.sizeOptions}
              placeholder="Select which size your widget is utilizing"
              required
              onChange={event => this.handleFieldChange(event, 'size')}
              error={this.state.error && !this.state.size}
              width="400px"
            />
            <div style={{ alignSelf: 'center' }}>
              <GenericButton
                textColor={theme.palette.primary.main}
                bdColor={theme.palette.primary.main}
                handleClick={() => this.submitWidget()}
              >
                SUBMIT WIDGET
              </GenericButton>

              {this.state.loading ? <LoadingNotifier /> : null}
            </div>
          </form>
        </Grid>
      </Grid>
    );
  }
}

function mapStateToProps(store: StoreState) {
  return {
    componenList: store.app.componentLists
  };
}
export default connect(mapStateToProps)(NewWidgetForm);
