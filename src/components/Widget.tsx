import * as React from 'react';
import Divider from '@material-ui/core/Divider';
import DragIndicatorIcon from '@material-ui/icons/DragIndicator';

import WidgetOptions, { WidgetOptionsProps } from '../widgets/WidgetOptions';

import '../css/Widget.css';
import { Typography } from '@material-ui/core';

export type WidgetProps = WidgetOptionsProps & {
  classes?: string;
  widgetSize?: string;
  order?: number;
  notItem?: boolean;
  title?: JSX.Element | string | null | undefined;
  subtitle?: string | null | undefined;
  footer?: JSX.Element;
  config?: { [key: string]: string | null | undefined };
  onClick?: React.MouseEventHandler;
  hideChrome?: boolean;
};

export class Widget extends React.Component<WidgetProps> {
  render() {
    return this.props.widgetId !== undefined ? (
      <div
        className={this.props.classes}
        style={{
          height: '100%'
        }}
        onClick={ev =>
          'function' === typeof this.props.onClick
            ? this.props.onClick(ev)
            : null
        }
        id={!this.props.notItem ? this.props.widgetId : ''}
      >
        {!this.props.hideChrome ? (
          <div className="draggableHandle">
            <Typography
              variant="h2"
              style={{
                textTransform:
                  typeof this.props.title === 'string' ? 'uppercase' : 'none'
              }}
            >
              {this.props.title}
            </Typography>
            <Typography
              variant="h3"
              style={{
                display: !this.props.subtitle ? 'none' : 'block',
                marginTop: '1px'
              }}
            >
              {this.props.subtitle}
            </Typography>
            <div className="widget-title item-title">
              <WidgetOptions
                hideOptions={this.props.hideOptions || false}
                widgetId={this.props.widgetId}
                refreshClick={this.props.refreshClick}
                removeClick={this.props.removeClick}
              />
            </div>
             
            <Divider
              style={{
                width: '300%',
                marginLeft: '-16px',
                marginTop: typeof this.props.title === 'string' ? '' : '-10px'
              }}
            />
          </div>
        ) : (
          <DragIndicatorIcon
            className="draggableHandle"
            fontSize="small"
            style={{
              position: 'absolute' as 'absolute',
              top: '40%',
              transform: 'translate(0, -50%)',
              right: 0,
              color: '#6B6B6B',
              cursor: 'move'
            }}
          />
        )}
        {this.props.children}
        <div className="widget-footer">{this.props.footer}</div>
      </div>
    ) : null;
  }
}
