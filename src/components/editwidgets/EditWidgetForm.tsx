import * as React from 'react';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import OutlinedSelect from '../shared/OutlinedSelect';
import GenericButton from '../shared/GenericButton';
import LoadingNotifier from '../LoadingNotifier';
import { Widget } from '../../models/marketplace/widget.model';
import { updateWidget } from '../../requests/widgetRequests';
import { theme } from '../../theme';
import { getWidgetNames } from '../../utils';
import { StoreState } from '../../store';
import { connect } from 'react-redux';

type EditWidgetFormProps = ReturnType<typeof mapStateToProps> &
  Widget & {
    handleClick: () => void;
  };

type EditWidgetFormState = Widget & {
  widgetTypeOptions: { value: string; label: string }[];
  size: string;
  sizeOptions: { value: string; label: string }[];
  error: boolean;
  loading: boolean;
  errorMessage: string;
};

export class EditWidgetForm extends React.Component<
  EditWidgetFormProps,
  EditWidgetFormState
> {
  state: EditWidgetFormState = {
    title: '',
    subtitle: '',
    widgetUrl: '',
    tags: '',
    type: '',
    widgetTypeOptions: getWidgetNames(this.props.componentList, false),
    size: '',
    sizeOptions: [
      { value: 'small', label: 'Small' },
      { value: 'medium', label: 'Medium' },
      { value: 'large', label: 'Large' },
      { value: 'extra-large', label: 'Extra Large' }
    ],
    image: '',
    error: false,
    errorMessage: '',
    loading: false
  };

  handleFieldChange(event: React.ChangeEvent<{}>, which: string): void {
    //@ts-ignore
    this.setState({
      [which]: (event.target as HTMLFormElement).value
    });
  }

  submitWidget(): void {
    if (
      this.state.title &&
      this.state.subtitle &&
      this.state.widgetUrl &&
      this.state.type &&
      this.state.size
    ) {
      const body = this.transformData(this.state);
      this.setState({
        error: false,
        loading: true
      });
      this.props.modelId &&
        updateWidget(this.props.modelId, body)
          .then(() => this.props.handleClick())
          .catch(() => {
            this.setState({
              error: true,
              errorMessage: `Something went wrong, please try again or contact an admin`,
              loading: false
            });
          });
    } else {
      this.setState({ error: true });
    }
  }

  transformData(state: EditWidgetFormState): EditWidgetFormState {
    const body = state;
    body.widgetConfigparms = [
      {
        key: 'widgetSize',
        value: state.size
      }
    ];

    body.image = `../../assets/${body.type}.png`;

    delete body.size;
    delete body.error;
    return body;
  }

  render() {
    return (
      <Container>
        {!this.state.error ? null : (
          <div
            style={{
              color: '#f44336',
              paddingTop: '10px',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center'
            }}
          >
            Please fill in all required fields.
          </div>
        )}
        <form
          className="widget-form"
          style={{
            display: 'flex',
            flexDirection: 'column',
            flexWrap: 'nowrap'
          }}
          noValidate
        >
          <TextField
            name="title"
            label="Widget Title"
            value={this.state.title || undefined}
            onChange={event => this.handleFieldChange(event, 'title')}
            required
            variant="outlined"
            placeholder="Enter the title of the widget"
            margin="normal"
            InputLabelProps={{ shrink: true }}
            error={this.state.error && !this.state.title}
          />

          <TextField
            name="subtitle"
            value={this.state.subtitle || undefined}
            onChange={event => this.handleFieldChange(event, 'subtitle')}
            variant="outlined"
            placeholder="Description *"
            margin="normal"
            InputLabelProps={{ shrink: true }}
            multiline
            rows={2}
            rowsMax={4}
            required
            error={this.state.error && !this.state.subtitle}
          />

          <TextField
            name="widgetUrl"
            label="Widget URL"
            value={this.state.widgetUrl || undefined}
            onChange={event => this.handleFieldChange(event, 'widgetUrl')}
            variant="outlined"
            placeholder="Enter the URL for the widget"
            required
            margin="normal"
            InputLabelProps={{ shrink: true }}
            error={this.state.error && !this.state.widgetUrl}
          />

          <TextField
            name="tags"
            label="Tags"
            value={this.state.tags || undefined}
            placeholder="Add tags for your widget"
            onChange={event => this.handleFieldChange(event, 'tags')}
            error={this.state.error && !this.state.tags}
            variant="outlined"
          />

          <OutlinedSelect
            inputLabel="Type"
            selectId="type"
            value={this.state.type || undefined}
            options={this.state.widgetTypeOptions}
            placeholder="Select the type of your widget"
            required
            onChange={event => this.handleFieldChange(event, 'type')}
            error={this.state.error && !this.state.type}
          />

          <OutlinedSelect
            inputLabel="Sizes"
            selectId="size"
            value={this.state.size || undefined}
            options={this.state.sizeOptions}
            placeholder="Select which size your widget is utilizing"
            required
            onChange={event => this.handleFieldChange(event, 'size')}
            error={this.state.error && !this.state.size}
          />

          <GenericButton
            textColor={theme.palette.primary.main}
            bdColor={theme.palette.primary.main}
            handleClick={() => this.submitWidget()}
          >
            SUBMIT WIDGET
          </GenericButton>

          {this.state.loading ? <LoadingNotifier /> : null}
        </form>
      </Container>
    );
  }
}
function mapStateToProps(store: StoreState) {
  return {
    componentList: store.app.componentLists
  };
}
export default connect(mapStateToProps)(EditWidgetForm);
