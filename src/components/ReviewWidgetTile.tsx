import * as React from 'react';
import { connect } from 'react-redux';
import { Card, CardContent, Typography, Grid } from '@material-ui/core';

import { StoreState } from '../store';
import { updateWidget } from '../requests/widgetRequests';
import { getPendingWidgets } from '../requests/getPendingWidgets';
import { getAvailableWidgets } from '../requests/getAvailableWidgets';
import { formatDate } from '../utils';
import Snackbar from './Snackbar';
import GenericButton from './shared/GenericButton';
import { Widget } from '../models/marketplace/widget.model';

import '../css/MarketplaceTile.css';
import { theme } from '../theme';

export type ReviewWidgetTileProps = Widget & {
  onClick?: (ev: React.MouseEvent<HTMLElement, MouseEvent>) => void;
};

class ReviewWidgetTile extends React.Component<
  ReturnType<typeof mapStateToProps> &
    ReturnType<typeof mapDispatchToProps> &
    ReviewWidgetTileProps
> {
  state = {
    openSnackbar: false,
    snackbarStyle: 'good',
    snackbarMessage: '',
    showActions: true
  };

  async approvePendingWidget() {
    this.setState({
      showActions: false
    });
    if (!this.props.modelId) return;

    await this.props.updateWidget(this.props.modelId, {
      status: 'A'
    });
    this.props.getAvailableWidgets();
    this.props.getPendingWidgets();
    this.setState({
      openSnackbar: true,
      snackbarStyle: 'good',
      snackbarMessage: 'Widget successfully added to Marketplace'
    });
  }

  async declinePendingWidget() {
    this.setState({
      showActions: false
    });
    if (!this.props.modelId) return;

    await this.props.updateWidget(this.props.modelId, {
      status: 'R'
    });
    this.props.getPendingWidgets();
    this.setState({
      openSnackbar: true,
      snackbarStyle: 'warn',
      snackbarMessage: 'Widget was declined, please contact submittee'
    });
  }

  handleSnackbarClose() {
    this.setState({
      openSnackbar: false
    });
  }

  render() {
    return (
      <>
        <Card style={{ width: '100%' }}>
          <CardContent>
            <Grid container>
              <Grid item zeroMinWidth md xs={12}>
                <Typography variant="h2">{this.props.title}</Typography>
                {this.props.subtitle ? (
                  <>
                    <br />
                    <Typography
                      variant="subtitle1"
                      color="textSecondary"
                      style={{ paddingBottom: '4px' }}
                    >
                      DESCRIPTION
                    </Typography>
                    <Typography variant="body2">
                      {this.props.subtitle}
                    </Typography>
                  </>
                ) : null}
                {this.props.widgetConfigparms || this.props.isAdmin ? (
                  <>
                    <br />
                    <Typography
                      variant="subtitle1"
                      color="textSecondary"
                      style={{ paddingBottom: '8px' }}
                    >
                      SPECS
                    </Typography>

                    {this.props.widgetConfigparms &&
                      this.props.widgetConfigparms.map(p =>
                        p.key ? (
                          <div
                            key={`ReviewWidgetTile${p.key}`}
                            style={{ paddingBottom: '8px' }}
                          >
                            <Typography variant="h4">
                              {p.key
                                .replace(/([A-Z])/g, ' $1')
                                .replace(/^./, function(str) {
                                  return str.toUpperCase();
                                })}
                            </Typography>{' '}
                            <Typography variant="h3"> {p.value}</Typography>
                          </div>
                        ) : null
                      )}
                    {!this.props.isAdmin ? null : (
                      <>
                        <div style={{ paddingBottom: '8px' }}>
                          <Typography variant="h4">Created By</Typography>{' '}
                          <Typography variant="h3">
                            {this.props.createdBy}
                          </Typography>
                        </div>

                        {this.props.createdOn ? (
                          <div style={{ paddingBottom: '8px' }}>
                            <Typography variant="h4">Created On</Typography>{' '}
                            <Typography variant="h3">
                              {formatDate(this.props.createdOn)}
                            </Typography>
                          </div>
                        ) : null}
                      </>
                    )}
                  </>
                ) : null}
              </Grid>
              <Grid item md xs={12} className="marketplace-tile-graph">
                <img
                  src={this.props.image || undefined}
                  alt="Widget Display Graphic"
                  onClick={ev => this.props.onClick && this.props.onClick(ev)}
                />
                <div
                  className="marketplace-tile-graph-overlay"
                  onClick={ev => this.props.onClick && this.props.onClick(ev)}
                >
                  <button>View</button>
                </div>
              </Grid>
              <Grid
                item
                container
                md
                xs={12}
                justify="flex-end"
                alignItems="center"
              >
                {this.state.showActions ? (
                  <>
                    <GenericButton
                      textColor={theme.palette.secondary.main}
                      bdColor={theme.palette.secondary.main}
                      handleClick={() => this.approvePendingWidget()}
                    >
                      PUBLISH WIDGET
                    </GenericButton>
                    <GenericButton
                      textColor={theme.palette.error.main}
                      bdColor={theme.palette.error.main}
                      handleClick={() => this.declinePendingWidget()}
                    >
                      Decline
                    </GenericButton>
                  </>
                ) : null}
              </Grid>
            </Grid>
          </CardContent>
        </Card>

        <Snackbar
          message={this.state.snackbarMessage}
          display={this.state.openSnackbar}
          styling={this.state.snackbarStyle}
          onClose={() => this.handleSnackbarClose()}
        />
      </>
    );
  }
}

function mapStateToProps(state: StoreState) {
  return {
    isAdmin: (state.app.user && state.app.user.isAdmin) || false,
    dashboardOrder: state.dashboard.dashboardWidgets
  };
}

function mapDispatchToProps() {
  return {
    updateWidget,
    getAvailableWidgets,
    getPendingWidgets
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReviewWidgetTile);
