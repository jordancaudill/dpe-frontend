import * as React from 'react';
import CloseIcon from '@material-ui/icons/Close';

import BGOverlay from './BGOverlay';

import '../css/Modals.css';

type ModalProps = {
  onClick?: () => void;
  content?: JSX.Element | null;
  title?: string;
};

class Modal extends React.Component<ModalProps> {
  render() {
    return this.props.content ? (
      <>
        <div className="modal no-scrollbars">
          {this.props.title ? (
            <div className="modal-title">{this.props.title}</div>
          ) : null}

          <button
            className="circle-button"
            onClick={() => this.props.onClick && this.props.onClick()}
          >
            <CloseIcon></CloseIcon>
          </button>

          <div className="modal-content">{this.props.content}</div>
        </div>

        <BGOverlay
          display={true}
          extraClasses="modal-overlay"
          onClick={() => this.props.onClick && this.props.onClick()}
        />
      </>
    ) : null;
  }
}

export default Modal;
