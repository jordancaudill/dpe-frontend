import * as React from 'react';

import { getParentNodeByClass } from '../utils';
import { Widget, WidgetProps } from './Widget';

type FlippableWidgetState = {
  classes: string;
  flipped: boolean;
};

type FlippableWidgetProps = WidgetProps & {
  startSide: string;
  classes?: string;
  beforeFlip?: () => void;
  afterFlip?: () => void;
  widgetSize: string;
  widgetId: string;
  front: JSX.Element;
  frontHeader?: JSX.Element;
  frontFooter?: JSX.Element;
  backHeader?: JSX.Element;
  backFooter?: JSX.Element;
  back: JSX.Element;
};

export default class FlippableWidget extends React.Component<
  FlippableWidgetProps,
  FlippableWidgetState
> {
  currentSide = this.props.startSide || 'front';
  defaultClasses = `item flippable ${
    this.props.classes ? this.props.classes : ''
  }`;
  state = {
    classes: `${this.defaultClasses} ${this.currentSide} ${
      this.currentSide === 'back' ? 'flipped' : null
    }`,
    flipped: this.currentSide !== 'front'
  };

  flipWidgetOnChildClick(ev: React.MouseEvent<HTMLDivElement, MouseEvent>) {
    if (getParentNodeByClass(ev.target as HTMLElement, 'flip-widget')) {
      this.flipWidget();
    }
  }

  flipWidget() {
    if ('function' === typeof this.props.beforeFlip) {
      this.props.beforeFlip();
    }

    this.currentSide = this.currentSide === 'front' ? 'back' : 'front';
    this.setState({
      classes:
        (!this.state.flipped
          ? this.defaultClasses + ' flipped'
          : this.defaultClasses) +
        ' ' +
        this.currentSide,
      flipped: !this.state.flipped
    });

    if ('function' === typeof this.props.afterFlip) {
      this.props.afterFlip();
    }
  }

  render() {
    return (
      <div
        className={this.state.classes}
        style={{
          height: '100%',
          marginRight: '-18px',
          marginTop: '-14px'
        }}
        onClick={ev => this.flipWidgetOnChildClick(ev)}
        // style={{ order: this.props.order }}
        id={this.props.widgetId}
      >
        <Widget
          hideOptions={this.props.hideOptions}
          classes="front"
          widgetId={this.props.widgetId}
          refreshClick={this.props.refreshClick}
          removeClick={this.props.removeClick}
          notItem={true}
          title={this.props.frontHeader || this.props.title}
          footer={this.props.frontFooter || this.props.footer}
        >
          {this.props.front}
        </Widget>

        <Widget
          hideOptions={this.props.hideOptions}
          classes="back"
          widgetId={this.props.widgetId}
          refreshClick={this.props.refreshClick}
          removeClick={this.props.removeClick}
          notItem={true}
          title={this.props.backHeader || this.props.title}
          footer={this.props.backFooter || this.props.footer}
        >
          {this.props.back}
        </Widget>

        {this.props.children}
      </div>
    );
  }
}
