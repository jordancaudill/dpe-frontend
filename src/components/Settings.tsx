import * as React from 'react';

//@ts-ignore
import settings_icon from '../assets/settings_icon.svg';

export class Settings extends React.Component {
  render() {
    return (
      <div className="settings">
        <img src={settings_icon} alt="settings" />
      </div>
    );
  }
}

export default Settings;
