import * as React from 'react';
import { connect } from 'react-redux';
import LoginPage from './LoginPage';
import MainPage from './MainPage';
import { StoreState } from '../store';

type HomePageProps = ReturnType<typeof mapStateToProps>;

class HomePage extends React.Component<HomePageProps> {
  shouldComponentUpdate(nextProps: HomePageProps) {
    return (
      this.props.oidc.isLoadingUser !== nextProps.oidc.isLoadingUser || // isLoadingUser changed
      ('user' in this.props.oidc &&
      'user' in nextProps.oidc && // Make sure user object exists in oidc
      typeof this.props.oidc.user === 'object' &&
      typeof nextProps.oidc.user === 'object' &&
      this.props.oidc.user &&
      nextProps.oidc.user &&
      'expired' in this.props.oidc.user &&
      'expired' in nextProps.oidc.user && // Make sure expired exists in oidc.user
        this.props.oidc.user.expired !== nextProps.oidc.user.expired) ||
      (this.props.user && this.props.user.id && this.props.user.id) !==
        (nextProps.user && nextProps.user.id && nextProps.user.id) // user id changed
    );
  }

  render() {
    return ('user' in this.props.oidc &&
      this.props.oidc.user &&
      this.props.oidc.isLoadingUser) ||
      !this.props.oidc.isAuthenticated ||
      !this.props.oidc.user ||
      this.props.oidc.user.expired ||
      !this.props.user ? (
      <LoginPage />
    ) : (
      <MainPage />
    );
  }
}

function mapStateToProps(state: StoreState) {
  return {
    oidc: state.oidc,
    user: state.app.user
  };
}

export default connect(mapStateToProps)(HomePage);
