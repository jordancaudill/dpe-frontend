import * as React from 'react';
import CheckIcon from '@material-ui/icons/Check';
import PriorityHighIcon from '@material-ui/icons/PriorityHigh';

import '../css/Snackbar.css';

type SnackbarProps = {
  styling?: string;
  message?: string;
  display?: boolean;
  onClose: () => void;
};

// TODO we should probably just use material-ui Snackbar instead...
export default class Snackbar extends React.Component<SnackbarProps> {
  snackbar = React.createRef<HTMLDivElement>();
  snackbarBaseClass = 'snackbar fadeable';
  timeout?: number = undefined;
  timeoutTiming = 6000;

  componentWillUpdate(nextProps: SnackbarProps) {
    if (nextProps.display) {
      const openSnackbars = document.querySelectorAll('.snackbar.glide-in');
      for (let i = 0; i < openSnackbars.length; i++) {
        openSnackbars[
          i
        ].className = `${this.snackbarBaseClass} fade-out glide-in`;
      }

      if (this.snackbar.current)
        this.snackbar.current.className = `${this.snackbarBaseClass} fade-in glide-in`;

      window.clearTimeout(this.timeout);
      this.timeout = window.setTimeout(() => {
        if (this.snackbar.current)
          this.snackbar.current.className = `${this.snackbarBaseClass} fade-out glide-in`;

        window.setTimeout(() => {
          if (this.snackbar.current)
            this.snackbar.current.className = `${this.snackbarBaseClass} fade-out`;
        }, 600);

        this.props.onClose();
      }, this.timeoutTiming + 600);
    }
  }

  componentWillUnmount() {
    window.clearTimeout(this.timeout);
  }

  render() {
    const styles = {
      IconStyle: {
        position: 'absolute' as 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        fontSize: '3rem',
        fontWeight: 'bold' as 'bold',
        color: '#33b871'
      }
    };

    return (
      <div
        className={`${this.snackbarBaseClass} fade-out display-none`}
        ref={this.snackbar}
      >
        <div
          className={
            this.props.styling === 'warn' ? 'exclamation-box' : 'checkmark-box'
          }
        >
          {this.props.styling === 'warn' ? (
            <PriorityHighIcon style={styles.IconStyle}></PriorityHighIcon>
          ) : (
            <CheckIcon style={styles.IconStyle}></CheckIcon>
          )}
        </div>

        <div className="copy">{this.props.message}</div>
      </div>
    );
  }
}
