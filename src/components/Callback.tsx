import * as React from 'react';
import { connect } from 'react-redux';
import { CallbackComponent } from 'redux-oidc';
import { push, CallHistoryMethodAction } from 'connected-react-router';
import userManager from '../userManager';
import { getUser } from '../requests/userRequests';
import { Path, LocationState } from 'history';

type CallbackPageProps = ReturnType<typeof mapDispatchToProps>;

class CallbackPage extends React.Component<CallbackPageProps> {
  render() {
    return (
      <CallbackComponent
        userManager={userManager}
        successCallback={() => {
          // Here we get the info from oidc, but we are actually
          // setting the app user object via middleware to keep
          // the app more testable as independent parts
          getUser().then(() => this.props.push('/'));
        }}
        errorCallback={err => {
          console.error(err);

          this.props.push('/error');
        }}
      >
        <div>Redirecting...</div>
      </CallbackComponent>
    );
  }
}

function mapDispatchToProps(
  dispatch: React.Dispatch<CallHistoryMethodAction<[Path, LocationState?]>>
) {
  return {
    push: (route: string) => dispatch(push(route))
  };
}

export default connect(
  () => {
    return {};
  },
  mapDispatchToProps
)(CallbackPage);
