import * as React from 'react';
import IconButton from '@material-ui/core/IconButton';

//@ts-ignore
import exelon_logo from '../assets/inverted_exelon_logo.svg';

export class LogoIcon extends React.Component {
  render() {
    return (
      <IconButton component="a" href="/">
        <img src={exelon_logo} alt="exelon logo" width="104" height="42" />
      </IconButton>
    );
  }
}

export default LogoIcon;
