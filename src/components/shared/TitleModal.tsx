import * as React from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  Typography,
  IconButton
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { DialogProps } from '@material-ui/core/Dialog';

type TitleModalProps = DialogProps & { title: string };

export default function TitleModal(props: TitleModalProps) {
  return (
    <Dialog {...props}>
      <DialogTitle
        id="submit-modal-title"
        style={{
          margin: '0px',
          padding: '32px 32px 16px 32px'
        }}
      >
        <Typography variant="h1" component="span">
          {props.title}
        </Typography>
        <IconButton
          aria-label="close modal"
          onClick={ev => props.onClose && props.onClose(ev, 'backdropClick')}
          style={{
            position: 'absolute',
            right: '8px',
            top: '8px'
          }}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent style={{ padding: '32px' }}>
        {props.children}
      </DialogContent>
    </Dialog>
  );
}
