import * as React from 'react';
import LaunchIcon from '@material-ui/icons/Launch';
import GenericIcon from './GenericIcon';
import { Link } from '../../models/app/link.model';

export default function ExternalIcon(props: Link) {
  return props.icon ? (
    <GenericIcon src={props.icon} alt={props.title} />
  ) : (
    <LaunchIcon fontSize="large" style={{ width: '16px', height: '16px' }} />
  );
}
