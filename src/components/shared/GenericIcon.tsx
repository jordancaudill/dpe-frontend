import * as React from 'react';
import Icon from '@material-ui/core/Icon';

type GenericIconProps = {
  src: any;
  alt: string;
};

export class GenericIcon extends React.Component<GenericIconProps> {
  render() {
    return (
      <Icon>
        <img src={this.props.src} alt={this.props.alt} width="18" height="18" />
      </Icon>
    );
  }
}

export default GenericIcon;
