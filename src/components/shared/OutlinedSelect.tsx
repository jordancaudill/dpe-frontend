import * as React from 'react';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import { Option } from './Option';

type OutlinedSelectProps = {
  selectId: string;
  value?: string;
  inputLabel: string;
  options: Option[];
  placeholder: string;
  required: boolean;
  error: boolean;
  onChange: (event: React.ChangeEvent<{}>) => void;
  width?: string;
};

export class OutlinedSelect extends React.Component<OutlinedSelectProps> {
  render() {
    return (
      <div style={{ paddingTop: '10px' }}>
        <TextField
          id={this.props.selectId}
          select
          label={this.props.inputLabel}
          value={this.props.value || ''}
          onChange={this.props.onChange}
          margin="normal"
          variant="outlined"
          required={this.props.required}
          error={this.props.error}
          InputLabelProps={{ shrink: true }}
          style={{ width: this.props.width }}
        >
          <MenuItem value="" disabled>
            {this.props.placeholder}
          </MenuItem>
          {this.props.options.map(option => (
            <MenuItem key={option.value} value={option.value}>
              {option.label}
            </MenuItem>
          ))}
        </TextField>
      </div>
    );
  }
}

export default OutlinedSelect;
