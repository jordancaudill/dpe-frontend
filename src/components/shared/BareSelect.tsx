import * as React from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import KeyboardArrowDownOutlinedIcon from '@material-ui/icons/KeyboardArrowDownOutlined';
import { Option } from './Option';
import { theme } from '../../theme';

type BareSelectProps = {
  inputLabel?: string;
  selectId: string;
  options: Option[];
  color?: string;
  padding?: string;
  onChange?: (event: React.ChangeEvent<{}>) => void;
  currentSelection?: string;
};

const DropDownArrow = () => (
  <KeyboardArrowDownOutlinedIcon
    style={{
      position: 'absolute',
      top: 'calc(50% - 12px)',
      right: '0'
    }}
  />
);

export class BareSelect extends React.Component<BareSelectProps> {
  render() {
    return (
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          padding: this.props.padding
        }}
      >
        {this.props.inputLabel ? (
          <InputLabel
            style={{ paddingRight: '25px' }}
            htmlFor={this.props.selectId}
          >
            {this.props.inputLabel}
          </InputLabel>
        ) : null}
        <Select
          id={this.props.selectId}
          value={
            this.props.currentSelection ||
            (this.props.options.length && this.props.options[0].value)
          }
          disableUnderline
          IconComponent={DropDownArrow}
          style={{
            color: this.props.color || theme.palette.primary.main
          }}
          onChange={this.props.onChange}
        >
          {this.props.options.map(option => (
            <MenuItem
              value={option.value}
              key={`bareSelect${this.props.selectId}${option.value}`}
            >
              {option.label}
            </MenuItem>
          ))}
        </Select>
      </div>
    );
  }
}

export default BareSelect;
