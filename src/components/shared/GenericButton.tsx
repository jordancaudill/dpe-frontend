import * as React from 'react';
import { Button } from '@material-ui/core';

type GenericButtonProps = {
  handleClick?: (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => void;
  textColor: string;
  bdColor: string;
  backgroundColor?: string;
};

export class GenericButton extends React.Component<GenericButtonProps> {
  render() {
    return (
      <Button
        style={{
          minWidth: '172px',
          height: '37px',
          borderRadius: '18.5px',
          border: 'solid 2px',
          backgroundColor: this.props.backgroundColor,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          color: this.props.bdColor
        }}
        className="button"
        onClick={this.props.handleClick}
      >
        <div
          style={{
            fontSize: '14px',
            letterSpacing: '0.79px',
            textAlign: 'center',
            color: this.props.textColor
          }}
        >
          {this.props.children}
        </div>
      </Button>
    );
  }
}

export default GenericButton;
