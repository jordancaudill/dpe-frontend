import * as React from 'react';

//@ts-ignore
import search_icon from '../../assets/search_icon.svg';
import { TextField } from '@material-ui/core';
import { useSelector } from 'react-redux';
import { StoreState } from '../../store';

export default function SearchBar() {
  const quicklinks: any = useSelector(
    (state: StoreState) => state.app.quickLinks['nukbook']
  );
  const handleKeyDown = (e: React.KeyboardEvent) => {
    if (e.key === 'Enter') {
      e.preventDefault();
      var searchUrl = quicklinks.link + (e.target as HTMLTextAreaElement).value;
      window.open(searchUrl, '_blank');
      (e.target as HTMLTextAreaElement).value = '';
    }
  };

  return (
    <form
      style={{
        width: '295px',
        height: '37px',
        borderRadius: '18.5px',
        border: 'solid 1px #d3d2d2',
        backgroundColor: '#ffffff',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
      }}
    >
      <img src={search_icon} alt="search" style={{ paddingLeft: '95px' }} />
      <TextField
        placeholder={'Search'}
        InputProps={{
          disableUnderline: true,
          style: { fontFamily: 'ProximaNova-Semibold' }
        }}
        style={{ paddingLeft: '10px' }}
        onKeyDown={handleKeyDown}
      />
    </form>
  );
}
