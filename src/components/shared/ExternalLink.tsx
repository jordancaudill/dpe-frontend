import * as React from 'react';
import LaunchIcon from '@material-ui/icons/Launch';
import GenericIcon from './GenericIcon';
import { Link } from '../../models/app/link.model';

type ExternalLinkProps = {
  item: Link;
};

export class ExternalLink extends React.Component<ExternalLinkProps> {
  render() {
    return (
      <a href={this.props.item.link} target="_blank" rel="noopener noreferrer">
        {this.props.item.icon ? (
          <GenericIcon src={this.props.item.icon} alt={this.props.item.title} />
        ) : (
          <LaunchIcon
            fontSize="large"
            style={{ width: '16px', height: '16px' }}
          />
        )}
      </a>
    );
  }
}

export default ExternalLink;
