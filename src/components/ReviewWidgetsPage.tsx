import * as React from 'react';
import ReviewHeader from './reviewwidgets/ReviewHeader';
import ReviewWidgetList from './reviewwidgets/ReviewWidgetList';

import '../css/review_widgets.css';

export class ReviewWidgetsPage extends React.Component {
  render() {
    return (
      <div>
        <ReviewHeader />
        <ReviewWidgetList />
      </div>
    );
  }
}

export default ReviewWidgetsPage;
