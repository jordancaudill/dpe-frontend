import * as React from 'react';
import { Action } from 'redux';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Responsive, WidthProvider, Layout, Layouts } from 'react-grid-layout';
import { Card, CardContent } from '@material-ui/core';

import { StoreState } from '../store';
import { Breakpoint } from '../actions/dashboard/types';
import * as GridActions from '../actions/dashboard/actions';
import { getComponents } from '../requests/getComponentsRequests';
import { getAvailableWidgets } from '../requests/getAvailableWidgets';
import DashboardWidgetComponent from './DashboardWidget';
import DashboardHeader from './DashboardHeader';

import { DashboardWidgetOrder } from '../models/marketplace/dashboardwidgetorder.model';

import '../../node_modules/react-grid-layout/css/styles.css';
import '../../node_modules/react-resizable/css/styles.css';

const ResponsiveReactGridLayout = WidthProvider(Responsive);

type WidgetGridState = {
  mounted: boolean;
};

type WidgetGridProps = ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatchToProps> & {
    rowHeight: number;
    hideChrome?: boolean;
  };

export class WidgetGrid extends React.Component<
  WidgetGridProps,
  WidgetGridState
> {
  static defaultProps = {
    rowHeight: 40
  };
  state: WidgetGridState = {
    mounted: false
  };

  componentDidMount() {
    this.setState({
      mounted: true
    });
    getAvailableWidgets();
    getComponents();
  }

  onLayoutChange(layout: Layout[], layouts: Layouts) {
    this.props.setGridLayout(layouts);
  }

  onBreakpointChange(breakpoint: Breakpoint, cols: number) {
    this.props.setGridBreakpoint(breakpoint);
  }

  generateDOM(dashboardWidgets: DashboardWidgetOrder[]) {
    return dashboardWidgets.map((widget, i) => {
      const layout = this.props.layouts[this.props.breakpoint].find(
        layout => layout.i === widget.instanceId
      );
      if (!layout) return null;
      const hideChrome = layout.h === 1;

      if (hideChrome) {
        return (
          <div key={widget.instanceId} style={{ overflow: 'hidden' }}>
            <div style={{ marginRight: '24px' }}>
              <DashboardWidgetComponent
                {...widget}
                hideChrome={hideChrome}
              ></DashboardWidgetComponent>
            </div>
          </div>
        );
      }

      return (
        <Card key={widget.instanceId}>
          <CardContent
            style={{
              display: 'block',
              position: 'absolute',
              height: 'auto',
              bottom: 0,
              top: 0,
              left: 0,
              right: 0
            }}
          >
            <DashboardWidgetComponent
              {...widget}
              hideChrome={layout.y === 0}
            ></DashboardWidgetComponent>
          </CardContent>
        </Card>
      );
    });
  }

  combineLayouts(layouts: Layouts) {
    return {
      [this.props.breakpoint]: [
        ...layouts[this.props.breakpoint],
        {
          i: 'head',
          x: 0,
          y: 1,
          h: 2,
          w: 12,
          static: true
        }
      ]
    };
  }

  render() {
    return (
      <ResponsiveReactGridLayout
        {...this.props}
        layouts={this.combineLayouts(this.props.layouts)}
        onLayoutChange={(layout, layouts) =>
          this.onLayoutChange(layout, layouts)
        }
        onBreakpointChange={(newBreakpoint, newCols) =>
          this.onBreakpointChange(newBreakpoint as Breakpoint, newCols)
        }
        measureBeforeMount={false}
        useCSSTransforms={this.state.mounted}
        preventCollision={false}
        draggableHandle=".draggableHandle"
        margin={[20, 20]}
        containerPadding={[10, 10]}
      >
        <div key="head">
          <DashboardHeader />
        </div>
        {this.generateDOM(this.props.dashboardWidgets)}
      </ResponsiveReactGridLayout>
    );
  }
}

function mapStateToProps(state: StoreState) {
  return {
    dashboardWidgets: state.dashboard.dashboardWidgets,
    breakpoint: state.dashboard.breakpoint,
    cols: state.dashboard.cols,
    layouts: state.dashboard.layouts
  };
}

function mapDispatchToProps(
  dispatch: ThunkDispatch<StoreState, undefined, Action>
) {
  return {
    setGridBreakpoint: (breakpoint: Breakpoint) =>
      dispatch(GridActions.setGridBreakpoint(breakpoint)),
    setGridLayout: (layouts: Layouts) =>
      dispatch(GridActions.setGridLayout(layouts))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WidgetGrid);
