import * as React from 'react';
import { connect } from 'react-redux';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ReviewWidgetTile from '../ReviewWidgetTile';

import { StoreState } from '../../store';
import { getPendingWidgets } from '../../requests/getPendingWidgets';
import { Widget } from '../../models/marketplace/widget.model';

type ReviewWidgetListProps = ReturnType<typeof mapStateToProps>;

type ReviewWidgetListState = {
  widgets: Widget[];
  selectedIndex: number;
  activeIndex: number;
};

export class ReviewWidgetList extends React.Component<
  ReviewWidgetListProps,
  ReviewWidgetListState
> {
  state: ReviewWidgetListState = {
    widgets: this.props.reviewWidgetRequests,
    selectedIndex: 0,
    activeIndex: 0
  };

  componentDidMount() {
    this.fetchWidgetRequests();
  }

  fetchWidgetRequests() {
    return getPendingWidgets()
      .then(json => {
        this.setState({
          widgets: json
        });
      })
      .catch(error => console.error(error));
  }

  handleListItemClick(
    event: React.MouseEvent<HTMLElement, MouseEvent>,
    index: number
  ): void {
    this.setState({ selectedIndex: index });
  }

  render() {
    return (
      <div className="container">
        <List aria-label="widgets to review">
          {this.state.widgets.map((request, i) => (
            <ListItem selected={this.state.selectedIndex === i} key={`${i}`}>
              <ReviewWidgetTile
                key={`reviewWidgetList${i}`}
                onClick={event => this.handleListItemClick(event, i)}
                {...request}
              />
            </ListItem>
          ))}
        </List>
      </div>
    );
  }
}

function mapStateToProps(state: StoreState) {
  return {
    reviewWidgetRequests: state.app.reviewWidgetRequests
  };
}

export default connect(mapStateToProps)(ReviewWidgetList);
