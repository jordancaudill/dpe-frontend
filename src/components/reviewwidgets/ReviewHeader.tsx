import * as React from 'react';
import { useSelector } from 'react-redux';
import { Tabs, Tab, Typography } from '@material-ui/core';

import { StoreState } from '../../store';

export default function ReviewHeader() {
  const [value, setValue] = React.useState(0);
  const reviewWidgetRequestCount: any = useSelector(
    (state: StoreState) => state.app.reviewWidgetRequestCount
  );

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  return (
    <>
      <Tabs
        value={value}
        onChange={handleChange}
        indicatorColor="primary"
        textColor="primary"
        centered
      >
        <Tab label="NEEDS REVIEW" />
        <Tab label="HISTORY" />
      </Tabs>
      <div
        style={{
          position: 'absolute' as 'absolute',
          top: '64px'
        }}
      >
        <Typography variant="h3" component="span">
          <span
            style={{
              color: '#5ae4c5'
            }}
          >
            ({reviewWidgetRequestCount})
          </span>{' '}
          TO REVIEW
        </Typography>
      </div>
    </>
  );
}
