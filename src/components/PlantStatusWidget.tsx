import * as React from 'react';
import { Action } from 'redux';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import {
  ScatterChart,
  Scatter,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
  Text,
  Label
} from 'recharts';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';

import { StoreState } from '../store';
import { updateDashboard } from '../requests/userRequests';
import LoadingNotifier from './LoadingNotifier';
import Error from './Error';
import { GaugeChart, DonutChart } from './GaugeChart';
import { Widget, WidgetProps } from './Widget';
import FlippableWidget from './FlippableWidget';
import PlantLocationsDropdown from './PlantLocationsDropdown';
import {
  fetchWrapper,
  uomConverter,
  parseTime,
  formatDate,
  getCurrentTime
} from '../utils';
import { ONE_HOUR } from '../vars';
import { UserDashboard } from '../models/marketplace/userdashboard.model';
import { Plant } from '../models/hl_data/plant.model';
import { GraphResponse } from '../models/hl_data/graphresponse.model';
import {
  LineChart,
  LineChartData,
  LineChartUnit
} from '../models/hl_data/linechart.model';
import {
  GaugeChart as GaugeChartModel,
  GaugeChartGroup
} from '../models/hl_data/guagechart.model';

import '../css/PlantStatus.css';
import { Grid, Typography } from '@material-ui/core';
import WarningIcon from '@material-ui/icons/Warning';

type CustomXAxisTickProps = {
  width: number;
  visibleTicksCount: number;
  index: number;
  y: number;
  dy: number;
  payload?: unknown;
};

type CustomYAxisTickProps = {
  uom: string;
  payload?: unknown;
};

type CustomYAxisLabelProps = {
  value: string;
  viewBox: unknown;
};

const CustomXAxisTick = (props: CustomXAxisTickProps) => {
  const width = 48;
  const height = 40;
  let x =
    (props.width / props.visibleTicksCount) * props.index +
    width / 2 -
    width * props.index;
  if (props.index === 0) {
    x += width / 2;
  }

  //@ts-ignore
  const payloadVal = props.payload.value;
  return (
    <>
      <text
        width={width}
        height={height}
        x={x}
        dx={x + width / 2}
        y={props.y - 0.8 * props.dy + height / 2}
        textAnchor="middle"
        dominantBaseline="central"
      >
        {formatDate(payloadVal)}
      </text>

      <text
        width={width}
        height={height}
        x={x}
        dx={x + width / 2}
        y={props.y - 0.85 * props.dy + height}
        textAnchor="middle"
        dominantBaseline="central"
      >
        {parseTime(payloadVal)}
      </text>
    </>
  );
};

const CustomYAxisTick = (props: CustomYAxisTickProps) => {
  const uom = !/percent/i.test(props.uom) ? '' : '%';
  //@ts-ignore
  const payloadVal = props.payload.value;
  const val = /\./.test(payloadVal)
    ? parseFloat(payloadVal).toFixed(2)
    : payloadVal;

  return (
    <Text textAnchor={'middle'} verticalAnchor={'middle'} {...props}>
      {`${(val + '').length <= 6 ? val : Math.round(val)}${uom}`}
    </Text>
  );
};

const CustomYAxisLabel = (props: CustomYAxisLabelProps) => {
  return !/percent/i.test(props.value) ? (
    //@ts-ignore
    <text x={props.viewBox.width} y={12}>
      {props.value}
    </text>
  ) : null;
};

type ComparisonLineChartState = {
  opacity: { [dataKey: string]: number };
  clicked?: boolean;
  data: { x: number; y: number }[][];
  colorData: { start: string; stop: string }[];
  units: { name: string; shadowId: string; dataKey: string }[];
  yMin: number;
  yMax: number;
  tickCount: number;
  metric?: string;
};

type ComparisonLineChartProps = {
  timeline: LineChartData;
  selected?: string;
  period?: string;
};

class ComparisonLineChart extends React.Component<
  ComparisonLineChartProps,
  ComparisonLineChartState
> {
  state: ComparisonLineChartState = {
    opacity: {
      unit1: 1,
      unit2: 1
    },
    clicked: false,
    data: [],
    colorData: [],
    units: [],
    yMin: Infinity,
    yMax: -Infinity,
    tickCount: 10,
    metric: undefined
  };
  dropShadow = {
    x: 10,
    y: 10,
    deviation: 7
  };
  hiddenOpacity = 0.2;

  transformData = (timeline = this.props.timeline) => {
    const plantData: { x: number; y: number }[][] = [];
    const colorData: { start: string; stop: string }[] = [];
    const units: { name: string; shadowId: string; dataKey: string }[] = [];
    let yMin = Infinity;
    let yMax = -Infinity;
    let itemCount = 0;

    timeline.units &&
      timeline.units.length &&
      timeline.units.forEach((info, i) => {
        const unit = info.unit;
        const colorSplit = unit.color.split(',');

        units.push({
          name: unit.name,
          shadowId: `dropshadow${colorSplit[0]}`,
          dataKey: `unit${i + 1}`
        });

        if (colorSplit.length) {
          colorData.push({
            start: colorSplit[0],
            stop: colorSplit.length > 1 ? colorSplit[1] : colorSplit[0]
          });
        }

        if (
          'data' in info &&
          'data' in info.data &&
          info.data.data &&
          info.data.data.length > 1
        ) {
          plantData.push(
            info.data.data.map((y, i) => {
              const t = new Date(info.data.timestamps[i]);
              const x = t.getTime();

              ++itemCount;

              y = Math.round(y);

              if (y < yMin) {
                yMin = y;
              } else if (y > yMax) {
                yMax = y;
              }

              return { x, y };
            })
          );
        }
      });

    if (itemCount) {
      itemCount /= timeline.units.length;

      //@ts-ignore
      if (!itemCount % 2) {
        ++itemCount;
      }
    }

    this.setState({
      metric: this.props.selected,
      data: itemCount > 2 ? plantData : [],
      colorData,
      units,
      yMin,
      yMax,
      tickCount: itemCount
    });
  };

  displayBoth = (opts: { clicked?: boolean } = {}) => {
    if (this.state.clicked && opts.clicked !== false) {
      this.setState({
        opacity: {
          unit1: this.hiddenOpacity,
          unit2: this.hiddenOpacity,
          //@ts-ignore
          [this.state.clicked]: 1,
          //@ts-ignore
          [opts.clicked]: 1
        },
        ...opts
      });
    } else {
      this.setState({
        opacity: {
          unit1: 1,
          unit2: 1
        },
        ...opts
      });
    }
  };

  displayOneLine = (line: unknown, opts: { clicked?: boolean } = {}) => {
    if (
      //@ts-ignore
      this.state.clicked !== line.dataKey ||
      //@ts-ignore
      (opts.clicked && opts.clicked !== line.dataKey)
    ) {
      this.setState({
        opacity: {
          unit1: this.hiddenOpacity,
          unit2: this.hiddenOpacity,
          //@ts-ignore
          [line.dataKey]: 1
        },
        ...opts
      });
    }
  };

  handleClick = (line: unknown) => {
    if (!this.state.clicked) {
      //@ts-ignore
      this.displayOneLine(line, { clicked: line.dataKey });
    } else {
      this.displayBoth({ clicked: false });
    }
  };

  // Check incoming vs existing data
  // return true is data doesn't match
  checkNewData(currUnits: LineChartUnit[], nextUnits: LineChartUnit[]) {
    if (currUnits.length !== nextUnits.length) {
      // Data doesn't match if number of units don't match
      return true;
    }

    for (let i = 0; i < currUnits.length; i++) {
      if (currUnits[i].data && nextUnits[i].data) {
        const currData = currUnits[i].data.data;
        const nextData = nextUnits[i].data.data;
        const currTime = currUnits[i].data.timestamps;
        const nextTime = nextUnits[i].data.timestamps;

        if (
          currData.length !== nextData.length ||
          currTime.length !== nextTime.length
        ) {
          // If number of data points don't match, then this is new data
          return true;
        }

        for (let j = 0; j < currData.length; j++) {
          if (currData[j] !== nextData[j] || currTime[j] !== nextTime[j]) {
            // If at any time anything in the data doesn't match, short-circuit loops, this is new data
            return true;
          }
        }
      }

      return false;
    }

    return false; // If no data has changed
  }

  componentDidMount() {
    this.transformData();
  }

  componentWillUpdate(nextProps: ComparisonLineChartProps) {
    if (
      this.props.selected !== nextProps.selected ||
      this.props.period !== nextProps.period ||
      this.checkNewData(this.props.timeline.units, nextProps.timeline.units)
    ) {
      this.transformData(nextProps.timeline);

      return true;
    }
  }

  render() {
    const buffer = Math.ceil(
      ((this.state.yMax - this.state.yMin) * this.state.tickCount) / 100
    );

    return this.state.units.length && this.state.data.length ? (
      <ResponsiveContainer height="90%">
        <ScatterChart margin={{ top: 32, left: 16, right: 16, bottom: 16 }}>
          <defs>
            {this.state.colorData.map((color, i) => (
              <filter key={i} id={`dropshadow${color.start}`}>
                <feGaussianBlur
                  in="SourceGraphic"
                  stdDeviation={this.dropShadow.deviation}
                />

                <feOffset
                  dx={this.dropShadow.x}
                  dy={this.dropShadow.y}
                  result="offsetblur"
                />

                <feFlood floodColor={`#${color.start}`} floodOpacity="0.9" />

                <feComposite in2="offsetblur" operator="in" />

                <feMerge>
                  <feMergeNode />

                  <feMergeNode in="SourceGraphic" />
                </feMerge>
              </filter>
            ))}
          </defs>

          <CartesianGrid vertical={false} />

          {/* 
          // @ts-ignore */}
          <XAxis
            dataKey="x"
            scale="time"
            type="number"
            name="time"
            tickCount={this.state.tickCount}
            tickSize={2}
            dy={12}
            domain={[dataMin => dataMin, dataMax => dataMax]}
            //@ts-ignore
            tick={<CustomXAxisTick />}
          />

          {/* 
          // @ts-ignore */}
          <YAxis
            dataKey="y"
            type="number"
            name="value"
            tickCount={this.state.tickCount}
            dy={-3}
            tickSize={3}
            tick={<CustomYAxisTick uom={this.props.timeline.uom} />}
            domain={[
              () => this.state.yMin - buffer,
              () => this.state.yMax + buffer
            ]}
          >
            <Label
              value={this.props.timeline.uom}
              //@ts-ignore
              content={<CustomYAxisLabel />}
            />
          </YAxis>

          {/* <Tooltip
            // content={<ComparisonChartTooltip colorData={this.props.colorData} unit={this.props.unit} />}
            isAnimationActive={false}
          /> */}

          <Tooltip
            isAnimationActive={false}
            formatter={(value, name) => {
              if (name === 'time') {
                //@ts-ignore
                return `${formatDate(value)} ${parseTime(value)}`;
              } else {
                return `${value}${uomConverter(this.props.timeline.uom)}`;
              }
            }}
          />

          <Legend
            align="right"
            wrapperStyle={{
              bottom: 0,
              right: 0
            }}
            onClick={this.handleClick}
            onMouseEnter={this.displayOneLine}
            onMouseLeave={this.displayBoth}
          />

          {this.state.units.map((unit, i) => (
            <Scatter
              key={i}
              data={this.state.data[i]}
              name={unit.name}
              shape="circle"
              fill={`#${this.state.colorData[i].start}`}
              line={{
                stroke: `#${this.state.colorData[i].start}`,
                strokeWidth: 2,
                strokeOpacity: this.state.opacity[unit.dataKey]
              }}
              style={{
                filter: `url(#${this.state.units[i].shadowId})`,
                opacity: this.state.opacity[unit.dataKey]
              }}
            />
          ))}
        </ScatterChart>
      </ResponsiveContainer>
    ) : (
      <div className="no-data-view">Not Enough Data</div>
    );
  }
}

type PlantStatusReadingsState = {
  pointsArray: string[][];
  currentPage: number;
};

type PlantStatusReadingsProps = {
  dataPoints: string[];
  selected?: string;
  onClick: (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>,
    point: string
  ) => void;
};

class PlantStatusReadings extends React.Component<
  PlantStatusReadingsProps,
  PlantStatusReadingsState
> {
  nth = 5;
  state: PlantStatusReadingsState = {
    pointsArray: [],
    currentPage: 0
  };

  transformPoints(dataPoints = this.props.dataPoints) {
    const pointsArray: string[][] = [];
    let tempArray: string[] = [];

    dataPoints &&
      dataPoints.length &&
      dataPoints.forEach((point, i) => {
        tempArray.push(point);

        if (i !== 0 && tempArray.length === this.nth) {
          pointsArray.push(tempArray);

          tempArray = [];
        }
      });

    if (tempArray.length) {
      pointsArray.push(tempArray);
    }

    this.setState({
      pointsArray
    });
  }

  componentDidMount() {
    this.transformPoints();
  }

  // Check incoming vs existing data
  // return true is data doesn't match
  checkNewData(currMenu: string[], nextMenu: string[]) {
    if (currMenu.length !== nextMenu.length) {
      // Data doesn't match if number of units don't match
      return true;
    }

    for (let i = 0; i < currMenu.length; i++) {
      if (currMenu[i] !== nextMenu[i]) {
        return true;
      }
    }

    return false;
  }

  componentWillUpdate(nextProps: PlantStatusReadingsProps) {
    if (this.checkNewData(this.props.dataPoints, nextProps.dataPoints)) {
      this.transformPoints(nextProps.dataPoints);

      return true;
    }
  }

  render() {
    return (
      <div className="PlantStatusReadings">
        <button
          className={`page ${
            this.state.currentPage === 0 ? 'disabled' : 'active'
          }`}
          onClick={() =>
            this.setState(prevState => {
              return { currentPage: prevState.currentPage - 1 };
            })
          }
        >
          <KeyboardArrowUpIcon></KeyboardArrowUpIcon>
        </button>

        <div className="dataPointsContainer">
          {this.state.pointsArray.map((points, i) => (
            <div
              key={i}
              className={`page page-${i} ${i === this.state.currentPage &&
                'active'} ${this.state.currentPage > i && 'above'} ${this.state
                .currentPage < i && 'below'}`}
            >
              {points.map((point, j) =>
                point ? (
                  <button
                    key={`${i}-${j}`}
                    id={point}
                    onClick={ev => this.props.onClick(ev, point)}
                    className={this.props.selected === point ? 'active' : ''}
                  >
                    {point}
                  </button>
                ) : null
              )}
            </div>
          ))}
        </div>

        <button
          className={`page ${
            this.state.currentPage !== this.state.pointsArray.length - 1
              ? 'active'
              : 'disabled'
          }`}
          onClick={() =>
            this.setState(prevState => {
              return { currentPage: prevState.currentPage + 1 };
            })
          }
        >
          <KeyboardArrowDownIcon></KeyboardArrowDownIcon>
        </button>
      </div>
    );
  }
}

type PlantStatusGaugesProps = GaugeChartModel;

const PlantStatusGauges = (props: PlantStatusGaugesProps) => {
  function createGaugeGrouping(group: GaugeChartGroup[]) {
    return group.map((item, i) => {
      if (item) {
        const colors = item.colors.split(',');

        return (
          <div key={i} className={`chart chart-${i} chart-${item.kind}`}>
            {item.kind === 'donut' ? (
              <DonutChart
                startColor={colors[0]}
                stopColor={colors[1]}
                {...item}
              />
            ) : (
              <GaugeChart
                startColor={colors[0]}
                stopColor={colors[1]}
                {...item}
              />
            )}
          </div>
        );
      }

      return null;
    });
  }

  return props.leftGaugeGroup || props.rightGaugeGroup ? (
    <div
      className={`PlantStatusGauges ${
        props.rightGaugeGroup ? '' : 'single-unit'
      }`}
    >
      {props.leftUnitName ? (
        <Typography variant="body1" className="PlantStatusTitle">
          {props.leftUnitName}
        </Typography>
      ) : null}

      {props.rightUnitName ? (
        <Typography variant="body1" className="PlantStatusTitle">
          {props.rightUnitName}
        </Typography>
      ) : null}

      {props.leftGaugeGroup && createGaugeGrouping(props.leftGaugeGroup)}

      {props.rightGaugeGroup && createGaugeGrouping(props.rightGaugeGroup)}
    </div>
  ) : (
    <div className="no-data-view">No Data</div>
  );
};

type PlantStatusHeaderProps = {
  plantLocation?: Plant;
  onChange: (event: React.ChangeEvent<{ name?: string; value: Plant }>) => void;
  statusHeader: string;
  message?: JSX.Element | string;
};

const PlantStatusHeader = (props: PlantStatusHeaderProps) => {
  return (
    <Grid container style={{ paddingTop: '16px' }}>
      <Grid container direction="column" item xs={8}>
        <Grid item>
          <Typography variant="h2">PLANT STATUS</Typography>
        </Grid>
        <Grid item>
          <PlantLocationsDropdown
            plantLocation={props.plantLocation}
            onChange={ev => props.onChange(ev)}
          />
        </Grid>
      </Grid>
      <Grid item xs={3}>
        <span className={props.statusHeader}>{props.message}</span>
      </Grid>
    </Grid>
  );
};

type PlantStatusWidgetState = {
  loading: boolean;
  error: boolean;
  intervalTiming: number;
  interval?: number;
  plantLocation?: Plant;
  currentTime: string;
  lastUpdated: string;
  dataPointSelected: string;
  dataPoints: string[];
  plantData: { [name: string]: LineChartData };
  gaugeData: GaugeChartModel;
  period?: string;
  side: string;
};

type PlantStatusWidgetProps = WidgetProps &
  ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatchToProps> & {};

type DataOptions = {
  plantId?: string;
  start?: string;
  end?: string;
  period?: string;
};

class PlantStatusWidget extends React.Component<
  PlantStatusWidgetProps,
  PlantStatusWidgetState
> {
  state: PlantStatusWidgetState = {
    loading: false,
    error: false,
    intervalTiming: 300000,
    currentTime: getCurrentTime(),
    lastUpdated: getCurrentTime(),
    dataPoints: [],
    dataPointSelected: '',
    plantData: {},
    gaugeData: {},
    period: 'BY_HOUR',
    side: 'front'
  };

  refreshEvery(timing = this.state.intervalTiming) {
    window.clearInterval(this.state.interval);

    return window.setInterval(() => this.fetchAllData(), timing);
  }

  errorState() {
    window.clearInterval(this.state.interval);

    this.setState({
      loading: false,
      error: true
    });
  }

  fetchAllData(opts: DataOptions = {}) {
    this.setState({ loading: true });

    return Promise.all([
      this.fetchGaugeData(opts),
      this.fetchGraphData(opts)
    ]).then(
      responses => {
        this.setState({
          interval: this.refreshEvery(),
          currentTime: getCurrentTime(),
          loading: false,
          error: false
        });
      },
      rejections => this.errorState()
    );
  }

  fetchGaugeData(opts: DataOptions = {}) {
    const defaults: DataOptions = {
      plantId: this.state.plantLocation && this.state.plantLocation.id
    };

    opts = {
      ...defaults,
      ...opts
    };

    if (opts.plantId) {
      this.setState({
        loading: true,
        gaugeData: {}
      });

      return fetchWrapper<
        GraphResponse<{ gaugeChartQuery: { data: GaugeChartModel } }>
      >('hld/graphql/', {
        method: 'POST',
        data: {
          query: `query {
          gaugeChartQuery(
            plant_ID:"${opts.plantId}"
          ){
            data
          } }`
        }
      })
        .then(json => {
          if (
            json.data.gaugeChartQuery &&
            'data' in json.data.gaugeChartQuery
          ) {
            this.setState({
              gaugeData: json.data.gaugeChartQuery.data || {},
              lastUpdated:
                (json.data.gaugeChartQuery.data.lastUpdated &&
                  parseTime(json.data.gaugeChartQuery.data.lastUpdated, {
                    timeZoneName: 'short'
                  })) ||
                getCurrentTime()
            });
          }
        })
        .catch(err => {
          this.errorState();
        });
    }

    return Promise.reject();
  }

  fetchGraphData(opts: DataOptions = {}) {
    const defaults = {
      plantId: this.state.plantLocation && this.state.plantLocation.id,
      start: new Date(new Date().getTime() - 24 * ONE_HOUR).toISOString(),
      end: new Date().toISOString(),
      period: this.state.period
    };

    opts = {
      ...defaults,
      ...opts
    };

    if (opts.plantId) {
      this.setState({ loading: true });

      return fetchWrapper<GraphResponse<{ lineChartQuery: LineChart }>>(
        'hld/graphql/',
        {
          method: 'POST',
          data: {
            query: `query {
          lineChartQuery(
            plant_ID:"${opts.plantId}",
            start:"${opts.start}",
            end:"${opts.end}",
            period:${opts.period}){
              menu
              data
            }
          }`
          }
        }
      )
        .then(res => {
          const json = res.data.lineChartQuery;

          this.setState({
            dataPoints: json && 'menu' in json ? json.menu : [],
            plantData: json.data,
            dataPointSelected:
              json.menu.length &&
              this.state.dataPointSelected &&
              json.menu.indexOf(this.state.dataPointSelected) !== -1
                ? this.state.dataPointSelected
                : json.menu.length
                ? json.menu[0]
                : '',
            period: opts.period
          });
        })
        .catch(err => {
          this.errorState();
        });
    }

    return Promise.reject();
  }

  dataPointSelect(
    ev: React.MouseEvent<HTMLButtonElement, MouseEvent>,
    dataPoint: string
  ) {
    if (dataPoint) {
      this.setState({
        dataPointSelected: dataPoint
      });
    }
  }

  handleChange(
    ev: React.ChangeEvent<{ name?: string | undefined; value: Plant }>
  ) {
    if (ev.target.value) {
      const dashboardList = this.props.widgetList;
      if (!dashboardList) return;
      let index = -1;

      for (let i = 0; i < dashboardList.length; i++) {
        const widget = dashboardList[i];

        if (widget.instanceId === this.props.widgetId) {
          index = i;

          break;
        }
      }

      let configParams = dashboardList[index].widgetInstanceConfigparms;
      if (index !== -1 && configParams) {
        let i = -1;
        configParams.length &&
          configParams.forEach((param, ind) => {
            if (param.key === 'PlantId') {
              i = ind;

              return;
            }
          });

        if (i > -1) {
          configParams[i].value = ev.target.value.id;
        } else {
          configParams.push({
            key: 'PlantId',
            value: ev.target.value.id
          });
        }

        this.props.updateDashboard({
          userId: this.props.userId,
          dashboardId: this.props.dashboardId,
          dashboardOrder: this.props.widgetList
        });
      } else {
        console.error(
          'Error saving plant location in Plant Status',
          this.props.widgetId
        );
      }

      this.setState({
        plantLocation: ev.target.value
      });
    }
  }

  changeViewPeriod(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
    this.setState({ loading: true });

    const activeItems = document.querySelectorAll(
      '.PlantStatusChart .statusViewControl button.active'
    );

    activeItems &&
      activeItems.length &&
      activeItems.forEach(
        $el => ($el.className = $el.className.replace(/active/gi, ''))
      );
    (e.target as HTMLElement).className += ' active';

    let start: string;
    let end: string;

    switch ((e.target as HTMLElement).id) {
      case 'BY_DAY':
        start = new Date(
          new Date().getTime() - 7 * 24 * ONE_HOUR
        ).toISOString();
        end = new Date().toISOString();
        break;
      case 'BY_HOUR':
        start = new Date(new Date().getTime() - 24 * ONE_HOUR).toISOString();
        end = new Date().toISOString();
        break;
      case 'BY_MINUTE':
      default:
        start = new Date(new Date().getTime() - 2 * ONE_HOUR).toISOString();
        end = new Date().toISOString();
        break;
    }

    this.fetchGraphData({
      start,
      end,
      period: (e.target as HTMLElement).id
    }).then(() => {
      this.refreshEvery();

      this.setState({ loading: false });
    });
  }

  statusMessage = () => {
    return (
      <>
        Showing status for <strong>{this.state.dataPointSelected}</strong> as of{' '}
        {this.state.lastUpdated || this.state.currentTime}
      </>
    );
  };

  getDefaultPlant(locations = this.props.plantLocations) {
    let loc;
    if (this.props.config && this.props.config.PlantId) {
      const match = new RegExp(this.props.config.PlantId, 'i');
      const locs = locations.filter(
        location =>
          location.id === (this.props.config && this.props.config.PlantId) ||
          match.test(location.name)
      );

      if (locs.length) {
        loc = locs[0];
      } else {
        loc = locations[0];
      }
    }

    this.setState({
      plantLocation: loc || {
        id: '1c664974-7096-11e9-a923-1681be663d3e',
        name: 'CALVERT CLIFFS NUCLEAR POWER PLANT'
      }
    });
  }

  componentDidMount() {
    if (this.props.plantLocations.length) {
      this.getDefaultPlant();

      this.fetchAllData();
    }
  }

  componentWillUnmount() {
    window.clearInterval(this.state.interval);
  }

  componentWillUpdate(
    nextProps: PlantStatusWidgetProps,
    nextState: PlantStatusWidgetState
  ) {
    if (this.state.dataPointSelected !== nextState.dataPointSelected) {
      return true;
    }

    if (
      (this.state.plantLocation && this.state.plantLocation.id) !==
      (nextState.plantLocation && nextState.plantLocation.id)
    ) {
      this.fetchAllData({
        plantId: nextState.plantLocation && nextState.plantLocation.id
      });

      return true;
    }

    if (this.props.plantLocations.length !== nextProps.plantLocations.length) {
      this.getDefaultPlant();

      this.fetchAllData();
    }
  }

  render() {
    return Object.keys(this.state.plantData).length ? (
      <FlippableWidget
        widgetId={this.props.widgetId}
        widgetSize={
          (this.props.config && this.props.config.widgetSize) || 'extra-large'
        }
        order={this.props.order}
        hideOptions={this.props.hideOptions}
        startSide={this.state.side}
        afterFlip={() =>
          this.setState({
            side: this.state.side === 'front' ? 'back' : 'front'
          })
        }
        front={
          <div className="flip-widget">
            <PlantStatusGauges {...this.state.gaugeData} />
          </div>
        }
        frontHeader={
          <PlantStatusHeader
            plantLocation={this.state.plantLocation}
            message={`Last updated ${this.state.lastUpdated ||
              this.state.currentTime}`}
            onChange={values => this.handleChange(values)}
            statusHeader={'front'}
          />
        }
        back={
          <>
            <div className="plant-chart-container">
              <PlantStatusReadings
                onClick={(ev, v) => this.dataPointSelect(ev, v)}
                dataPoints={this.state.dataPoints}
                selected={this.state.dataPointSelected}
              />

              <div className="PlantStatusChart">
                <div className="statusViewControl">
                  <button
                    id="BY_DAY"
                    onClick={e => this.changeViewPeriod(e)}
                    className={this.state.period === 'BY_DAY' ? 'active' : ''}
                  >
                    1w
                  </button>

                  <button
                    id="BY_HOUR"
                    onClick={e => this.changeViewPeriod(e)}
                    className={this.state.period === 'BY_HOUR' ? 'active' : ''}
                  >
                    1d
                  </button>

                  <button
                    id="BY_MINUTE"
                    onClick={e => this.changeViewPeriod(e)}
                    className={
                      this.state.period === 'BY_MINUTE' ? 'active' : ''
                    }
                  >
                    2h
                  </button>
                </div>

                <ComparisonLineChart
                  timeline={this.state.plantData[this.state.dataPointSelected]}
                  selected={this.state.dataPointSelected}
                  period={this.state.period}
                />
              </div>
            </div>

            <span className="flipButton flip-widget">
              <ArrowBackIcon></ArrowBackIcon>
            </span>
          </>
        }
        backHeader={
          <PlantStatusHeader
            plantLocation={this.state.plantLocation}
            message={this.statusMessage()}
            onChange={values => this.handleChange(values)}
            statusHeader={'rear'}
          />
        }
      >
        {this.state.loading ? (
          <LoadingNotifier classes="content-loading" />
        ) : null}
      </FlippableWidget>
    ) : this.state.loading ? (
      <Widget widgetId={Math.random().toString()} hideOptions={true}>
        <PlantStatusHeader
          plantLocation={this.state.plantLocation}
          message={`Last updated ${this.state.lastUpdated ||
            this.state.currentTime}`}
          onChange={values => this.handleChange(values)}
          statusHeader={'front'}
        />

        <LoadingNotifier />
      </Widget>
    ) : this.state.error ? (
      <Widget widgetId={Math.random().toString()} hideOptions={true}>
        <PlantStatusHeader
          plantLocation={this.state.plantLocation}
          message={`Last updated ${(this.state.gaugeData &&
            this.state.gaugeData.lastUpdated) ||
            this.state.currentTime}`}
          onChange={values => this.handleChange(values)}
          statusHeader={'front'}
        />

        <Error
          display={true}
          icon={<WarningIcon style={{ fontSize: '10rem' }} />}
          message="We are currently unable to display your data."
          onClick={() => this.fetchAllData()}
        />
      </Widget>
    ) : null;
  }
}

function mapStateToProps(state: StoreState) {
  return {
    userId: state.dashboard.userId,
    dashboardId: state.dashboard.dashboardId,
    widgetList: state.dashboard.dashboardWidgets,
    plantLocations: state.app.plantLocations
  };
}

function mapDispatchToProps(
  dispatch: ThunkDispatch<StoreState, undefined, Action>
) {
  return {
    updateDashboard: (dashboard: UserDashboard) => updateDashboard(dashboard)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlantStatusWidget);
