import * as React from 'react';
import Icon, { IconProps } from '@material-ui/core/Icon';

//@ts-ignore
import app_launcher_icon from '../../assets/app_launcher_icon.svg';

export default function QuickLinksIcon(props: IconProps) {
  return (
    <Icon {...props} style={{ textAlign: 'center' }}>
      <img
        src={app_launcher_icon}
        alt="quick links icon"
        width="18"
        height="18"
      />
    </Icon>
  );
}
