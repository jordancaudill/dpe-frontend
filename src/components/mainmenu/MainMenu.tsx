import * as React from 'react';
import { Popover, List, ListItem, ListItemIcon } from '@material-ui/core';

import { getQuickLinks } from '../../requests/quickLinkRequests';
import QuickLinksPopover from './QuickLinksPopover';
import DashboardIcon from './DashboardIcon';
import QuickLinksIcon from './QuickLinksIcon';

export default function MainMenu(props: { style?: React.CSSProperties }) {
  React.useEffect(() => getQuickLinks(), []);

  const [quickLinksEl, setQuickLinksEl] = React.useState<HTMLElement | null>(
    null
  );
  const open = Boolean(quickLinksEl);

  const toggleQuickLinks = (
    event: React.MouseEvent<HTMLSpanElement, MouseEvent>
  ) => {
    setQuickLinksEl(event.currentTarget);
  };

  return (
    <>
      <List
        style={{
          ...props.style,
          ...styles.list
        }}
      >
        <ListItem button component="a" href="/">
          <ListItemIcon style={styles.icon}>
            <DashboardIcon />
          </ListItemIcon>
        </ListItem>
        <ListItem button>
          <ListItemIcon style={styles.icon}>
            <QuickLinksIcon onClick={toggleQuickLinks} />
          </ListItemIcon>
        </ListItem>
      </List>

      <Popover
        open={open}
        anchorEl={quickLinksEl}
        onClose={() => setQuickLinksEl(null)}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left'
        }}
      >
        <QuickLinksPopover style={styles.popover}></QuickLinksPopover>
      </Popover>
    </>
  );
}

const styles = {
  list: {
    top: '120px',
    width: '61px',
    borderRight: '1px solid rgba(0, 0, 0, 0.12)',
    position: 'absolute' as 'absolute',
    height: '100%'
  },
  popover: {
    width: '200px',
    border: '1px solid rgba(0, 0, 0, 0.12)',
    backgroundColor: 'inherit'
  },
  icon: {
    minWidth: '18px'
  }
};
