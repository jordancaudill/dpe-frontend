import * as React from 'react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { mount } from 'enzyme';
import { ListItem } from '@material-ui/core';
import LaunchIcon from '@material-ui/icons/Launch';

import QuickLinksPopover from '../QuickLinksPopover';
import GenericIcon from '../../shared/GenericIcon';

let store: any;

describe('QuickLinksPopover', () => {
  const initialState = {
    app: {
      quickLinks: {
        testA: {
          title: 'test A',
          link: 'https://example.com/test/a'
        },
        testB: {
          title: 'test B',
          icon: 'https://example.com/test/b.jpg',
          link: 'https://example.com/test/b'
        },
        testC: {
          title: 'test C',
          icon: 'https://example.com/test/c.gif',
          link: 'https://example.com/test/c'
        }
      }
    }
  };

  beforeEach(() => {
    const mockStore = configureStore();
    store = mockStore(initialState);
  });

  it('should render properly', () => {
    const wrapper = mount(
      <Provider store={store}>
        <QuickLinksPopover />
      </Provider>
    );
    expect(wrapper).toBeDefined();
    expect(wrapper.find(ListItem)).toHaveLength(3);
    expect(wrapper.find(GenericIcon)).toHaveLength(2);
    expect(wrapper.find(LaunchIcon)).toHaveLength(1);
  });
});
