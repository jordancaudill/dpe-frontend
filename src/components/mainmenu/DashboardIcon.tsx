import * as React from 'react';
import Icon from '@material-ui/core/Icon';

//@ts-ignore
import dashboard_icon from '../../assets/dashboard_icon.svg';

export class DashboardIcon extends React.Component {
  render() {
    return (
      <Icon style={{ textAlign: 'center' }}>
        <img src={dashboard_icon} alt="dashboard icon" width="18" height="18" />
      </Icon>
    );
  }
}

export default DashboardIcon;
