import * as React from 'react';
import { useSelector } from 'react-redux';
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  ListSubheader
} from '@material-ui/core';

import { StoreState } from '../../store';
import ExternalIcon from '../shared/ExternalIcon';

export default function QuickLinksPopover(props: {
  style?: React.CSSProperties;
}) {
  const quickLinks = useSelector((state: StoreState) => state.app.quickLinks);

  return (
    <List style={props.style}>
      {Object.keys(quickLinks).map((quickLink, i) => (
        <React.Fragment key={`quickLink${quickLinks[quickLink].title}`}>
          {i === 0 ? (
            <ListSubheader style={styles.subHeader}>
              My Saved Shortcuts
            </ListSubheader>
          ) : null}
          {i === 4 ? (
            <ListSubheader style={styles.subHeader}>
              Frequently Visited
            </ListSubheader>
          ) : null}
          <ListItem
            button
            component="a"
            href={quickLinks[quickLink].link}
            target="_blank"
            rel="noopener noreferrer"
          >
            <ListItemIcon style={styles.icon}>
              <ExternalIcon {...quickLinks[quickLink]} />
            </ListItemIcon>
            <ListItemText primary={quickLinks[quickLink].title} />
          </ListItem>
        </React.Fragment>
      ))}
    </List>
  );
}

const styles = {
  icon: {
    minWidth: '18px',
    marginRight: '1rem'
  },
  subHeader: {
    textAlign: 'center' as 'center',
    textTransform: 'uppercase' as 'uppercase'
  }
};
