import * as React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { initialState } from '../../store';

import NewWidgetForm from '../addwidgets/NewWidgetForm';

describe('NewWidgetForm', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render NewWidgetForm properly', () => {
    const mockedSubmitFunction = jest.fn();

    const mockStore = configureStore();
    const store = mockStore(initialState);

    const wrapper = shallow(
      <Provider store={store}>
        <NewWidgetForm
          handleClick={mockedSubmitFunction}
          userName={'Fake name'}
        />
      </Provider>
    );
    expect(wrapper.find(NewWidgetForm)).toBeDefined();
  });
});
