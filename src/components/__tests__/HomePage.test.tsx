import * as React from 'react';
import { Store, AnyAction } from 'redux';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';

import HomePage from '../HomePage';
import MainPage from '../MainPage';
import LoginPage from '../LoginPage';

const mockStore = configureStore();

function shallowWithStore(
  Component: React.ComponentType<any>,
  store: Store<any, AnyAction>
) {
  return shallow(<Component store={store} />)
    .dive()
    .shallow();
}

describe('HomePage', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render successfully', () => {
    const store = mockStore({ oidc: {}, app: { user: {} } });
    const wrapper = shallowWithStore(HomePage, store);
    expect(wrapper).toBeDefined();
  });

  it('should render main successfully', () => {
    const store = mockStore({
      oidc: { isLoadingUser: false, isAuthenticated: true, user: {} },
      app: { user: {} }
    });
    const wrapper = shallowWithStore(HomePage, store);
    expect(wrapper.find(LoginPage)).toHaveLength(0);
    expect(wrapper.find(MainPage)).toHaveLength(1);
  });

  it('should render login when no store user', () => {
    const store = mockStore({
      oidc: { isLoadingUser: false, isAuthenticated: true, user: {} },
      app: { user: null }
    });
    const wrapper = shallowWithStore(HomePage, store);
    expect(wrapper.find(LoginPage)).toHaveLength(1);
    expect(wrapper.find(MainPage)).toHaveLength(0);
  });

  it('should render main when store user added', () => {
    const store = mockStore({
      oidc: { isLoadingUser: false, isAuthenticated: true, user: {} },
      app: { user: null }
    });
    const wrapper = shallowWithStore(HomePage, store);
    wrapper
      .setProps({ user: {} } as any)
      .instance()
      .forceUpdate();
    expect(wrapper.find(LoginPage)).toHaveLength(0);
    expect(wrapper.find(MainPage)).toHaveLength(1);
  });

  it('should render login when oidc user expired', () => {
    const store = mockStore({
      oidc: {
        isLoadingUser: false,
        isAuthenticated: true,
        user: { expired: true }
      },
      app: { user: {} }
    });
    const wrapper = shallowWithStore(HomePage, store);
    expect(wrapper.find(LoginPage)).toHaveLength(1);
    expect(wrapper.find(MainPage)).toHaveLength(0);
  });

  it('should render login when oidc user no longer expired', () => {
    const store = mockStore({
      oidc: {
        isLoadingUser: false,
        isAuthenticated: true,
        user: { expired: true }
      },
      app: { user: {} }
    });
    const wrapper = shallowWithStore(HomePage, store);
    wrapper
      .setProps({
        oidc: {
          isLoadingUser: false,
          isAuthenticated: true,
          user: { expired: false }
        }
      } as any)
      .instance()
      .forceUpdate();
    expect(wrapper.find(LoginPage)).toHaveLength(0);
    expect(wrapper.find(MainPage)).toHaveLength(1);
  });

  it('should render login when oidc user is loading', () => {
    const store = mockStore({
      oidc: { isLoadingUser: true, isAuthenticated: true, user: {} },
      app: { user: {} }
    });
    const wrapper = shallowWithStore(HomePage, store);
    expect(wrapper.find(LoginPage)).toHaveLength(1);
    expect(wrapper.find(MainPage)).toHaveLength(0);
  });

  it('should render login when oidc user is no longer loading', () => {
    const store = mockStore({
      oidc: { isLoadingUser: true, isAuthenticated: true, user: {} },
      app: { user: {} }
    });
    const wrapper = shallowWithStore(HomePage, store);
    wrapper
      .setProps({
        oidc: { isLoadingUser: false, isAuthenticated: true, user: {} }
      } as any)
      .instance()
      .forceUpdate();
    expect(wrapper.find(LoginPage)).toHaveLength(0);
    expect(wrapper.find(MainPage)).toHaveLength(1);
  });

  it('should render login when oidc user is not authenticated', () => {
    const store = mockStore({
      oidc: { isLoadingUser: false, isAuthenticated: false, user: {} },
      app: { user: {} }
    });
    const wrapper = shallowWithStore(HomePage, store);
    expect(wrapper.find(LoginPage)).toHaveLength(1);
    expect(wrapper.find(MainPage)).toHaveLength(0);
  });

  it('should render login when oidc user becomes authenticated', () => {
    const store = mockStore({
      oidc: { isLoadingUser: false, isAuthenticated: false, user: {} },
      app: { user: {} }
    });
    const wrapper = shallowWithStore(HomePage, store);
    wrapper
      .setProps({
        oidc: { isLoadingUser: false, isAuthenticated: true, user: {} }
      } as any)
      .instance()
      .forceUpdate();
    expect(wrapper.find(LoginPage)).toHaveLength(0);
    expect(wrapper.find(MainPage)).toHaveLength(1);
  });

  it('should render login when oidc user is null', () => {
    const store = mockStore({
      oidc: { isLoadingUser: false, isAuthenticated: true, user: null },
      app: { user: {} }
    });
    const wrapper = shallowWithStore(HomePage, store);
    expect(wrapper.find(LoginPage)).toHaveLength(1);
    expect(wrapper.find(MainPage)).toHaveLength(0);
  });

  it('should render login when oidc user is no longer null', () => {
    const store = mockStore({
      oidc: { isLoadingUser: false, isAuthenticated: true, user: null },
      app: { user: {} }
    });
    const wrapper = shallowWithStore(HomePage, store);
    wrapper
      .setProps({
        oidc: { isLoadingUser: false, isAuthenticated: true, user: {} }
      } as any)
      .instance()
      .forceUpdate();
    expect(wrapper.find(LoginPage)).toHaveLength(0);
    expect(wrapper.find(MainPage)).toHaveLength(1);
  });

  it('should render login when oidc user is undefined', () => {
    const store = mockStore({
      oidc: { isLoadingUser: false, isAuthenticated: true },
      app: { user: {} }
    });
    const wrapper = shallowWithStore(HomePage, store);
    expect(wrapper.find(LoginPage)).toHaveLength(1);
    expect(wrapper.find(MainPage)).toHaveLength(0);
  });

  it('should render login when oidc user is no longer undefined', () => {
    const store = mockStore({
      oidc: { isLoadingUser: false, isAuthenticated: true },
      app: { user: {} }
    });
    const wrapper = shallowWithStore(HomePage, store);
    wrapper
      .setProps({
        oidc: { isLoadingUser: false, isAuthenticated: true, user: {} }
      } as any)
      .instance()
      .forceUpdate();
    expect(wrapper.find(LoginPage)).toHaveLength(0);
    expect(wrapper.find(MainPage)).toHaveLength(1);
  });
});
