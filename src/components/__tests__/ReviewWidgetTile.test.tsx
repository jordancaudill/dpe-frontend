import * as React from 'react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { mount } from 'enzyme';
import { Card, CardContent, Typography } from '@material-ui/core';

import { initialState } from '../../store';
import { generateUUID } from '../../utils';

import ReviewWidgetTile from '../ReviewWidgetTile';
import GenericButton from '../shared/GenericButton';

import { updateWidget } from '../../requests/widgetRequests';
import { getAvailableWidgets } from '../../requests/getAvailableWidgets';
import { getPendingWidgets } from '../../requests/getPendingWidgets';

const updateWidgetPromise = Promise.resolve();
jest.mock('../../requests/widgetRequests', () => ({
  updateWidget: jest.fn(() => updateWidgetPromise)
}));
jest.mock('../../requests/getAvailableWidgets');
jest.mock('../../requests/getPendingWidgets');

describe('ReviewWidgetTile', () => {
  const widget = {
    title: 'Test1',
    subtitle: 'test description',
    image: 'https://example.com/test-1.png',
    widgetUrl: 'https://example.com/test-1.png',
    modelId: 'test-1',
    instanceId: generateUUID(),
    order: 0,
    type: 'powerbi',
    widgetConfigparms: [{ key: 'widgetSize', value: 'small' }]
  };
  const mockStore = configureStore();
  const store = mockStore(initialState);

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render ReviewWidgetTile properly', () => {
    const wrapper = mount(
      <Provider store={store}>
        <ReviewWidgetTile {...widget} />
      </Provider>
    );

    expect(wrapper.find(ReviewWidgetTile)).toBeDefined();
    expect(wrapper.find(Card)).toHaveLength(1);
    expect(wrapper.find(CardContent)).toHaveLength(1);
    expect(
      wrapper.contains(<Typography variant="h2">Test1</Typography>)
    ).toEqual(true);
    expect(
      wrapper.contains(
        <Typography variant="body2">test description</Typography>
      )
    ).toEqual(true);
    expect(wrapper.find(GenericButton)).toHaveLength(2);
  });

  it('should approve widgets properly', async () => {
    const wrapper = mount(
      <Provider store={store}>
        <ReviewWidgetTile {...widget} />
      </Provider>
    );

    const approveButton = wrapper
      .findWhere(o => o.text() === 'PUBLISH WIDGET')
      .find(GenericButton);
    expect(approveButton).toBeDefined();

    approveButton.simulate('click');
    await updateWidgetPromise;
    expect(updateWidget).toHaveBeenCalledTimes(1);
    expect(getAvailableWidgets).toHaveBeenCalledTimes(1);
    expect(getPendingWidgets).toHaveBeenCalledTimes(1);
  });

  it('should decline widgets properly', async () => {
    const wrapper = mount(
      <Provider store={store}>
        <ReviewWidgetTile {...widget} />
      </Provider>
    );

    const declineButton = wrapper
      .findWhere(o => o.text() === 'Decline')
      .find(GenericButton);
    expect(declineButton).toBeDefined();

    declineButton.simulate('click');
    await updateWidgetPromise;
    expect(updateWidget).toHaveBeenCalledTimes(1);
    expect(getAvailableWidgets).toHaveBeenCalledTimes(0);
    expect(getPendingWidgets).toHaveBeenCalledTimes(1);
  });
});
