import * as React from 'react';
import { shallow } from 'enzyme';

import FormSubmissionMessage from '../addwidgets/FormSubmissionMessage';

describe('FormSubmissionMessage', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render FormSubmissionMessage properly', () => {
    const mockedSubmitFunction = jest.fn();
    const wrapper = shallow(
      <FormSubmissionMessage handleClick={mockedSubmitFunction} />
    );
    expect(wrapper.find(FormSubmissionMessage)).toBeDefined();
  });
});
