import * as React from 'react';
import configureStore from 'redux-mock-store';

import { initialState } from '../../store';
import AddWidgetsContainer from '../addwidgets/AddWidgetsContainer';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import AddWidgetsHeader from '../addwidgets/AddWidgetsHeader';
import AddWidgetList from '../addwidgets/AddWidgetList';

describe('AddWidgetsContainer', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render addwidgetcontainer properly', () => {
    const AddWidgetsContainerProps = {
      drawerHandle: jest.fn()
    };

    const mockStore = configureStore();
    const store = mockStore(initialState);

    const wrapper = mount(
      <Provider store={store}>
         
        <AddWidgetsContainer {...AddWidgetsContainerProps} />
      </Provider>
    );
    expect(wrapper.find(AddWidgetsContainer)).toBeDefined();
    expect(wrapper.find(AddWidgetsHeader)).toHaveLength(1);
    expect(wrapper.find(AddWidgetList)).toHaveLength(1);
  });
});
