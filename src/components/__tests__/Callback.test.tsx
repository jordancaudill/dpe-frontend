import * as React from 'react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { mount } from 'enzyme';

import { initialState } from '../../store';
import Callback from '../Callback';

let store: any;

describe('Callback', () => {
  beforeEach(() => {
    const mockStore = configureStore();
    store = mockStore(initialState);
  });

  it('should render properly', () => {
    const wrapper = mount(
      <Provider store={store}>
        <Callback />
      </Provider>
    );
    expect(wrapper).toBeDefined();
  });
});
