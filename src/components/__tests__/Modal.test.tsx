import * as React from 'react';
import { shallow } from 'enzyme';

import Modal from '../Modal';
import BGOverlay from '../BGOverlay';

describe('Modal', () => {
  it('should render properly', () => {
    const wrapper = shallow(
      <Modal content={<div>test</div>} onClick={() => {}} />
    );
    expect(wrapper).toBeDefined();
    expect(wrapper.find(BGOverlay)).toHaveLength(1);
  });
});

describe('Modal', () => {
  it('should hide properly', () => {
    const wrapper = shallow(<Modal content={null} onClick={() => {}} />);
    expect(wrapper).toBeDefined();
    expect(wrapper.find('div')).toHaveLength(0);
  });
});
