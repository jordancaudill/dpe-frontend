import * as React from 'react';
import { shallow } from 'enzyme';
import { mocked } from 'ts-jest/utils';
import configureStore from 'redux-mock-store';

import LoginPage from '../LoginPage';
import LoadingNotifier from '../LoadingNotifier';

import userManager from '../../userManager';
import { getUser } from '../../requests/userRequests';
import { Store, AnyAction } from 'redux';

jest.mock('../../userManager', () => ({
  getUser: jest.fn(() => Promise.resolve({ profile: {} })),
  signinRedirect: jest.fn()
}));
jest.mock('../../requests/userRequests');

const mockStore = configureStore();

function shallowWithStore(
  Component: React.ComponentType<any>,
  store: Store<any, AnyAction>
) {
  return shallow(<Component store={store} />)
    .dive()
    .shallow();
}

describe('LoginPage', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render successfully', () => {
    const store = mockStore({ oidc: { isAuthenticated: true } });
    const wrapper = shallowWithStore(LoginPage, store);
    expect(wrapper).toBeDefined();
    expect(wrapper.find(LoadingNotifier)).toHaveLength(1);
  });

  it('should try auth if not authenticated', () => {
    const store = mockStore({ oidc: { isAuthenticated: false } });
    shallowWithStore(LoginPage, store);

    expect(getUser).toHaveBeenCalledTimes(0);
    expect(userManager.getUser).toHaveBeenCalledTimes(1);
  });

  it('should not try auth if authenticated', () => {
    const store = mockStore({ oidc: { isAuthenticated: true } });
    shallowWithStore(LoginPage, store);

    expect(getUser).toHaveBeenCalledTimes(1);
    expect(userManager.getUser).toHaveBeenCalledTimes(0);
  });

  it('should try auth if no user saved', async () => {
    const mockUser = Promise.resolve(null);
    mocked(userManager.getUser).mockImplementation(() => mockUser);

    const store = mockStore({ oidc: { isAuthenticated: false } });
    shallowWithStore(LoginPage, store);
    await mockUser;

    expect(getUser).toHaveBeenCalledTimes(0);
    expect(userManager.signinRedirect).toHaveBeenCalledTimes(1);
  });

  it('should try auth if expired', async () => {
    const mockUser = Promise.resolve({ expired: true, profile: {} } as any);
    mocked(userManager.getUser).mockImplementation(() => mockUser);

    const store = mockStore({ oidc: { isAuthenticated: false } });
    shallowWithStore(LoginPage, store);
    await mockUser;

    expect(getUser).toHaveBeenCalledTimes(0);
    expect(userManager.signinRedirect).toHaveBeenCalledTimes(1);
  });

  it('should not try auth if user saved', async () => {
    const mockUser = Promise.resolve({ expired: false, profile: {} } as any);
    mocked(userManager.getUser).mockImplementation(() => mockUser);

    const store = mockStore({ oidc: { isAuthenticated: false } });
    shallowWithStore(LoginPage, store);
    await mockUser;

    expect(getUser).toHaveBeenCalledTimes(1);
    expect(userManager.signinRedirect).toHaveBeenCalledTimes(0);
  });
});
