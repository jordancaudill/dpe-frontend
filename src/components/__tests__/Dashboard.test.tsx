import * as React from 'react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { mount } from 'enzyme';

import { initialState } from '../../store';
import Dashboard from '../Dashboard';
import WidgetGrid from '../WidgetGrid';
import LoadingNotifier from '../LoadingNotifier';

import * as actions from '../../actions/dashboard/actions';

const testAction = {
  type: 'TEST_ACTION',
  payload: {}
};

jest.mock('../../actions/dashboard/actions', () => ({
  fetchWidgets: jest.fn(() => testAction),
  setGridLayout: jest.fn(() => testAction)
}));

describe('Dashboard', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render grid properly', () => {
    const mockStore = configureStore();
    const store = mockStore(initialState);
    const wrapper = mount(
      <Provider store={store}>
        <Dashboard />
      </Provider>
    );
    expect(wrapper).toBeDefined();
    expect(actions.fetchWidgets).toBeCalledTimes(1);
    expect(wrapper.find(LoadingNotifier)).toHaveLength(0);
    expect(wrapper.find(WidgetGrid)).toHaveLength(1);
  });

  it('should render spinner properly', () => {
    const mockStore = configureStore();
    const store = mockStore({
      ...initialState,
      dashboard: {
        ...initialState.dashboard,
        dashboardLoading: true
      }
    });
    const wrapper = mount(
      <Provider store={store}>
        <Dashboard />
      </Provider>
    );
    expect(wrapper).toBeDefined();
    expect(actions.fetchWidgets).toBeCalledTimes(1);
    expect(wrapper.find(LoadingNotifier)).toHaveLength(1);
    expect(wrapper.find(WidgetGrid)).toHaveLength(0);
  });
});
