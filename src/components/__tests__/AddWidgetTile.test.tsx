import * as React from 'react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { mount } from 'enzyme';
import { Card, CardContent, Typography } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';

import { initialState } from '../../store';
import { generateUUID } from '../../utils';
import { updateWidget } from '../../requests/widgetRequests';
import { getAvailableWidgets } from '../../requests/getAvailableWidgets';

import AddWidgetTile from '../AddWidgetTile';
import GenericButton from '../shared/GenericButton';
import Modal from '../Modal';

const updateWidgetPromise = Promise.resolve();
jest.mock('../../requests/widgetRequests', () => ({
  updateWidget: jest.fn(() => updateWidgetPromise)
}));
jest.mock('../../requests/getAvailableWidgets');

describe('AddWidgetTile', () => {
  const widget = {
    title: 'Test1',
    subtitle: 'test description',
    image: 'https://example.com/test-1.png',
    widgetUrl: 'https://example.com/test-1.png',
    modelId: 'test-1',
    instanceId: generateUUID(),
    order: 0,
    type: 'powerbi',
    widgetConfigparms: [{ key: 'widgetSize', value: 'small' }]
  };

  const mockStore = configureStore();

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render properly', () => {
    const store = mockStore(initialState);

    const wrapper = mount(
      <Provider store={store}>
        <AddWidgetTile {...widget} />
      </Provider>
    );
    expect(wrapper.find(AddWidgetTile)).toBeDefined();
    expect(wrapper.find(Card)).toHaveLength(1);
    expect(wrapper.find(CardContent)).toHaveLength(1);
    expect(
      wrapper.contains(<Typography variant="h2">Test1</Typography>)
    ).toEqual(true);
    expect(
      wrapper.contains(
        <Typography variant="body2">test description</Typography>
      )
    ).toEqual(true);
    expect(wrapper.find(GenericButton)).toHaveLength(1);
    expect(wrapper.find(GenericButton).text()).toEqual('Add to Dash');
  });

  it('should not render edit button for non-admin', () => {
    const store = mockStore({
      app: {
        user: {
          isAdmin: false
        }
      },
      dashboard: {}
    });

    const wrapper = mount(
      <Provider store={store}>
        <AddWidgetTile {...widget} />
      </Provider>
    );
    expect(wrapper.find(EditIcon)).toHaveLength(0);
  });

  it('should render edit button for admin', () => {
    const store = mockStore({
      app: {
        user: {
          isAdmin: true
        }
      },
      dashboard: {}
    });

    const wrapper = mount(
      <Provider store={store}>
        <AddWidgetTile {...widget} />
      </Provider>
    );
    expect(wrapper.find(EditIcon)).toHaveLength(1);
  });

  it('should try delete when clicked', async () => {
    const store = mockStore({
      app: {
        user: {
          isAdmin: true
        }
      },
      dashboard: {}
    });

    const wrapper = mount(
      <Provider store={store}>
        <AddWidgetTile {...widget} />
      </Provider>
    );
    const editButton = wrapper.find(EditIcon);
    editButton.simulate('click');
    wrapper.update();

    const deleteButton = wrapper.find(GenericButton);
    deleteButton.simulate('click');
    wrapper.update();

    const confirmButton = wrapper
      .find(Modal)
      .findWhere(o => o.text() === 'DELETE WIDGET')
      .find('button');
    confirmButton.simulate('click');

    await updateWidgetPromise;
    expect(updateWidget).toHaveBeenCalledTimes(1);
    expect(getAvailableWidgets).toHaveBeenCalledTimes(1);
  });

  it('should not try delete when cancelled', async () => {
    const store = mockStore({
      app: {
        user: {
          isAdmin: true
        }
      },
      dashboard: {}
    });

    const wrapper = mount(
      <Provider store={store}>
        <AddWidgetTile {...widget} />
      </Provider>
    );
    const editButton = wrapper.find(EditIcon);
    editButton.simulate('click');
    wrapper.update();

    const deleteButton = wrapper.find(GenericButton);
    deleteButton.simulate('click');
    wrapper.update();

    const confirmButton = wrapper
      .find(Modal)
      .findWhere(o => o.text() === 'Cancel')
      .find('button');
    confirmButton.simulate('click');

    await updateWidgetPromise;
    expect(updateWidget).toHaveBeenCalledTimes(0);
    expect(getAvailableWidgets).toHaveBeenCalledTimes(0);
  });
});
