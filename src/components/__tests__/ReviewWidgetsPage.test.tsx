import * as React from 'react';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';

import { initialState } from '../../store';
import { ReviewWidgetList } from '../reviewwidgets/ReviewWidgetList';
import ReviewWidgetsPage from '../ReviewWidgetsPage';
import ReviewHeader from '../reviewwidgets/ReviewHeader';

describe('ReviewWidgetsPage', () => {
  it('should render review widgets page properly', () => {
    const mockStore = configureStore();
    const store = mockStore(initialState);
    const wrapper = mount(
      <Provider store={store}>
        <ReviewWidgetsPage />
      </Provider>
    );
    expect(wrapper).toBeDefined();
    expect(wrapper.find(ReviewHeader)).toHaveLength(1);
    expect(wrapper.find(ReviewWidgetList)).toHaveLength(1);
  });
});
