import * as React from 'react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { mount } from 'enzyme';
import { Typography } from '@material-ui/core';

import { initialState } from '../../store';
import { generateUUID } from '../../utils';

import DashboardWidgetComponent from '../DashboardWidget';
import PlantStatusWidget from '../PlantStatusWidget';
import ToDoWidget from '../../widgets/todo/ToDoWidget';
import AppianWidget from '../../widgets/AppianWidget';
import ButtonLinkWidget from '../../widgets/ButtonLinkWidget';
import IFrameWidget from '../../widgets/IFrameWidget';

describe('DashboardWidgetComponent', () => {
  const mockStore = configureStore();
  const componentLists = [
    {
      component: 'IFrameWidget',
      id: ['powerbi'],
      name: 'PowerBI'
    },
    {
      component: 'AppianWidget',
      id: ['appian'],
      name: 'Appian'
    },
    {
      id: ['button', 'Button'],
      component: 'ButtonLinkWidget',
      name: 'Button'
    },
    {
      id: ['todo', 'Todo'],
      component: 'ToDoWidget',
      name: 'Todo'
    },
    {
      id: ['status', 'PlantStatus', 'Plant Status'],
      component: 'PlantStatusWidget',
      name: 'PlantStatus'
    },
    {
      id: ['vsa', 'VSA'],
      component: 'IFrameWidget',
      name: 'VSA'
    }
  ];

  const store = mockStore({
    ...initialState,

    app: {
      ...initialState.app,

      componentLists: componentLists
    }
  });

  it('should render IFrameWidget on  type powerbi', () => {
    const widgetProps = {
      hideChrome: true,
      image: 'https://dpe-dev-mock-api.azurewebsites.net/weather-sm.png',
      modelId: 'weather-sm',
      instanceId: generateUUID(),
      order: 0,
      title: 'Weather',
      type: 'powerbi',
      widgetConfigparms: [
        {
          key: 'widgetSize',
          value: 'small'
        },
        {
          key: 'widgetUrl',
          value: 'https://dpe-dev-mock-api.azurewebsites.net/weather-sm.png'
        }
      ]
    };

    const wrapper = mount(
      <Provider store={store}>
                
        <DashboardWidgetComponent {...widgetProps} />
              
      </Provider>
    );
    expect(wrapper.find(IFrameWidget)).toHaveLength(1);
  });

  it('should render AppianWidget on type appian', () => {
    const widgetProps = {
      hideChrome: true,
      image: 'https://dpe-dev-mock-api.azurewebsites.net/weather-sm.png',
      modelId: 'model1',
      instanceId: generateUUID(),
      order: 0,
      title: 'Appain',
      type: 'appian',
      widgetConfigparms: [
        {
          key: 'widgetSize',
          value: 'small'
        },
        {
          key: 'widgetUrl',
          value: 'https://dpe-dev-mock-api.azurewebsites.net/weather-sm.png'
        }
      ]
    };

    const wrapper = mount(
      <Provider store={store}>
                
        <DashboardWidgetComponent {...widgetProps} />
              
      </Provider>
    );
    expect(wrapper.find(AppianWidget)).toHaveLength(1);
  });

  it('should render ButtonLinkWidget on type button', () => {
    const widgetProps = {
      hideChrome: false,
      image: 'https://dpe-dev-mock-api.azurewebsites.net/weather-sm.png',
      modelId: 'model1',
      instanceId: generateUUID(),
      order: 0,
      title: 'Check Quals',
      type: 'button',
      widgetConfigparms: [
        {
          key: 'widgetSize',
          value: 'small'
        },
        {
          key: 'widgetUrl',
          value: 'https://dpe-dev-mock-api.azurewebsites.net/weather-sm.png'
        }
      ]
    };

    const wrapper = mount(
      <Provider store={store}>
                
        <DashboardWidgetComponent {...widgetProps} />
              
      </Provider>
    );
    expect(wrapper.find(ButtonLinkWidget)).toHaveLength(1);
    expect(
      wrapper.contains(
        <Typography variant="h2" style={{ textTransform: 'uppercase' }}>
          Weather
        </Typography>
      )
    ).toEqual(false);
  });

  it('should render IFrameWidget on type vsa', () => {
    const widgetProps = {
      hideChrome: true,
      image: 'https://dpe-dev-mock-api.azurewebsites.net/weather-sm.png',
      modelId: 'model1',
      instanceId: generateUUID(),
      order: 0,
      title: 'VSA',
      type: 'vsa',
      widgetConfigparms: [
        {
          key: 'widgetSize',
          value: 'small'
        },
        {
          key: 'widgetUrl',
          value: 'https://dpe-dev-mock-api.azurewebsites.net/weather-sm.png'
        }
      ]
    };

    const wrapper = mount(
      <Provider store={store}>
                
        <DashboardWidgetComponent {...widgetProps} />
              
      </Provider>
    );
    expect(wrapper.find(IFrameWidget)).toHaveLength(1);
  });

  it('should render ToDoWidget on type todo', () => {
    const widgetProps = {
      hideChrome: true,
      image: 'https://dpe-dev-mock-api.azurewebsites.net/weather-sm.png',
      modelId: 'model1',
      instanceId: generateUUID(),
      order: 0,
      title: 'ToDo',
      type: 'todo',
      widgetConfigparms: [
        {
          key: 'widgetSize',
          value: 'small'
        },
        {
          key: 'widgetUrl',
          value: 'https://dpe-dev-mock-api.azurewebsites.net/weather-sm.png'
        }
      ]
    };

    const wrapper = mount(
      <Provider store={store}>
                
        <DashboardWidgetComponent {...widgetProps} />
              
      </Provider>
    );
    expect(wrapper.find(ToDoWidget)).toHaveLength(1);
  });

  it('should render PlantStatusWidget on type status', () => {
    const widgetProps = {
      hideChrome: true,
      image: 'https://dpe-dev-mock-api.azurewebsites.net/weather-sm.png',
      modelId: 'model1',
      instanceId: generateUUID(),
      order: 0,
      title: 'PlantStatus',
      type: 'status',
      widgetConfigparms: [
        {
          key: 'widgetSize',
          value: 'small'
        },
        {
          key: 'widgetUrl',
          value: 'https://dpe-dev-mock-api.azurewebsites.net/weather-sm.png'
        }
      ]
    };

    const wrapper = mount(
      <Provider store={store}>
                
        <DashboardWidgetComponent {...widgetProps} />
              
      </Provider>
    );
    expect(wrapper.find(PlantStatusWidget)).toHaveLength(1);
  });

  it('should return emty object on invalid type', () => {
    const widgetProps = {
      hideChrome: true,
      image: 'https://dpe-dev-mock-api.azurewebsites.net/weather-sm.png',
      modelId: 'model1',
      instanceId: generateUUID(),
      order: 0,
      title: 'test',
      type: '',
      widgetConfigparms: [
        {
          key: 'widgetSize',
          value: 'small'
        },
        {
          key: 'widgetUrl',
          value: 'https://dpe-dev-mock-api.azurewebsites.net/weather-sm.png'
        }
      ]
    };

    const wrapper = mount(
      <Provider store={store}>
                
        <DashboardWidgetComponent {...widgetProps} />
              
      </Provider>
    );
    expect(wrapper).toEqual({});
  });
});
