import * as React from 'react';
import { shallow } from 'enzyme';

import FlippableWidget from '../FlippableWidget';
import { Widget } from '../Widget';

describe('FlippableWidget', () => {
  test('should render properly', () => {
    const wrapper = shallow(
      <FlippableWidget
        front={<div>Front</div>}
        back={<div>Back</div>}
        widgetId="test"
        widgetSize="medium"
        startSide="front"
      />
    );

    expect(wrapper).toBeDefined();
    expect(wrapper.find(Widget)).toHaveLength(2);
  });
});
