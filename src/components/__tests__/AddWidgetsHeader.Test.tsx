import * as React from 'react';
import AddWidgetsHeader from '../addwidgets/AddWidgetsHeader';
import { shallow } from 'enzyme';
import GenericButton from '../shared/GenericButton';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import BareSelect from '../shared/BareSelect';
import SearchBar from '../shared/SearchBar';
import SubmitNewModal from '../addwidgets/SubmitNewModal';

describe('AddWidgetsHeader', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render addwidgetsheader properly', () => {
    const AddWidgetsHeaderProps = {
      currentFilter: 'Appian',
      filterOptions: [{ value: 'appian', label: 'Appian' }],
      handleFilterChange: jest.fn(),
      drawerHandle: jest.fn(),
      userName: 'User Name'
    };

    const wrapper = shallow(<AddWidgetsHeader {...AddWidgetsHeaderProps} />);
    expect(wrapper.find(AddWidgetsHeader)).toBeDefined();
    expect(wrapper.find(GenericButton)).toHaveLength(1);
    expect(wrapper.find(BareSelect)).toHaveLength(1);
    expect(wrapper.find(SearchBar)).toHaveLength(1);
    expect(wrapper.find(ArrowForwardIosIcon)).toHaveLength(1);
    expect(wrapper.find(SubmitNewModal)).toHaveLength(1);
  });
});
