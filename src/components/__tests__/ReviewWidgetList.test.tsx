import * as React from 'react';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';

import { initialState } from '../../store';
import { ReviewWidgetList } from '../reviewwidgets/ReviewWidgetList';
import ReviewWidgetTile from '../ReviewWidgetTile';
import { getPendingWidgets } from '../../requests/getPendingWidgets';

jest.mock('../../requests/getPendingWidgets', () => ({
  getPendingWidgets: jest.fn(() => Promise.resolve({ items: [] }))
}));

describe('ReviewWidgetsList', () => {
  it('should list all the widgets to be reviewed', async () => {
    await getPendingWidgets;

    const reviewWidgetRequests = [
      {
        modelId: 'model1',
        title: 'Dashboard',
        subtitle: 'logs',
        type: 'powerbi',
        widgetUrl: 'http://www.example.com/',
        createdBy: 'test',
        createdOn: '11/10/19'
      },
      {
        modelId: 'model2',
        title: 'Report',
        subtitle: 'status',
        type: 'powerbi',
        widgetUrl: 'http://www.example.com/',
        createdBy: 'test',
        createdOn: '11/10/19'
      }
    ];

    const mockStore = configureStore();
    const store = mockStore(initialState);
    const wrapper = mount(
      <Provider store={store}>
                
        <ReviewWidgetList reviewWidgetRequests={reviewWidgetRequests} />
              
      </Provider>
    );
    expect(wrapper).toBeDefined();
    expect(getPendingWidgets).toHaveBeenCalledTimes(1);
    expect(wrapper.find(ReviewWidgetList)).toHaveLength(1);
    expect(
      wrapper.find(ReviewWidgetList).find(ReviewWidgetTile).length
    ).toEqual(2);
  });

  it('should return an empty list of widgets', () => {
    const reviewWidgetRequests = [{}];

    const mockStore = configureStore();
    const store = mockStore(initialState);
    const wrapper = mount(
      <Provider store={store}>
                
        <ReviewWidgetList reviewWidgetRequests={reviewWidgetRequests} />
              
      </Provider>
    );
    expect(wrapper).toBeDefined();
    expect(wrapper.find(ReviewWidgetList)).toHaveLength(1);
    expect(wrapper.find(ReviewWidgetList).find(ReviewWidgetTile)).toEqual({});
  });
});
