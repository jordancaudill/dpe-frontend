import * as React from 'react';
import { Store, AnyAction } from 'redux';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';

import MainPage from '../MainPage';

const mockStore = configureStore();

function shallowWithStore(
  Component: React.ComponentType<any>,
  store: Store<any, AnyAction>
) {
  return shallow(<Component store={store} />)
    .dive()
    .shallow();
}

describe('MainPage', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should not render if not authenticated', () => {
    const store = mockStore({ oidc: { isAuthenticated: false } });
    const wrapper = shallowWithStore(MainPage, store);

    expect(wrapper.find('*')).toHaveLength(0);
  });

  it('should render if authenticated', () => {
    const store = mockStore({ oidc: { isAuthenticated: true } });
    const wrapper = shallowWithStore(MainPage, store);

    expect(wrapper.find('*').length).toBeGreaterThan(0);
  });
});
