import * as React from 'react';
import { shallow } from 'enzyme';

import Snackbar from '../Snackbar';

describe('Snackbar', () => {
  it('should render properly', () => {
    const wrapper = shallow(<Snackbar onClose={() => {}} />);
    expect(wrapper).toBeDefined();
  });
});
