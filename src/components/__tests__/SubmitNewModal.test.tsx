import * as React from 'react';
import { shallow } from 'enzyme';

import NewWidgetForm from '../addwidgets/NewWidgetForm';
import FormSubmissionMessage from '../addwidgets/FormSubmissionMessage';
import SubmitNewModal from '../addwidgets/SubmitNewModal';

describe('SubmitNewModal', () => {
  it('should render SubmitNewModal properly', () => {
    const wrapper = shallow(<SubmitNewModal userName={'user name'} />);
    expect(wrapper).toBeDefined();
  });

  it('should render newwidgetform component', () => {
    const wrapper = shallow(<SubmitNewModal userName={'user name'} />);
    wrapper.setState({ isFormOpen: true });
    expect(wrapper.find(NewWidgetForm)).toHaveLength(1);
  });
  it('should render FormSubmissionMessage component', () => {
    const wrapper = shallow(<SubmitNewModal userName={'user name'} />);
    wrapper.setState({ isFormOpen: false });
    expect(wrapper.find(FormSubmissionMessage)).toHaveLength(1);
  });
});
