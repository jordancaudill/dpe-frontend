import * as React from 'react';
import { shallow } from 'enzyme';
import { useSelector } from 'react-redux';
import { mocked } from 'ts-jest/utils';

import ReviewHeader from '../reviewwidgets/ReviewHeader';

jest.mock('react-redux', () => ({
  useSelector: jest.fn()
}));

describe('ReviewHeader', () => {
  it('should render review widgets header properly', () => {
    const count = 2;
    mocked(useSelector).mockImplementation(() => count);
    const wrapper = shallow(<ReviewHeader />);
    expect(wrapper).toBeDefined();
    expect(wrapper.find('span').text()).toEqual('(2)');
  });
});
