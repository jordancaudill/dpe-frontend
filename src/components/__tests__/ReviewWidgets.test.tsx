import * as React from 'react';
import configureStore from 'redux-mock-store';

import { mount } from 'enzyme';
import { initialState } from '../../store';
import { ReviewWidgetList } from '../reviewwidgets/ReviewWidgetList';

import ReviewWidgets from '../ReviewWidgets';
import GenericButton from '../shared/GenericButton';
import TitleModal from '../shared/TitleModal';
import ReviewWidgetsPage from '../ReviewWidgetsPage';
import ReviewHeader from '../reviewwidgets/ReviewHeader';
import { Provider } from 'react-redux';

describe('ReviewWidgets', () => {
  it('should render review widgets properly', () => {
    const mockStore = configureStore();
    const store = mockStore(initialState);
    const wrapper = mount(
      <Provider store={store}>
                
        <ReviewWidgets />
              
      </Provider>
    );
    expect(wrapper).toBeDefined();
    expect(wrapper.find(TitleModal)).toHaveLength(1);
    expect(wrapper.find(GenericButton)).toHaveLength(1);

    wrapper.find(GenericButton).simulate('click');
    expect(wrapper.find(ReviewWidgetsPage)).toHaveLength(1);
    expect(wrapper.find(ReviewHeader)).toHaveLength(1);
    expect(wrapper.find(ReviewWidgetList)).toHaveLength(1);
  });
});
