import * as React from 'react';
import AddWidgetList from '../addwidgets/AddWidgetList';
import { shallow } from 'enzyme';
import AddWidgetTile from '../AddWidgetTile';

describe('AddWidgetList', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render  properly', () => {
    const AddWidgetListProps = {
      currentFilter: 'Appian',
      filterOptions: [{ value: 'appian', label: 'Appian' }],
      handleFilterChange: jest.fn(),
      drawerHandle: jest.fn(),
      tiles: [
        {
          modelId: 'model1',
          title: 'Dashboard',
          subtitle: 'logs',
          type: 'appian',
          widgetUrl: 'http://www.example.com/',
          createdBy: 'test',
          createdOn: '11/10/19'
        }
      ]
    };

    const wrapper = shallow(<AddWidgetList {...AddWidgetListProps} />);
    expect(wrapper.find(AddWidgetList)).toBeDefined();
    expect(wrapper.find(AddWidgetTile)).toHaveLength(1);
  });
});
