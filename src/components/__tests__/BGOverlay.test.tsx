import * as React from 'react';
import { shallow } from 'enzyme';

import BGOverlay from '../BGOverlay';

describe('BGOverlay', () => {
  it('should render properly', () => {
    const wrapper = shallow(<BGOverlay display onClick={() => {}} />);
    expect(wrapper).toBeDefined();
    expect(wrapper.find('div')).toHaveLength(1);
  });
});

describe('BGOverlay', () => {
  it('should hide properly', () => {
    const wrapper = shallow(<BGOverlay display={false} onClick={() => {}} />);
    expect(wrapper).toBeDefined();
    expect(wrapper.find('div')).toHaveLength(0);
  });
});
