import * as React from 'react';
import { createStore, AnyAction, Store } from 'redux';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import createMemoryHistory from 'history/createMemoryHistory';

import { setWidgetList } from '../../actions/dashboard/actions';
import { updateMarketplaceWidgets } from '../../actions/marketplace/actions';
import createRootReducer from '../../reducers';
import { StoreState } from '../../store';
import { generateUUID } from '../../utils';

import WidgetGrid from '../WidgetGrid';
import DashboardWidgetComponent from '../DashboardWidget';

jest.mock('../../requests/getComponentsRequests');
jest.mock('../../requests/getAvailableWidgets');

let store: Store<StoreState, AnyAction>;

describe('WidgetGrid', () => {
  beforeEach(() => {
    store = createStore(createRootReducer(createMemoryHistory()));
  });

  it('should render no widgets properly', () => {
    const wrapper = mount(
      <Provider store={store}>
        <WidgetGrid />
      </Provider>
    );
    expect(wrapper).toBeDefined();
  });

  it('should render legacy widgets properly', () => {
    const testWidgets = [
      {
        title: 'Test1',
        image: 'https://example.com/test-1.png',
        widgetUrl: 'https://example.com/test-1.png',
        modelId: 'test-1',
        instanceId: generateUUID(),
        order: 0,
        type: 'powerbi',
        widgetConfigparms: [{ key: 'widgetSize', value: 'small' }]
      },
      {
        title: 'Test2',
        image: 'https://example.com/test-2.png',
        widgetUrl: 'https://example.com/test-2.png',
        modelId: 'test-2',
        instanceId: generateUUID(),
        order: 1,
        type: 'powerbi',
        widgetConfigparms: [{ key: 'widgetSize', value: 'medium' }]
      },
      {
        title: 'Test3',
        image: 'https://example.com/test-3.png',
        widgetUrl: 'https://example.com/test-3.png',
        modelId: 'test-3',
        instanceId: generateUUID(),
        order: 1,
        type: 'powerbi',
        widgetConfigparms: [{ key: 'widgetSize', value: 'large' }]
      },
      {
        title: 'Test4',
        image: 'https://example.com/test-4.png',
        widgetUrl: 'https://example.com/test-4.png',
        modelId: 'test-4',
        instanceId: generateUUID(),
        order: 1,
        type: 'powerbi',
        widgetConfigparms: [{ key: 'widgetSize', value: 'extra-large' }]
      },
      {
        title: 'Test5',
        image: 'https://example.com/test-5.png',
        widgetUrl: 'https://example.com/test-4.png',
        modelId: 'test-5',
        instanceId: generateUUID(),
        order: 1,
        type: 'powerbi',
        widgetConfigparms: []
      }
    ];

    const wrapper = mount(
      <Provider store={store}>
        <WidgetGrid />
      </Provider>
    );
    store.dispatch(updateMarketplaceWidgets(testWidgets));
    store.dispatch(setWidgetList({ dashboardOrder: testWidgets }));
    wrapper.update();
    expect(wrapper.find(DashboardWidgetComponent)).toHaveLength(
      testWidgets.length
    );

    const state = store.getState();
    expect(state.dashboard.layouts.lg).toHaveLength(testWidgets.length + 1); // est widgets + 1 dashboard ui
    testWidgets.forEach(testWidget => {
      const layout = state.dashboard.layouts.lg.find(
        l => l.i === testWidget.instanceId
      );
      expect(layout).toBeDefined();
      if (layout) {
        const size = testWidget.widgetConfigparms.find(
          o => o.key === 'widgetSize'
        );
        switch (size && size.value) {
          case 'small':
            expect(layout.w).toBe(3);
            break;
          case 'medium':
            expect(layout.w).toBe(6);
            break;
          case 'large':
            expect(layout.w).toBe(9);
            break;
          case 'extra-large':
          default:
            expect(layout.w).toBe(12);
            break;
        }
        expect(layout.h).toBe(6);
      }
    });
  });

  it('should render new widgets properly', () => {
    const testWidgets = [
      {
        title: 'Test1',
        image: 'https://example.com/test-1.png',
        widgetUrl: 'https://example.com/test-1.png',
        modelId: 'test-1',
        instanceId: generateUUID(),
        order: 0,
        type: 'powerbi',
        widgetConfigparms: [{ key: 'w', value: '3' }, { key: 'h', value: '1' }]
      },
      {
        title: 'Test2',
        image: 'https://example.com/test-2.png',
        widgetUrl: 'https://example.com/test-2.png',
        modelId: 'test-2',
        instanceId: generateUUID(),
        order: 1,
        type: 'powerbi',
        widgetConfigparms: [{ key: 'w', value: '3' }, { key: 'h', value: '1' }]
      }
    ];

    const wrapper = mount(
      <Provider store={store}>
        <WidgetGrid />
      </Provider>
    );
    store.dispatch(updateMarketplaceWidgets(testWidgets));
    store.dispatch(setWidgetList({ dashboardOrder: testWidgets }));
    wrapper.update();
    expect(wrapper.find(DashboardWidgetComponent)).toHaveLength(
      testWidgets.length
    );

    const state = store.getState();
    expect(state.dashboard.layouts.lg).toHaveLength(testWidgets.length + 1); // est widgets + 1 dashboard ui
    testWidgets.forEach(testWidget => {
      const layout = state.dashboard.layouts.lg.find(
        l => l.i === testWidget.instanceId
      );
      expect(layout).toBeDefined();
      layout && expect(layout.w).toBe(3);
      layout && expect(layout.h).toBe(1);
    });
  });

  it('should render resized legacy widgets properly', () => {
    const testWidgets = [
      {
        title: 'Test1',
        image: 'https://example.com/test-1.png',
        widgetUrl: 'https://example.com/test-1.png',
        modelId: 'test-1',
        instanceId: generateUUID(),
        order: 0,
        type: 'powerbi',
        widgetConfigparms: [{ key: 'widgetSize', value: 'small' }],
        widgetInstanceConfigparms: [
          { key: 'w', value: '10' },
          { key: 'h', value: '10' }
        ]
      },
      {
        title: 'Test2',
        image: 'https://example.com/test-2.png',
        widgetUrl: 'https://example.com/test-2.png',
        modelId: 'test-2',
        instanceId: generateUUID(),
        order: 1,
        type: 'powerbi',
        widgetConfigparms: [{ key: 'widgetSize', value: 'medium' }],
        widgetInstanceConfigparms: [
          { key: 'w', value: '10' },
          { key: 'h', value: '10' }
        ]
      }
    ];

    const wrapper = mount(
      <Provider store={store}>
        <WidgetGrid />
      </Provider>
    );
    store.dispatch(updateMarketplaceWidgets(testWidgets));
    store.dispatch(setWidgetList({ dashboardOrder: testWidgets }));
    wrapper.update();

    const state = store.getState();
    expect(state.dashboard.layouts.lg).toHaveLength(testWidgets.length + 1); // est widgets + 1 dashboard ui
    testWidgets.forEach(testWidget => {
      const layout = state.dashboard.layouts.lg.find(
        l => l.i === testWidget.instanceId
      );
      expect(layout).toBeDefined();
      layout && expect(layout.w).toBe(10);
      layout && expect(layout.h).toBe(10);
    });
  });

  it('should render resized new widgets properly', () => {
    const testWidgets = [
      {
        title: 'Test1',
        image: 'https://example.com/test-1.png',
        widgetUrl: 'https://example.com/test-1.png',
        modelId: 'test-1',
        instanceId: generateUUID(),
        order: 0,
        type: 'powerbi',
        widgetConfigparms: [{ key: 'w', value: '3' }, { key: 'h', value: '1' }],
        widgetInstanceConfigparms: [
          { key: 'w', value: '10' },
          { key: 'h', value: '10' }
        ]
      },
      {
        title: 'Test2',
        image: 'https://example.com/test-2.png',
        widgetUrl: 'https://example.com/test-2.png',
        modelId: 'test-2',
        instanceId: generateUUID(),
        order: 1,
        type: 'powerbi',
        widgetConfigparms: [{ key: 'w', value: '3' }, { key: 'h', value: '1' }],
        widgetInstanceConfigparms: [
          { key: 'w', value: '10' },
          { key: 'h', value: '10' }
        ]
      }
    ];

    const wrapper = mount(
      <Provider store={store}>
        <WidgetGrid />
      </Provider>
    );
    store.dispatch(updateMarketplaceWidgets(testWidgets));
    store.dispatch(setWidgetList({ dashboardOrder: testWidgets }));
    wrapper.update();

    const state = store.getState();
    expect(state.dashboard.layouts.lg).toHaveLength(testWidgets.length + 1); // est widgets + 1 dashboard ui
    testWidgets.forEach(testWidget => {
      const layout = state.dashboard.layouts.lg.find(
        l => l.i === testWidget.instanceId
      );
      expect(layout).toBeDefined();
      layout && expect(layout.w).toBe(10);
      layout && expect(layout.h).toBe(10);
    });
  });
});
