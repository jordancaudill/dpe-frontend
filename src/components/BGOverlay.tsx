import * as React from 'react';
import { MouseEventHandler } from 'react';

type BGOverlayProps = {
  display: boolean,
  onClick: MouseEventHandler<Element>,
  extraClasses?: string,
}

const BGOverlay = (props: BGOverlayProps) => {
  const display = () =>
    props.display ? (
      <div onClick={props.onClick} className={`bg-overlay ${props.extraClasses ? props.extraClasses : ''}`} />
    ) : null;

  return <>{display()}</>;
};

export default BGOverlay;
