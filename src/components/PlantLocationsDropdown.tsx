import * as React from 'react';
import { connect } from 'react-redux';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import { StoreState } from '../store';
import plantLocationsRequests from '../requests/plantLocationsRequests';
import { Plant } from '../models/hl_data/plant.model';

const plantLocationFormatter = (text: string) => {
  return text
    .toLowerCase()
    .split(' ')
    .map(s => s.charAt(0).toUpperCase() + s.substring(1))
    .join(' ');
};

type PlantLocationsDropdownProps = ReturnType<typeof mapStateToProps> & {
  plantLocation?: Plant;
  onChange?: (
    event: React.ChangeEvent<{ name?: string; value: Plant }>
  ) => void;
  options?: unknown;
};

export class PlantLocationsDropdown extends React.Component<
  PlantLocationsDropdownProps
> {
  componentDidMount() {
    plantLocationsRequests(); // We should already have plant data, but just in case
  }

  render() {
    if (!this.props.plantLocation) return null;
    const plantLocation = this.props.plantLocation;

    return (
      <Select
        value={plantLocation}
        renderValue={() => plantLocationFormatter(plantLocation.name)}
        onChange={ev =>
          this.props.onChange &&
          this.props.onChange(ev as React.ChangeEvent<{
            name?: string;
            value: Plant;
          }>)
        }
        onClick={ev => ev.stopPropagation()}
        disableUnderline={true}
        style={{
          fontSize: '14px',
          color: '#9b9b9b',
          marginTop: '-5px',
          letterSpacing: '-0.04px'
        }}
      >
        {this.props.options &&
          this.props.options.map((item, i) => (
            <MenuItem key={i} value={(item as unknown) as string}>
              {plantLocationFormatter(item.name)}
            </MenuItem>
          ))}
      </Select>
    );
  }
}

function mapStateToProps(state: StoreState) {
  return {
    options: state.app.plantLocations
  };
}

export default connect(mapStateToProps)(PlantLocationsDropdown);
