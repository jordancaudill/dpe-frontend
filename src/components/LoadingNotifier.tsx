import * as React from 'react';
import { PulseLoader } from 'react-spinners';

import '../css/LoadingNotifier.css';

type LoadingNotifierProps = {
  classes?: string;
  color?: string;
}

export default class LoadingNotifier extends React.Component<LoadingNotifierProps> {
  render() {
    return (
      <div className={`loading-notifier ${this.props.classes}`}>
        <PulseLoader color={this.props.color || '#5ABAFF'} />
      </div>
    );
  }
}
