import * as React from 'react';
import { connect } from 'react-redux';
import { Badge } from '@material-ui/core';
import { Theme, withStyles, createStyles } from '@material-ui/core/styles';
import { getPendingWidgets } from '../requests/getPendingWidgets';

import { StoreState } from '../store';
import ReviewWidgetsPage from './ReviewWidgetsPage';
import GenericButton from './shared/GenericButton';
import TitleModal from './shared/TitleModal';

const StyledBadge = withStyles((theme: Theme) =>
  createStyles({
    badge: {
      backgroundColor: '#f5a623',
      color: '#ffffff'
    }
  })
)(Badge);

type ReviewWidgetsProps = ReturnType<typeof mapStateToProps>;

type ReviewWidgetsState = {
  open: boolean;
};

class ReviewWidgets extends React.Component<
  ReviewWidgetsProps,
  ReviewWidgetsState
> {
  state = {
    open: false
  };

  componentDidMount() {
    getPendingWidgets();
  }

  render() {
    return (
      <>
        <StyledBadge badgeContent={this.props.reviewWidgetRequestCount}>
          <GenericButton
            textColor="#4a4a4a"
            bdColor="#4a4a4a"
            handleClick={() => this.setState({ open: true })}
          >
            REVIEW WIDGETS
          </GenericButton>
        </StyledBadge>
        <TitleModal
          open={this.state.open}
          onClose={() => this.setState({ open: false })}
          maxWidth="md"
          fullWidth
          title="Review Submissions"
        >
          <ReviewWidgetsPage />
        </TitleModal>
      </>
    );
  }
}

function mapStateToProps(state: StoreState) {
  return {
    reviewWidgetRequestCount: state.app.reviewWidgetRequestCount
  };
}

export default connect(mapStateToProps)(ReviewWidgets);
