import * as React from 'react';
import { Action } from 'redux';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { IconButton, Menu, MenuItem } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';

import { StoreState } from '../store';
import { removeDashboardWidget } from '../actions/dashboard/actions';

export type WidgetOptionsProps = {
  widgetId: string;
  removeClick?: () => void;
  refreshClick?: () => void;
  hideOptions?: boolean;
};

export type WidgetOptionsState = {
  anchorEl: HTMLButtonElement | null;
  open: boolean;
};

class WidgetOptions extends React.Component<
  ReturnType<typeof mapDispatchToProps> & WidgetOptionsProps,
  WidgetOptionsState
> {
  state = {
    anchorEl: null,
    open: false
  };

  handleClick(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
    this.setState({
      anchorEl: event.currentTarget
    });
  }

  handleClose() {
    this.setState({
      anchorEl: null
    });
  }

  render() {
    return !this.props.hideOptions ? (
      <>
        <IconButton
          style={{
            position: 'absolute',
            right: 0,
            top: 0
          }}
          aria-label="more"
          aria-controls="long-menu"
          aria-haspopup="true"
          onClick={ev => this.handleClick(ev)}
        >
          <MoreVertIcon />
        </IconButton>
        <Menu
          anchorEl={this.state.anchorEl}
          keepMounted
          open={Boolean(this.state.anchorEl)}
          onClose={() => this.handleClose()}
        >
          {'function' === typeof this.props.refreshClick ? (
            <MenuItem
              onClick={() => {
                this.props.refreshClick && this.props.refreshClick();
                this.handleClose();
              }}
            >
              Refresh Widget
            </MenuItem>
          ) : null}
          <MenuItem
            onClick={() => {
              this.props.removeClick && this.props.removeClick();
              this.props.removeDashboardWidget(this.props.widgetId);
              this.handleClose();
            }}
          >
            Remove Widget
          </MenuItem>
        </Menu>
      </>
    ) : null;
  }
}

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(
  dispatch: ThunkDispatch<StoreState, undefined, Action>
) {
  return {
    removeDashboardWidget: (widgetId: string) =>
      dispatch(removeDashboardWidget(widgetId))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WidgetOptions);
