import * as React from 'react';
import { widgetContainer, ContentProps } from './WidgetContainer';

const IFrameWidget = (props: ContentProps) => {
  const src =
    props.config && props.config.widgetUrl ? props.config.widgetUrl : '';
  return (
    <iframe
      src={src}
      title={props.title ? props.title.toString() : ''}
      style={{
        display: 'block',
        width: '100%',
        height: '100%',
        overflow: 'hidden',
        overflowY: 'auto',
        border: 'none'
      }}
      name={props && props.title ? props.title.toString() : ''}
    />
  );
};

export default widgetContainer(IFrameWidget);
