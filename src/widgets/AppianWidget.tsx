import * as React from 'react';
import { widgetContainer, ContentProps } from './WidgetContainer';

const AppianWidget = (props: ContentProps) => {
  const reportUrlStub =
    props.config && props.config.widgetUrl ? props.config.widgetUrl : '';
  if (!document.getElementById('appianEmbedded')) {
    let appianScriptInject = document.createElement('script');
    //@ts-ignore
    appianScriptInject.setAttribute('src', process.env.REACT_APP_APPIAN_URL);
    appianScriptInject.setAttribute('id', 'appianEmbedded');

    document.getElementsByTagName('head')[0].appendChild(appianScriptInject);
  }
  // @ts-ignore
  return <appian-report reportUrlStub={reportUrlStub} />;
};

export default widgetContainer(AppianWidget);
