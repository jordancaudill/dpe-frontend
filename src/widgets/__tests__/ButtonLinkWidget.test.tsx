import * as React from 'react';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';
import ButtonLinkWidget from '../ButtonLinkWidget';

let store: any;
const initialAppState = {
  app: {
    user: {
      id: '1'
    },
    plantLocations: [
      {
        id: '1',
        name: 'Calvert Cliffs'
      }
    ]
  }
};

describe('ButtonLinkWidget', () => {
  beforeEach(() => {
    const mockStore = configureStore();
    store = mockStore(initialAppState);
  });
  it('should render the ButtonLink widget', () => {
    const ContentProps = {
      title: 'CHECK YOUR QUALS',
      widgetSize: 'extra-large',
      widgetId: '1',
      src: 'www.chaione.com'
    };

    const wrapper = mount(
      <Provider store={store}>
        <ButtonLinkWidget {...ContentProps} />
      </Provider>
    );
    expect(wrapper).toBeDefined();
  });
  it('should display the text on the button', () => {
    const ContentProps = {
      title: 'CHECK YOUR QUALS',
      widgetSize: 'extra-large',
      widgetId: '1',
      src: 'www.chaione.com'
    };
    const wrapper = mount(
      <Provider store={store}>
        <ButtonLinkWidget {...ContentProps} />
      </Provider>
    );
    expect(wrapper.text()).toEqual(ContentProps.title);
  });

  it('should not render the ButtonLink widget wen src is null', () => {
    const ContentProps = {
      title: 'CHECK YOUR QUALS',
      widgetSize: 'extra-large',
      widgetId: '1'
    };

    const wrapper = mount(
      <Provider store={store}>
        <ButtonLinkWidget {...ContentProps} />
      </Provider>
    );
    expect(wrapper.find('ButtonLinkWidget').children().length).toEqual(0);
  });
});
