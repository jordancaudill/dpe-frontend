import * as React from 'react';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';
import widgetContainer, { ContentProps } from '../WidgetContainer';

let store: any;
const initialAppState = {
  app: {
    user: {
      id: '1'
    },
    plantLocations: [
      {
        id: '1',
        name: 'Calvert Cliffs'
      }
    ]
  }
};

describe('widgetContainer', () => {
  beforeEach(() => {
    const mockStore = configureStore();

    store = mockStore(initialAppState);
  });
  it('should pass down the plant locations and user as props', () => {
    class ChildComponent extends React.Component<ContentProps> {
      render() {
        return <h1>Hello World!</h1>;
      }
    }
    const HOC = widgetContainer(ChildComponent) as any;
    const wrapper = mount(
      <Provider store={store}>
        <HOC />
      </Provider>
    );
    expect(wrapper.find(ChildComponent).props().plantLocations).toEqual(
      initialAppState.app.plantLocations
    );
    expect(wrapper.find(ChildComponent).props().user).toEqual(
      initialAppState.app.user
    );
  });
});
