import * as React from 'react';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';
import IFrameWidget from '../IFrameWidget';

let store: any;
const initialAppState = {
  app: {
    user: {
      id: '1'
    },
    plantLocations: [
      {
        id: '1',
        name: 'Calvert Cliffs'
      }
    ]
  }
};
const ContentProps = {
  title: 'iFrame',
  widgetSize: 'extra-large',
  widgetId: '1',
  src: 'www.chaione.com'
};

describe('IFrameWidget', () => {
  beforeEach(() => {
    const mockStore = configureStore();
    store = mockStore(initialAppState);
  });

  it('should render the IFrame widget', () => {
    const wrapper = mount(
      <Provider store={store}>
        <IFrameWidget {...ContentProps} />
      </Provider>
    );
    expect(wrapper).toBeDefined();
  });
  it('should display the title', () => {
    const wrapper = mount(
      <Provider store={store}>
        <IFrameWidget {...ContentProps} />
      </Provider>
    );
    expect(wrapper.find('h2').text()).toEqual(ContentProps.title);
  });
});
