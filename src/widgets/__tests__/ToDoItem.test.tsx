import * as React from 'react';
import { shallow } from 'enzyme';
import ToDoItem from '../todo/ToDoItem';
import MeetingCaption from '../todo/captions/MeetingCaption';
import { Chip } from '@material-ui/core';

const toggleModal = () => console.log(true);

const toDoItem = {
  item: {
    type: 'M' as 'M',
    body: 'Meeting with team',
    id: '4',
    meetingInfo: {
      date: 'Wednesday, April 3, 2019',
      startTime: '12:00 PM (EST)',
      endTime: '1:00 PM (EST)',
      length: 1,
      organizer: 'Kennedy, McKenzie',
      attendees: [
        { name: 'brian carrol' },
        { name: 'kevin burroughs' },
        { name: 'davisson, mark' },
        { name: 'barnum, michael' },
        { name: 'peterson, elizabeth' }
      ]
    }
  },
  itemKey: `Due Today toDo0`,
  category: `Due Today`,
  onClick: toggleModal
};

describe('ToDoItem', () => {
  it('should render the caption and category', () => {
    const wrapper = shallow(<ToDoItem {...toDoItem} />);
    expect(wrapper.find(MeetingCaption)).toBeDefined();
    expect(wrapper.find(Chip)).toBeDefined();
  });

  it('should return the correct system for the item', () => {
    const wrapper = shallow(<ToDoItem {...toDoItem} />);
    const receivedValue = (wrapper.instance() as ToDoItem).getTypeSystem(
      toDoItem.item.type
    );
    expect(receivedValue).toEqual('Office 365');
  });

  it('should return the correct chip color for the item', () => {
    const wrapper = shallow(<ToDoItem {...toDoItem} />);
    const receivedValue = (wrapper.instance() as ToDoItem).getChipColor();
    expect(receivedValue).toEqual('#f5a623');
  });
});
