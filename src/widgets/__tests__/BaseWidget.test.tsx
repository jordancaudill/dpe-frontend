import * as React from 'react';
import { Provider } from 'react-redux';
import { shallow, mount } from 'enzyme';
import configureStore from 'redux-mock-store';

import { initialState } from '../../store';
import BaseWidget from '../BaseWidget';
import WidgetOptions from '../WidgetOptions';

const mockStore = configureStore();
let store: any;

describe('BaseWidget', () => {
  beforeEach(() => {
    store = mockStore(initialState);
  });

  it('should render widget', () => {
    const title = 'hello world';
    const wrapper = mount(
      <Provider store={store}>
        <BaseWidget
          widgetId={new Date().getTime().toString()}
          title={title}
          widgetSize="small"
        />
      </Provider>
    );
    expect(wrapper).toBeDefined();
    expect(wrapper.find('.draggableHandle')).toHaveLength(1);
    expect(wrapper.find('h2').text()).toEqual(title);
    expect(wrapper.find('h3')).toHaveLength(0);
    expect(wrapper.find(WidgetOptions)).toHaveLength(1);
  });

  it('should render subtitle', () => {
    const subtitle = 'hello world';
    const wrapper = mount(
      <Provider store={store}>
        <BaseWidget
          widgetId={new Date().getTime().toString()}
          title="test"
          subtitle={subtitle}
          widgetSize="small"
        />
      </Provider>
    );
    expect(wrapper.find('h3').text()).toEqual(subtitle);
  });

  it('should render children', () => {
    const TestChildren = () => <div>Hello World</div>;
    const wrapper = shallow(
      <BaseWidget
        widgetId={new Date().getTime().toString()}
        title="test"
        widgetSize="small"
      >
        <TestChildren />
      </BaseWidget>
    ).shallow();
    expect(wrapper.find(TestChildren)).toHaveLength(1);
  });

  it('should render without chrome', () => {
    const wrapper = shallow(
      <BaseWidget
        widgetId={new Date().getTime().toString()}
        title="test"
        hideChrome
        widgetSize="small"
      />
    );
    expect(wrapper).toBeDefined();
    expect(wrapper.find('.draggableHandle')).toHaveLength(1);
  });
});
