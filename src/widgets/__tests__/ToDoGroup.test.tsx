import * as React from 'react';
import { shallow } from 'enzyme';
import ToDoGroup from '../todo/ToDoGroup';
import ToDoItem from '../todo/ToDoItem';
import OutlookModal from '../todo/modals/OutlookModal';
import ToDoModal from '../todo/modals/ToDoModal';
import PassportModal from '../todo/modals/PassportModal';
import { fetchWrapper } from '../../utils';

const toDoGroup = {
  category: 'Due Today',
  tasks: [
    {
      type: 'M' as 'M',
      body: 'Meeting with team',
      id: '4',
      meetingInfo: {
        date: 'Wednesday, April 3, 2019',
        startTime: '12:00 PM (EST)',
        endTime: '1:00 PM (EST)',
        length: 1,
        organizer: 'Kennedy, McKenzie',
        attendees: [
          { name: 'brian carrol' },
          { name: 'kevin burroughs' },
          { name: 'davisson, mark' },
          { name: 'barnum, michael' },
          { name: 'peterson, elizabeth' }
        ]
      }
    },
    {
      type: 'T' as 'T',
      body: 'Check plant status and coordinate with green team for update',
      id: '5'
    },
    {
      type: 'T' as 'T',
      body: 'Check plant status for variant data alignment',
      id: '6'
    },
    {
      type: 'P' as 'P',
      body: 'One on one with team to review status uptage for project x',
      id: '7'
    }
  ]
};
const mockFetchToDos = () => toDoGroup;

const updatedToDos = {
  category: toDoGroup.category,
  tasks: toDoGroup.tasks.slice(0, 2)
};

jest.mock('../../utils', () => ({
  fetchWrapper: jest.fn(() => Promise.resolve(updatedToDos))
}));

describe('ToDoGroup', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render four todo item components', () => {
    const wrapper = shallow(
      <ToDoGroup group={toDoGroup} fetchToDos={mockFetchToDos} />
    );
    expect(wrapper.find(ToDoItem)).toHaveLength(4);
  });

  it('should associate the correct modal with the task', () => {
    const wrapper = shallow(
      <ToDoGroup group={toDoGroup} fetchToDos={mockFetchToDos} />
    );
    const modals = toDoGroup.tasks.map(task =>
      (wrapper.instance() as ToDoGroup).getModalContent(task)
    );
    expect(modals[0] ? modals[0].type : null).toEqual(OutlookModal);
    expect(modals[1] ? modals[1].type : null).toEqual(ToDoModal);
    expect(modals[2] ? modals[2].type : null).toEqual(ToDoModal);
    expect(modals[3] ? modals[3].type : null).toEqual(PassportModal);
  });

  it('should update the list of tasks', async () => {
    const mockToDos = Promise.resolve(updatedToDos);
    let wrapper = shallow(
      <ToDoGroup group={toDoGroup} fetchToDos={mockFetchToDos} />
    );
    await mockToDos;

    (wrapper.instance() as ToDoGroup).updateTodo(toDoGroup.tasks[3]);
    wrapper.update();
    expect(fetchWrapper).toHaveBeenCalledTimes(1);
    let receivedValues = wrapper.state('group') as any;
    expect(receivedValues.tasks).toHaveLength(3);
  });
});
