import * as React from 'react';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';
import ToDoWidget from '../todo/ToDoWidget';

import { mocked } from 'ts-jest/utils';
import { fetchWrapper, todoTransform } from '../../utils';
import ToDoGroup from '../todo/ToDoGroup';

let store: any;
const initialAppState = {
  app: {
    user: {
      id: '1'
    },
    plantLocations: [
      {
        id: '1',
        name: 'Calvert Cliffs'
      }
    ]
  }
};
const toDoLists = [
  {
    category: 'Overdue',
    tasks: [
      {
        type: 'T' as 'T',
        body: 'Check plant status for rad levels',
        id: '1'
      },
      { type: 'P' as 'P', body: 'Create plant report for unit one', id: '2' },
      { type: 'P' as 'T', body: 'Submit status readings', id: '3' }
    ]
  },
  {
    category: 'Due Today',
    tasks: [
      {
        type: 'M' as 'M',
        body: 'Meeting with team',
        id: '4',
        meetingInfo: {
          date: 'Wednesday, April 3, 2019',
          startTime: '12:00 PM (EST)',
          endTime: '1:00 PM (EST)',
          length: 1,
          organizer: 'Kennedy, McKenzie',
          attendees: [
            { name: 'brian carrol' },
            { name: 'kevin burroughs' },
            { name: 'davisson, mark' },
            { name: 'barnum, michael' },
            { name: 'peterson, elizabeth' }
          ]
        }
      },
      {
        type: 'T' as 'T',
        body: 'Check plant status and coordinate with green team for update',
        id: '5'
      },
      {
        type: 'T' as 'T',
        body: 'Check plant status for variant data alignment',
        id: '6'
      },
      {
        type: 'P' as 'P',
        body: 'One on one with team to review status uptage for project x',
        id: '7'
      }
    ]
  },
  {
    category: '',
    tasks: [
      { type: 'T' as 'T', body: 'Update systems at plant B', id: '8' },
      { type: 'T' as 'T', body: 'Check temperature for report', id: '9' }
    ]
  }
];
const ContentProps = {
  title: 'My To-Do List',
  widgetSize: 'extra-large',
  widgetId: '1'
};

jest.mock('../../utils', () => ({
  fetchWrapper: jest.fn(() => Promise.resolve({ items: toDoLists })),
  todoTransform: jest.fn(() => toDoLists),
  generateUUID: jest.fn(() => 'test-uuid')
}));

describe('ToDoWidget', () => {
  beforeEach(() => {
    const mockStore = configureStore();
    store = mockStore(initialAppState);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render three todo group components', async () => {
    const mockToDos = Promise.resolve(toDoLists);
    mocked(fetchWrapper).mockImplementation(() => mockToDos);
    mocked(todoTransform).mockImplementation(() => toDoLists);

    const wrapper = mount(
      <Provider store={store}>
        <ToDoWidget {...ContentProps} />
      </Provider>
    );
    await mockToDos;

    wrapper.update();
    expect(wrapper.find(ToDoGroup)).toHaveLength(3);
  });

  it('should render a message when no items are present', async () => {
    const mockToDos = Promise.resolve(toDoLists);
    mocked(fetchWrapper).mockImplementation(() => mockToDos);
    mocked(todoTransform).mockImplementation(() => []);

    const wrapper = mount(
      <Provider store={store}>
        <ToDoWidget {...ContentProps} />
      </Provider>
    );
    await mockToDos;

    wrapper.update();
    expect(wrapper.find('div.finished-message')).toHaveLength(1);
  });
});
