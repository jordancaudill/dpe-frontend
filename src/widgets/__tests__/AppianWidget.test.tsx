import * as React from 'react';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';
import AppianWidget from '../AppianWidget';

let store: any;
const initialAppState = {
  app: {
    user: {
      id: '1'
    },
    plantLocations: [
      {
        id: '1',
        name: 'Calvert Cliffs'
      }
    ]
  }
};
const appianWidgetProps = {
  title: 'Appian',
  widgetSize: 'extra-large',
  widgetId: '1',
  reportUrlStub: 'www.chaione.com'
};

describe('AppianWidget', () => {
  beforeEach(() => {
    const mockStore = configureStore();
    store = mockStore(initialAppState);
  });

  it('should render the Appian widget', () => {
    const wrapper = mount(
      <Provider store={store}>
        <AppianWidget {...appianWidgetProps} />
      </Provider>
    );
    expect(wrapper).toBeDefined();
  });

  it('should display the title', () => {
    const wrapper = mount(
      <Provider store={store}>
        <AppianWidget {...appianWidgetProps} />
      </Provider>
    );
    expect(wrapper.find('h2').text()).toEqual(appianWidgetProps.title);
  });
});
