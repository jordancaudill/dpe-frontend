import * as React from 'react';
import { Modal, Paper } from '@material-ui/core';
import { Todo } from '../../models/app/todo.model';
import ToDoItem from './ToDoItem';
import ToDoModal from './modals/ToDoModal';
import OutlookModal from './modals/OutlookModal';
import PassportModal from './modals/PassportModal';
import { fetchWrapper } from '../../utils';
import { Item } from '../../models/todo/item.model';

type ToDoGroupProps = {
  group: {
    category: string;
    tasks: Todo[];
  };
  fetchToDos: () => void;
};

type ToDoGroupState = {
  open: boolean;
  modalItem?: Todo;
  group: {
    category: string;
    tasks: Todo[];
  };
};

export class ToDoGroup extends React.Component<ToDoGroupProps, ToDoGroupState> {
  state: ToDoGroupState = {
    open: false,
    group: this.props.group
  };

  getModalContent = (item: Todo) => {
    switch (item.type) {
      case 'T':
        return (
          <ToDoModal
            item={item}
            onCancel={() => this.closeModal()}
            onSubmit={() => {
              this.updateTodo(item);
              this.closeModal();
            }}
          />
        );
      case 'M':
        return (
          <OutlookModal
            item={item.meetingInfo}
            onCancel={() => this.closeModal()}
          />
        );
      case 'P':
        return (
          <PassportModal
            item={item}
            onCancel={() => this.closeModal()}
            onSubmit={() => {
              this.updateTodo(item);
              this.closeModal();
            }}
          />
        );
      default:
        return null;
    }
  };

  updateTodo(item: Todo) {
    let status = {
      itemType: item.type,
      status: 'completed'
    };

    this.setState(state => {
      return {
        group: {
          category: state.group.category,
          tasks: state.group.tasks.filter(task => task !== item)
        }
      };
    });

    fetchWrapper<Item>(`todoLists/todos/${item.id}`, {
      method: 'PUT',
      data: status
    });
  }

  closeModal = () => this.setState({ open: false });

  renderToDos() {
    return (
      this.state.group.tasks &&
      this.state.group.tasks.map((item, i) => {
        return (
          <ToDoItem
            item={item}
            itemKey={`${this.props.group.category} toDo${i}`}
            category={
              this.props.group.category ? this.props.group.category : undefined
            }
            onClick={() => this.setState({ open: true, modalItem: item })}
          />
        );
      })
    );
  }

  render() {
    const modalContent =
      this.state.modalItem && this.getModalContent(this.state.modalItem);

    return (
      <div>
        {this.renderToDos()}
        {!!modalContent ? (
          <Modal open={this.state.open} onClose={this.closeModal}>
            <Paper style={styles.modal}>{modalContent}</Paper>
          </Modal>
        ) : null}
      </div>
    );
  }
}

const styles = {
  modal: {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    outline: 'none'
  }
};

export default ToDoGroup;
