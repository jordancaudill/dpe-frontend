import * as React from 'react';
import Chip from '@material-ui/core/Chip';
import Typography from '@material-ui/core/Typography';
import { Todo } from '../../models/app/todo.model';
import PassportCaption from './captions/PassportCaption';
import MeetingCaption from './captions/MeetingCaption';

type ToDoItemProps = {
  item: Todo;
  itemKey: string;
  category?: string;
  onClick: () => void;
};

export class ToDoItem extends React.Component<ToDoItemProps> {
  getTypeSystem = (itemType?: string | null) => {
    switch (itemType) {
      case 'T':
      case 'M':
        return 'Office 365';
      case 'P':
        return 'Passport';
    }
    return null;
  };

  getChipColor = () => {
    switch (this.props.category) {
      case 'Overdue':
        return '#d0021b';
      case 'Due Today':
        return '#f5a623';
    }
    return undefined;
  };

  render() {
    return (
      <div
        style={styles.todo.container}
        id={this.props.item.id}
        key={this.props.itemKey}
        onClick={this.props.onClick}
      >
        <div style={styles.todo.check} />

        <div style={styles.todo.body}>
          <Typography variant="body2" component="div">
            {this.props.item.body}
          </Typography>

          <Typography variant="h3" component="span">
            {this.getTypeSystem(this.props.item.type)}
          </Typography>

          {this.props.item.type === 'P' && (
            <PassportCaption item={this.props.item} />
          )}

          {this.props.item.type === 'M' && (
            <MeetingCaption item={this.props.item} />
          )}
        </div>

        {this.props.category && (
          <Chip
            color="secondary"
            label={
              <Typography
                variant="caption"
                component="span"
                style={styles.todo.chipLabel}
              >
                {this.props.category}
              </Typography>
            }
            size="small"
            style={{
              ...styles.todo.chip,
              backgroundColor: this.getChipColor()
            }}
          />
        )}
      </div>
    );
  }
}

const styles = {
  todo: {
    container: {
      padding: '.75rem 0',
      cursor: 'pointer',
      position: 'relative' as 'relative',
      borderBottom: '1px solid rgba(0, 0, 0, 0.12)'
    },
    check: {
      width: '20px',
      height: '20px',
      border: '1px solid rgba(0, 0, 0, 0.12)',
      borderRadius: '10px',
      position: 'absolute' as 'absolute',
      top: '50%',
      transform: 'translate(0, -50%)'
    },
    body: {
      marginLeft: '38px',
      marginRight: '100px'
    },
    chip: {
      position: 'absolute' as 'absolute',
      top: '50%',
      right: 0,
      transform: 'translate(0, -50%)',
      textTransform: 'uppercase' as 'uppercase'
    },
    chipLabel: {
      letterSpacing: '.05rem',
      fontSize: '.66rem',
      fontWeight: 'bold' as 'bold',
      margin: '0 .5rem',
      maxWidth: '100px'
    }
  }
};

export default ToDoItem;
