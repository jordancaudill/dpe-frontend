import * as React from 'react';
import { MeetingInfo } from '../../../models/app/todo.model';
import { shortenString } from '../../../utils';

type OutlookModalProps = {
  item: MeetingInfo;
  onCancel: () => void;
};

export class OutlookModal extends React.Component<OutlookModalProps> {
  render() {
    return (
      <div className="clearfix" style={styles.modal.outlookModal}>
        <div>
          <span style={styles.modal.outlookHeader.subject}>
            {this.props.item.subject}
          </span>

          {this.props.item.onlineMeetingUrl ? (
            <a
              href={this.props.item.onlineMeetingUrl}
              target="_blank"
              rel="noopener noreferrer"
              style={styles.modal.outlookHeader.joinLink}
            >
              JOIN
            </a>
          ) : null}
        </div>

        <div style={styles.modal.outlookContainer}>
          <div style={styles.modal.outlookBody}>
            <div>
              <span style={styles.modal.meetingDetails}>
                {this.props.item.date}
              </span>

              <span
                style={styles.modal.meetingDetails}
              >{`${this.props.item.startTime} - ${this.props.item.endTime} (${this.props.item.length} minutes)`}</span>

              {this.props.item.webLink ? (
                <a
                  href={this.props.item.webLink}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  Open in Outlook
                </a>
              ) : null}
            </div>

            <div style={styles.modal.emailBody}>
              {this.props.item.body || ''}
            </div>
          </div>

          <div style={styles.modal.outlookAttendees}>
            <div>Organizer</div>

            <ul style={styles.modal.outlookAttendees.list}>
              <li style={styles.modal.outlookAttendees.listItem}>
                <div style={styles.modal.outlookAttendees.userIcon}>
                  <span>{(this.props.item.organizer || '').slice(0, 1)}</span>
                </div>

                {shortenString(this.props.item.organizer || '', 20)}
              </li>
            </ul>

            <div style={{ marginTop: '1rem' }}>Attendees</div>

            <ul style={styles.modal.outlookAttendees.list}>
              {this.props.item.attendees.map((attendee, i) => {
                if (
                  attendee.name &&
                  attendee.name !== this.props.item.organizer
                ) {
                  return (
                    <li key={i} style={styles.modal.outlookAttendees.listItem}>
                      <div style={styles.modal.outlookAttendees.userIcon}>
                        <span>{attendee.name.slice(0, 1)}</span>
                      </div>

                      {shortenString(attendee.name, 20)}
                    </li>
                  );
                }

                return null;
              })}
            </ul>
          </div>
          <button style={styles.modal.button} onClick={this.props.onCancel}>
            Close
          </button>
        </div>
      </div>
    );
  }
}

const styles = {
  modal: {
    outlookModal: {
      position: 'relative' as 'relative',
      width: '80vw',
      maxWidth: '800px',
      padding: '0.5rem',
      border: '3px solid #d8d8d8',
      backgroundColor: '#d8d8d8'
    },
    outlookHeader: {
      subject: {
        position: 'relative' as 'relative',
        display: 'inline-block',
        padding: '0.75 rem 0',
        fontSize: '1.4 rem',
        color: '#58595b'
      },
      joinLink: {
        padding: '0.5rem 2rem',
        marginRight: '0.5rem 2rem',
        color: '#ffffff',
        backgroundColor: 'rgba(79, 114, 242)',
        position: 'relative' as 'relative',
        top: '5px',
        borderRadius: '5px',
        letterSpacing: '1px',
        textDecoration: 'none',
        float: 'right' as 'right'
      }
    },
    outlookContainer: {
      padding: '1rem',
      backgroundColor: '#fff'
    },
    outlookBody: {
      display: 'inline-block',
      width: '69%'
    },
    meetingDetails: {
      display: 'block',
      marginBottom: '4px'
    },
    emailBody: {
      fontSize: '0.8rem',
      lineHeight: '1.4rem',
      borderBottom: '1px dotted #5c5b5a',
      whiteSpace: 'pre' as 'pre'
    },
    outlookAttendees: {
      display: 'inline-block',
      verticalAlign: 'top',
      width: '30%',
      backgroundColor: '#ffffff',
      height: 'auto',
      overflow: 'hidden',
      list: { listStyleType: 'none' },
      listItem: {
        marginTop: '10px',
        marginBottom: '10px',
        whiteSpace: 'nowrap' as 'nowrap'
      },
      userIcon: {
        width: '2rem',
        height: '2rem',
        margin: '0 0.5rem'
      }
    },
    button: {
      marginLeft: '20px'
    }
  }
};

export default OutlookModal;
