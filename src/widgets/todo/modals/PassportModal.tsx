import * as React from 'react';
import { PassportTodo } from '../../../models/app/todo.model';

type PassportModalProps = {
  item: PassportTodo;
  onCancel: () => void;
  onSubmit: () => void;
};

export class PassportModal extends React.Component<PassportModalProps> {
  render() {
    return (
      <div style={styles.modal.passportModal}>
        <h3 style={styles.modal.closureNote}>Closure note</h3>
        <span style={styles.modal.span}>
          ATI #
          {this.props.item.newcapurl ? (
            <a href={this.props.item.newcapurl} color="#0095da">
              {this.props.item.id}
            </a>
          ) : (
            this.props.item.id
          )}
        </span>
        <span>Description: {this.props.item.body}</span>
        <div style={styles.modal.inputs}>
          <span style={styles.modal.span}>In-Progress notes:</span>
          <textarea style={styles.modal.textArea} />
          <span style={styles.modal.span}>Completion notes:</span>
          <textarea style={styles.modal.textArea} />
          <div style={styles.modal.buttonGroup}>
            <button style={styles.modal.button} onClick={this.props.onCancel}>
              Cancel
            </button>
            <button
              style={styles.modal.submitButton}
              onClick={this.props.onSubmit}
            >
              Close action
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const styles = {
  modal: {
    passportModal: {
      margin: '10px 20px'
    },
    closureNote: {
      fontWeight: 'normal' as 'normal',
      marginBottom: '10px'
    },
    span: {
      display: 'block',
      margin: '5px 0px',
      fontSize: '15px'
    },
    inputs: {
      margin: '40px 0px 15px'
    },
    textArea: {
      display: 'block',
      marginBottom: '30px',
      width: '100%',
      height: '60px',
      backgroundColor: '#d8d8d8',
      border: 'none'
    },
    buttonGroup: {
      float: 'right' as 'right',
      marginBottom: '30px'
    },
    button: {
      marginLeft: '20px'
    },
    submitButton: {
      marginLeft: '20px',
      color: 'white',
      borderRadius: '8px',
      backgroundColor: '#0095da',
      padding: '10px'
    }
  }
};

export default PassportModal;
