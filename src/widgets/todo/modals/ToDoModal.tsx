import * as React from 'react';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import { Todo } from '../../../models/app/todo.model';

type ToDoModalProps = {
  item: Todo;
  onCancel: () => void;
  onSubmit: () => void;
};

export class ToDoModal extends React.Component<ToDoModalProps> {
  render() {
    return (
      <div style={styles.modal.container}>
        <div style={styles.modal.header}>
          <CheckCircleIcon
            style={{
              color: '#6fbf98',
              fontSize: '1.9rem',
              marginRight: '5px',
              marginBottom: '-3px'
            }}
          />
          Complete Task?
        </div>
        <div style={styles.modal.body}>{this.props.item.body}</div>
        <button style={styles.modal.button} onClick={this.props.onCancel}>
          Cancel
        </button>
        <button style={styles.modal.submitButton} onClick={this.props.onSubmit}>
          Complete
        </button>
      </div>
    );
  }
}

const styles = {
  modal: {
    container: {
      position: 'relative' as 'relative',
      margin: '4rem auto 0',
      paddingRight: '5rem',
      paddingLeft: '5rem',
      textAlign: 'center' as 'center'
    },
    header: {
      fontSize: '24px',
      marginBottom: '12px'
    },
    body: {
      marginBottom: '48px'
    },
    button: {
      margin: '20px'
    },
    submitButton: {
      margin: '20px',
      color: 'white',
      borderRadius: '8px',
      backgroundColor: '#0095da',
      padding: '10px'
    }
  }
};

export default ToDoModal;
