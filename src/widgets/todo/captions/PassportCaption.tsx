import * as React from 'react';
import Typography from '@material-ui/core/Typography';
import { PassportTodo } from '../../../models/app/todo.model';

type PassportCaptionProps = {
  item: PassportTodo;
};

export class PassportCaption extends React.Component<PassportCaptionProps> {
  render() {
    return (
      <Typography variant="caption" component="span">
        &nbsp;|&nbsp;ATI #
        {this.props.item.newcapurl ? (
          <a href={this.props.item.newcapurl}>{this.props.item.id}</a>
        ) : (
          this.props.item.id
        )}
      </Typography>
    );
  }
}

export default PassportCaption;
