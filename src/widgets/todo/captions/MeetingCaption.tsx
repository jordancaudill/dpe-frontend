import * as React from 'react';
import Typography from '@material-ui/core/Typography';
import { MeetingTodo } from '../../../models/app/todo.model';

type MeetingCaptionProps = {
  item: MeetingTodo;
};

export class MeetingCaption extends React.Component<MeetingCaptionProps> {
  render() {
    return (
      <Typography variant="caption" component="span">
        {this.props.item.meetingInfo.onlineMeetingUrl ? (
          <span>
            &nbsp;|&nbsp;
            <a
              onClick={ev => ev.stopPropagation()}
              href={this.props.item.meetingInfo.onlineMeetingUrl}
              target="_blank"
              rel="noopener noreferrer"
            >
              Join
            </a>
          </span>
        ) : this.props.item.meetingInfo.webLink ? (
          <span>
            &nbsp;|&nbsp;
            <a
              onClick={ev => ev.stopPropagation()}
              href={this.props.item.meetingInfo.webLink}
              target="_blank"
              rel="noopener noreferrer"
            >
              Open in Outlook
            </a>
          </span>
        ) : null}
      </Typography>
    );
  }
}

export default MeetingCaption;
