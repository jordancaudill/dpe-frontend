import * as React from 'react';
import { ToDoList } from './ToDoList';
import { fetchWrapper, todoTransform } from '../../utils';
import { Item } from '../../models/todo/item.model';
import { Todo } from '../../models/app/todo.model';
import widgetContainer, { ContentProps } from '../WidgetContainer';
import LoadingNotifier from '../../components/LoadingNotifier';

type ToDoWidgetState = {
  loading: boolean;
  toDos: { category: string; tasks: Todo[] }[];
};

export class ToDoWidget extends React.Component<ContentProps, ToDoWidgetState> {
  state: ToDoWidgetState = {
    loading: false,
    toDos: []
  };

  componentDidMount() {
    this.fetchToDos(true);
    this.refreshToDos();
  }

  componentWillUnmount() {
    window.clearInterval(this.toDoRefresh);
  }

  limit = 80;

  toDoRefresh: number | undefined;

  fetchToDos = (showLoader?: boolean) => {
    if (showLoader) {
      this.setState({ loading: true });
    }
    const userId = this.props.user && this.props.user.id;
    fetchWrapper<Item[]>(
      `todoLists/todos?userId=${userId}&since=1&limit=${this.limit}&itemType=A`
    )
      .then(items => {
        const currentUserDisplayName =
          this.props.user && this.props.user.displayName;
        const toDos = todoTransform(items, currentUserDisplayName);
        this.setState({
          toDos: toDos,
          loading: false
        });
      })
      .catch(err => {
        console.error(`Error fetching todoList ${err}`);
        this.setState({ loading: false });
      });
  };

  refreshToDos() {
    this.toDoRefresh = window.setInterval(this.fetchToDos, 300000);
  }

  render() {
    return this.state.loading ? (
      <div
        style={{
          height: '100%',
          overflowY: 'auto' as 'auto'
        }}
      >
        <LoadingNotifier />
      </div>
    ) : (
      <ToDoList toDoLists={this.state.toDos} fetchToDos={this.fetchToDos} />
    );
  }
}

export default widgetContainer(ToDoWidget);
