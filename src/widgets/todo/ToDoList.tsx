import * as React from 'react';
import ToDoGroup from './ToDoGroup';
import { Todo } from '../../models/app/todo.model';

type ToDoListProps = {
  toDoLists: { category: string; tasks: Todo[] }[];
  fetchToDos: () => void;
};

export class ToDoList extends React.Component<ToDoListProps> {
  render() {
    return (
      <div
        style={{
          height: '100%',
          overflowY: 'auto' as 'auto'
        }}
      >
        {this.props.toDoLists && this.props.toDoLists.length ? (
          this.props.toDoLists.map((group, i) => {
            if (group.tasks.length) {
              return (
                <ToDoGroup
                  key={`category: ${group.category}`}
                  group={group}
                  fetchToDos={this.props.fetchToDos}
                />
              );
            }

            return null;
          })
        ) : (
          <div className="finished-message">All done ... for now :)</div>
        )}
      </div>
    );
  }
}
