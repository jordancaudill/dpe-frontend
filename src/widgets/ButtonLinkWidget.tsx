import * as React from 'react';
import { widgetContainer, ContentProps } from './WidgetContainer';
import { GenericButton } from '../components/shared/GenericButton';

type ButtonLinkWidgetProps = ContentProps & { color?: string };

const ButtonLinkWidget = (props: ContentProps) => {
  const color = props.config && props.config.color ? props.config.color : '';
  if (props.config && props.config.widgetUrl) {
    return (
      <div
        style={{
          height: '100%',
          alignItems: 'center',
          display: 'flex',
          justifyContent: 'flex-end'
        }}
      >
        <GenericButton
          handleClick={() =>
            props.config &&
            props.config.widgetUrl &&
            window.open(props.config.widgetUrl, '_blank')
          }
          textColor={color || '#4a4a4a'}
          bdColor={color || '#4a4a4a'}
        >
          {props.title}
        </GenericButton>
      </div>
    );
  }
  return null;
};

export default widgetContainer(ButtonLinkWidget);
