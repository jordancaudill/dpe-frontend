import * as React from 'react';
import DragIndicatorIcon from '@material-ui/icons/DragIndicator';
import { Typography } from '@material-ui/core';
import Divider from '@material-ui/core/Divider';
import WidgetOptions, { WidgetOptionsProps } from './WidgetOptions';

export type BaseWidgetProps = WidgetOptionsProps & {
  title: JSX.Element | string | null | undefined;
  subtitle?: string | null | undefined;
  classes?: string;
  notItem?: boolean;
  footer?: JSX.Element;
  widgetSize: string;
  onClick?: React.MouseEventHandler;
  hideChrome?: boolean;
  config?: {
    [key: string]: string | null | undefined;
  };
};

export class BaseWidget extends React.Component<BaseWidgetProps> {
  render() {
    return (
      <div
        className={this.props.classes}
        style={{
          height: '100%'
        }}
        onClick={ev =>
          'function' === typeof this.props.onClick
            ? this.props.onClick(ev)
            : null
        }
        id={!this.props.notItem ? this.props.widgetId : ''}
      >
        {!this.props.hideChrome ? (
          <div className="draggableHandle">
            <Typography
              variant="h2"
              component="h2"
              style={{ textTransform: 'uppercase' }}
            >
              {this.props.title}
            </Typography>
            {this.props.subtitle ? (
              <Typography
                variant="h3"
                component="h3"
                style={{
                  marginTop: '1px'
                }}
              >
                {this.props.subtitle}
              </Typography>
            ) : null}
            <WidgetOptions
              hideOptions={this.props.hideOptions || false}
              widgetId={this.props.widgetId}
              refreshClick={this.props.refreshClick}
              removeClick={this.props.removeClick}
            />
             
            <Divider
              style={{
                width: '300%',
                marginLeft: '-16px'
              }}
            />
          </div>
        ) : (
          <DragIndicatorIcon
            className="draggableHandle"
            fontSize="small"
            style={{
              position: 'absolute' as 'absolute',
              top: '40%',
              transform: 'translate(0, -50%)',
              right: 0,
              color: '#6B6B6B',
              cursor: 'move'
            }}
          />
        )}
        {this.props.children}
        {this.props.footer}
      </div>
    );
  }
}

export default BaseWidget;
