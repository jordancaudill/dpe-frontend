import * as React from 'react';
import { useSelector } from 'react-redux';
import BaseWidget, { BaseWidgetProps } from './BaseWidget';
import { StoreState } from '../store';

export type ContentProps = Partial<ReturnType<typeof createSelector>> &
  BaseWidgetProps;

const createSelector = (state: StoreState) => {
  return {
    user: state.app.user,
    plantLocations: state.app.plantLocations
  };
};

export const widgetContainer = <P extends BaseWidgetProps>(
  Component: React.ComponentType<P>
) => {
  return (props: P) => {
    const storeProps = useSelector(createSelector);
    return (
      <BaseWidget {...props}>
        <Component {...props} {...storeProps} />
      </BaseWidget>
    );
  };
};

export default widgetContainer;
