import AppianWidget from './AppianWidget';
import IFrameWidget from './IFrameWidget';
import ButtonLinkWidget from './ButtonLinkWidget';
import ToDoWidget from './todo/ToDoWidget';
import PlantStatusWidget from '../components/PlantStatusWidget';

const components = {
  AppianWidget,
  IFrameWidget,
  ButtonLinkWidget,
  ToDoWidget,
  PlantStatusWidget
};

export { components };
