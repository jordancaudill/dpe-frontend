import { createUserManager } from 'redux-oidc';

require('dotenv').config();

export const tenantId = process.env.REACT_APP_TENANT_ID;
const resourceId = process.env.REACT_APP_RESOURCE_ID;
const appId = process.env.REACT_APP_CLIENT_ID;
export const loginDomain = 'https://login.microsoftonline.com';
const authURLBase = `${loginDomain}/${tenantId}/v2.0`;

const userManagerConfig = {
  client_id: appId,
  redirect_uri: process.env.REACT_APP_REDIRECT_URI,
  response_mode: 'fragment',
  response_type: 'token id_token',
  scope: `api://${resourceId}/access_as_user openid profile`,
  authority: `${authURLBase}/.well-known/openid-configuration?appid=${appId}`,
  silent_redirect_uri: process.env.REACT_APP_SILENT_REDIRECT_URI,
  automaticSilentRenew: true,
  filterProtocolClaims: true,
  loadUserInfo: false,
  // extraQueryParams: {
  //   resource: resourceId,
  // },
};

const userManager = createUserManager(userManagerConfig);

export default userManager;

export { userManagerConfig };

// https://login.microsoftonline.com/600d01fc-055f-49c6-868f-3ecfcc791773/v2.0/.well-known/openid-configuration
// {
// 	"authorization_endpoint": "https://login.microsoftonline.com/600d01fc-055f-49c6-868f-3ecfcc791773/oauth2/v2.0/authorize",
// 	"token_endpoint": "https://login.microsoftonline.com/600d01fc-055f-49c6-868f-3ecfcc791773/oauth2/v2.0/token",
// 	"token_endpoint_auth_methods_supported": ["client_secret_post", "private_key_jwt", "client_secret_basic"],
// 	"jwks_uri": "https://login.microsoftonline.com/600d01fc-055f-49c6-868f-3ecfcc791773/discovery/v2.0/keys",
// 	"response_modes_supported": ["query", "fragment", "form_post"],
// 	"subject_types_supported": ["pairwise"],
// 	"id_token_signing_alg_values_supported": ["RS256"],
// 	"http_logout_supported": true,
// 	"frontchannel_logout_supported": true,
// 	"end_session_endpoint": "https://login.microsoftonline.com/600d01fc-055f-49c6-868f-3ecfcc791773/oauth2/v2.0/logout",
// 	"response_types_supported": ["code", "id_token", "code id_token", "id_token token"],
// 	"scopes_supported": ["openid", "profile", "email", "offline_access"],
// 	"issuer": "https://login.microsoftonline.com/600d01fc-055f-49c6-868f-3ecfcc791773/v2.0",
// 	"claims_supported": ["sub", "iss", "cloud_instance_name", "cloud_instance_host_name", "cloud_graph_host_name", "msgraph_host", "aud", "exp", "iat", "auth_time", "acr", "nonce", "preferred_username", "name", "tid", "ver", "at_hash", "c_hash", "email"],
// 	"request_uri_parameter_supported": false,
// 	"userinfo_endpoint": "https://graph.microsoft.com/oidc/userinfo",
// 	"tenant_region_scope": "NA",
// 	"cloud_instance_name": "microsoftonline.com",
// 	"cloud_graph_host_name": "graph.windows.net",
// 	"msgraph_host": "graph.microsoft.com",
// 	"rbac_url": "https://pas.windows.net"
// }
