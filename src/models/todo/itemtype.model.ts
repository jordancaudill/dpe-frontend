export enum ItemType {
    Task = "T",
    Meeting = "M",
    Passport = "P",
    All = "A",
}
