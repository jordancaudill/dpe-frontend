import { Attendee } from './attendee.model';

export interface Item {
  itemType: 'T' | 'P' | 'M';
  subjectLine?: string | null;
  subjectTitle?: string | null;
  startDate?: Date | null;
  dueDate?: Date | null;
  assignedTo?: string | null | null;
  status?: string | null;
  webLink?: string | null;
  onlineMeetingUrl?: string | null;
  newcapurl?: string | null;
  owner?: string | null;
  body?: string | null;
  attendees?: Attendee[] | null;
  error?: any | null;
  id: string;
}
