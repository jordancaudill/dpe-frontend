export class Attendee {
    public emailAddress?: string | null;
    public name?: string | null;
    public responseStatus?: string | null;
    public userPhoto?: string | null;
}
