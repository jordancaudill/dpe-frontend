export class User {
    public businessPhones?: [string] | null;
    public displayName?: string | null;
    public jobTitle?: string | null;
    public mail?: string | null;
    public mobilePhone?: string | null;
    public officeLocation?: string | null;
    public preferredLanguage?: string | null;
    public givenName?: string | null;
    public surname?: string | null;
    public userPrincipalName?: string | null;
    public userPhoto?: string | null;
    public timezone?: string | null;
    public role?: string | null;
    public id: string;

    constructor(id: string) {
        this.id = id;
    }
}
