import { Attendee } from '../todo/attendee.model';

export type Todo = RegularTodo | PassportTodo | MeetingTodo;

interface BaseTodo {
  id: string;
  body?: string | null;
}

export interface RegularTodo extends BaseTodo {
  type: 'T';
}

export interface PassportTodo extends BaseTodo {
  type: 'P';
  newcapurl?: string | null;
}

export interface MeetingTodo extends BaseTodo {
  type: 'M';
  meetingInfo: MeetingInfo;
}

export interface MeetingInfo {
  subject?: string | null;
  date: string;
  startTime: string;
  endTime: string;
  length: number;
  allDay?: boolean | null;
  organizer?: string | null;
  attendees: Attendee[];
  status?: string | null;
  body?: string | null;
  webLink?: string | null;
  onlineMeetingUrl?: string | null;
}
