export type Link = {
  title: string;
  icon?: string;
  link: string;
};

export type Links = {
  [link: string]: Link;
};
