import { WidgetConfigParam } from './widgetconfigparam.model';

export class Widget {
  public modelId?: string | null;
  public configurable?: string | null;
  public userConfigurable?: string | null;
  public title?: string | null;
  public subtitle?: string | null;
  public image?: string | null;
  public category?: string | null;
  public type?: string | null;
  public widgetUrl?: string | null;
  public tags?: string | null;
  public contentArea?: string | null;
  public widgetConfigparms?: WidgetConfigParam[] | null;
  public status?: string | null;
  public createdBy?: string | null;
  public createdOn?: string | null;
}
