import { DashboardWidgetOrder } from "./dashboardwidgetorder.model";

export class UserDashboard {
    public userId?: string | null;
    public dashboardId?: string | null;
    public dashboardOrder?: DashboardWidgetOrder[] | null;
}
