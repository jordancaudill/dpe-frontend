export class WidgetComponentMap {
  public id?: string[];
  public component?: string;
  public name?: string | null;
}
