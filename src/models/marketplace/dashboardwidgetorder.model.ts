import { Widget } from './widget.model';
import { WidgetConfigParam } from './widgetconfigparam.model';

export class DashboardWidgetOrder extends Widget {
  public instanceId?: string;
  public order?: number | null;
  public widgetInstanceConfigparms?: WidgetConfigParam[] | null;

  constructor(widget: Widget | DashboardWidgetOrder) {
    super();
    Object.assign(this, widget);
  }
}
