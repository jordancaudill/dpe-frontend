export class WidgetConfigParam {
    public key?: string | null;
    public value?: string | null;

    constructor(key: string, value: string) {
        this.key = key;
        this.value = value;
    }
}
