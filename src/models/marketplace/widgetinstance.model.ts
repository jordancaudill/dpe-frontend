import { WidgetConfigParam } from "./widgetconfigparam.model";

export class WidgetInstance {
    public modelId?: string | null;
    public instanceId?: string | null;
    public widgetInstanceConfigparms?: WidgetConfigParam[] | null;
}
