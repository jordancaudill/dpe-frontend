export interface PlantUnitLimit {
  metric: string;
  max: string;
  min: string;
}

export interface PlantUnitGenerator {
  id: string;
  name: string;
  metrics: string[];
}

export interface PlantUnit {
  id: string;
  name: string;
  color: string;
  limits: PlantUnitLimit[];
  metrics: string[];
  generators: PlantUnitGenerator[];
}

export interface Plant {
  name: string;
  codeName?: string;
  id: string;
  reactorType?: string;
  plantMetrics?: any[];
  units?: PlantUnit[];
}
