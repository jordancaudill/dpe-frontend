export interface GaugeChartGroup {
  kind: string;
  min: string;
  max: string;
  value: number;
  uom: string;
  displayName: string;
  colors: string;
}

export interface GaugeChart {
  lastUpdated?: string;
  leftUnitName?: string;
  leftGaugeGroup?: GaugeChartGroup[];
  rightUnitName?: string;
  rightGaugeGroup?: GaugeChartGroup[];
}
