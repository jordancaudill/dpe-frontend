export interface LineChartUnitDetail {
  name: string;
  color: string;
  id: string;
  gid: string;
}

export interface LineChartUnitData {
  timestamps: string;
  data: number[];
}

export interface LineChartUnit {
  unit: LineChartUnitDetail;
  data: LineChartUnitData;
}

export interface LineChartData {
  units: LineChartUnit[];
  type: string;
  metric: string;
  uom: string;
}

export interface LineChart {
  menu: string[];
  data: { [name: string]: LineChartData };
}
