export interface GraphResponse<T> {
    data: T,
}