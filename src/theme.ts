import { createMuiTheme } from '@material-ui/core/styles';

export const theme = createMuiTheme({
  palette: {
    primary: {
      //////// Title bar blue
      main: '#2372b9'
    },
    secondary: {
      /////// green used on the add to dash button
      main: '#5ae4c5'
    },
    error: {
      ////// red color used in the notifications background
      main: '#f50057'
    }
  },
  typography: {
    fontFamily: 'ProximaNova',
    h1: {
      ////// Main Headers : Dashboard Header, AddWidget Header, Review Widget Header
      fontSize: '26px',
      fontStyle: 'normal',
      fontWeight: 600,
      lineHeight: 'normal',
      letterSpacing: '-0.45px',
      color: '#4a4a4a',
      fontFamily: 'ProximaNova-Semibold'
    },
    h2: {
      /////// Widget Titles
      fontSize: '16px',
      fontStyle: 'normal',
      letterSpacing: '1px',
      lineHeight: 'normal',
      color: '#4a4a4a',
      fontWeight: 600,
      textTransform: 'uppercase',
      fontFamily: 'ProximaNova-Semibold'
    },
    h3: {
      /////// Widget Subtitles
      fontSize: '14px',
      fontStyle: 'normal',
      letterSpacing: '-0.04px',
      lineHeight: 'normal',
      color: '#9b9b9b',
      fontWeight: 600
    },
    h4: {
      //////// Market Tile Specs
      fontSize: '14px',
      fontStyle: 'normal',
      letterSpacing: '-0.04px',
      lineHeight: 'normal',
      fontWeight: 600,
      color: '#4a4a4a'
    },
    subtitle1: {
      /////// Notification text on the Dashboard Header
      fontSize: '14px',
      fontStyle: 'normal',
      letterSpacing: '0.79px',
      lineHeight: 'normal',
      color: '#d0021b',
      fontWeight: 600
    }
  },
  overrides: {
    MuiCard: {
      root: {
        borderRadius: '0px'
      }
    }
  }
});
