Quick Links Handling
In this folder you will find a folder called json.  It contains two files:

links.json:  this is a list of potential links to be used on DPE.
  The format of this file is a comma-separated list of key/value pairs as such
    KEY: {
      title: THE TITLE DISPLAYED WHEN A MOUSE HOVERS OVER A LINK,
      icon: THE PATH TO THE LINK'S ICON **,
      link: THE URL TO THE SITE,
    },

quickLinks.json:  this is a list of what links display, in order, within the Quick Links Menu.
  The format of this file is a comma-separated list of KEYs corresponding to a KEY values within the file above
    [ KEY1, KEY2, etc. ]

To update items within the Quick Links Menu, find the proper KEY within the links.json file and make whatever changes are necessary.
To add new items within the Quick Links Menu, add a new entry in the proper format to the links.json file and add the corresponding KEY to the quickLinks.json file.

NOTE:  making changes directly in this folder will be overwritten on code deployments.  Whenever a change is made here, that change should also be made to the corresponding files in the codebase:  ./public/json/links.json and ./public/json/quickLinks.json.  A code deployment will not be necessary for this change, however it is best to keep the two sets of files in sync so changes made directly on the server are not lost.
