# Custom branch argument
branch=$1
if [ $# -eq 0 ]
  then
    branch="develop"
fi

# Remove current swagger.json
rm -rf public/assets/*.swagger.json

# Make sure documentation is up to date
cd ../documentation/apis
git checkout $branch
git pull

# Generate
./node_modules/yamljs/bin/yaml2json -psr .
mv *.swagger.json ../../frontend/public/assets/

# Return to project
cd ../../frontend
