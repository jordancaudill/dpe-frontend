module.exports = {
  setupFiles: ['<rootDir>/setupTests.js'],
  transform: {
    '^.+\\.(t|j)sx?$': 'ts-jest'
  },
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx'],
  moduleNameMapper: {
    '\\.(css|less|sass|scss)$': '<rootDir>/__mocks__/styleMock.js',
    '\\.(gif|ttf|woff|otf|eot|svg|png)$': '<rootDir>/__mocks__/fileMock.js',
    '@fortawesome|recharts|./assets/': '<rootDir>/__mocks__/fileMock.js',
  },
  setupFilesAfterEnv: ['@testing-library/jest-dom/extend-expect'],
  coverageDirectory: '<rootDir>/coverage/jest',
  reporters: [
    'default',
    'jest-junit'
  ],
  globals: {
    'ts-jest': {
      tsConfig: 'tsconfig.test.json'
    }
  }
};
