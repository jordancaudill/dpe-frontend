// Declarative pipeline script for building and deploying the frontend
pipeline {
  agent any
  options {
    disableConcurrentBuilds()
  }
  stages {
    stage('Downloading code') { // for display purposes
      steps {
        git branch: 'master',
          credentialsId: '9cb4e24f-aa7b-4108-819f-b8521292738e',
          url: 'ssh://git@digitalplant:7999/dpe/digital-plant-experience-frontend.git'
      }
    }
    stage('Cleaning') {
      steps {
        dir('build') {
          deleteDir()
        }
      }
    }
    stage('Building') {
      steps {
        configFileProvider([configFile(fileId: 'spa.dev', targetLocation: '.env')]) {
        }
        bat("""
          npm config set registry http://registry.npmjs.org/ && \
          npm install && \
          npm run build
        """)
      }
    }
    stage('OWASP scan') {
      steps {
        echo "OWASP scan"
        bat("""
          npm config set registry http://registry.npmjs.org/
        """)
        dependencyCheck additionalArguments: '-s build -o dependency-check-report.xml -f XML', odcInstallation: 'OWASP'
        dependencyCheckPublisher pattern: 'dependency-check-report.xml'
      }
    }
    stage('Publish') {
      steps {
        script {
          sshPublisher(
            continueOnError: false, failOnError: true,
            publishers: [
              sshPublisherDesc(
                configName: "dpe-apigee-dev",
                verbose: true,
                transfers: [
                  sshTransfer(
                    sourceFiles: "build/**/*",
                    remoteDirectory: ".",
                    removePrefix: "build"
                  )
                ]
              )
            ]
          )
        }
        office365ConnectorSend message: "Pushed to Exelon's dev server", 
          status: "Build",
          webhookUrl: "https://outlook.office.com/webhook/40aad043-f6b5-4387-af49-703c4f16e5fd@e0793d39-0939-496d-b129-198edd916feb/IncomingWebhook/372a40a82c5445849b6d8075aaffe858/2ec10aaf-5cf3-4f4d-b85c-aac90ceb5063"
      }
    }
  }
}